import os
import logging
import psycopg2

from uuid import uuid4
from datetime import datetime
from pandas import read_csv, DataFrame
from .salesforce import SalesforceConnector


class PostgresConnector:
    def __init__(self, connection_string: str, tmp_dir: str):
        self.conn            = psycopg2.connect(connection_string)
        self.conn.autocommit = True
        self.cur             = self.conn.cursor()
        self.tmp_dir         = tmp_dir

    @classmethod
    def initialise_from(cls, salesforce: SalesforceConnector):
        credentials = salesforce.query(
            """SELECT BR_PG_Database__c, BR_PG_Host__c, BR_PG_Password__c, BR_PG_Port__c, BR_PG_Schema__c, BR_PG_UserName__c FROM ETL_Config__c 
            WHERE Name = 'BRMS' LIMIT 1"""
        )
        connection_str = "dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
            dbname=credentials.at[0, 'BR_PG_Database__c'],
            host=credentials.at[0, 'BR_PG_Host__c'],
            port=credentials.at[0, 'BR_PG_Port__c'],
            user=credentials.at[0, 'BR_PG_UserName__c'],
            password=credentials.at[0, 'BR_PG_Password__c']
        )
        return cls(connection_str)

    # @timer
    def execute_query(self, query):
        logging.info('\n\n %s \n' % query)
        # cur = self.conn.cursor()
        self.cur.execute(query)
        # cur.close()

    # @timer
    def fetch_records(self, **kwargs):
        query = kwargs.get("query", None)
        table_name = kwargs.get('object_name', None)
        column_details = kwargs.get("column_details", DataFrame())
        logging.info('\n\n {} \n'.format(query))
        abs_file_path = os.path.abspath(os.path.join(self.tmp_dir, table_name + '.csv'))
        # cur = self.conn.cursor()
        # cur.execute(query)
        # df = DataFrame(cur.fetchall())
        # output_query = "Copy (" + query.replace(" FROM ", " FROM brms_instance.") + ") TO '{}' WITH CSV HEADER".format(abs_file_path)
        output_query = "Copy (" + query + ") TO '{}' WITH CSV HEADER".format(abs_file_path)
        # with open(abs_file_path, 'w') as f:
        #     cur.copy_expert(output_query, f)
        self.cur.execute(output_query)
        # cur.close()
        df = read_csv(abs_file_path, dtype='object')
        logging.debug("Postgres record count %d" % len(df))
        # if not column_details.empty:
        #     # df.columns = column_details["Source_Column__c"].values.tolist()
        #     df.columns = column_details[column_details['tb_col_nm__c'].isin(df.columns)]['Source_Column__c']
        # df = df.apply(lambda x: x.fillna(""))
        # df.fillna("",inplace= True)
        return abs_file_path

    # @timer
    def return_output(self, query: str):
        logging.info('\n\n %s \n' % query)
        # cur = self.conn.cursor()
        self.cur.execute(query)
        df = DataFrame(self.cur.fetchall())
        # cur.close()
        return df
    
        
    def close(self):
        self.conn.close()


    def _in_tuple(self,iterable):
        return str(tuple(set(iterable))).replace(',)',')')

    def delete_records(self, table_name, key_column, key_values):
        # cur = self.conn.cursor()
        query = "DELETE FROM "+table_name+" WHERE "+key_column+" IN "+_in_tuple(key_values)
        logging.debug('\n\n %s \n' % query)
        self.cur.execute(query)
        self.cur.close()

    def upload_from_csv(self, table_name, ordered_cols, file_name, delimiter=','):
        self.execute_query("COPY " + table_name + " (" + ordered_cols + ") FROM '" + file_name + "' DELIMITER '"+delimiter+"' CSV HEADER")

    def insert_records(self, **kwargs):
        logging.info("Inserting records into Postgres")
        table_name = kwargs.get("object_name", None)
        filepath = os.path.abspath(kwargs.get("file_path", None))
        adapter_columns = kwargs.get('adapter_columns', None)
        create_table = kwargs.get('create_table', True)

        table_name = 'brms_instance.' + table_name
        # with open(filepath) as f:
        #     a = f.readline()
        # columns = a[:-2].split(",")
        column_details = ''
        for col in adapter_columns:
            column_details += col.replace('"', '') + " varchar, "
        column_details = column_details[:-2]
        if create_table:
            table_name += '_' + datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
            create_query = "CREATE TABLE IF NOT EXISTS {} ({})".format(table_name, column_details)
            logging.debug("\n\n {} \n".format(create_query))
            self.cur.execute(create_query)
        copy_sql = "COPY {} ({}) FROM STDIN WITH CSV HEADER".format(table_name, ', '.join(adapter_columns))
        logging.debug("\n\n {} \n".format(copy_sql))
        logging.debug(filepath)
        with open(filepath, 'r') as f:
            self.cur.copy_expert(sql=copy_sql, file=f)
        # self.cur.execute(copy_sql)
        # with open(filepath, 'r') as f:
        #     self.cur.copy_expert(sql=copy_sql, file=f)
        return table_name.replace("brms_instance.", '')

    def upload_across_server(self, table_name, columns, filepath):
        # abs_file_path = os.path.abspath(os.path.join('{}'.format(folder), "{}".format(file_name)) + '.csv')
        # cur = self.conn.cursor()
        # cur.execute(query)
        # df = DataFrame(cur.fetchall())
        copy_sql = """COPY {} ({}) FROM STDIN WITH  CSV HEADER DELIMITED as ','""".format(table_name, columns)
        with open(filepath, 'r') as f:
            self.cur.copy_expert(sql = copy_sql, file = f)
        # self.cur.close()
        # df = read_csv(abs_file_path, dtype = 'object')
        # if not column_details.empty:
        #     # df.columns = column_details["Source_Column__c"].values.tolist()
        #     df.columns = column_details[column_details['tb_col_nm__c'].isin(df.columns)]['Source_Column__c']
        # df = df.apply(lambda x: x.fillna(""))
        # df.fillna("",inplace= True)