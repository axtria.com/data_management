import os
import boto3
import logging

from uuid import uuid4
from pandas import read_csv, DataFrame


class S3Connector:
    """
    This class implements boto3 API to interact with S3 service. Currently it supports the following methods:
    upload_file(): copy an input dataframe into a csv at unique S3 temp location,
    use default s3 client method to copy to desired path

    download_file(): copy csv from remote path to unique S3 path with temp filename,
    return csv as dataframe to user and delete temp file.

    copy_file(): use boto3 resource methods to loop and filter out src_path objects and copy one by one

    exists(): use try catch block to validate whether given key exists in the bucket

    list_files(): use it to get list of files at a given prefix of location

    Attributes:
        :param: src_path: source path
        :param: dest_path: destination_path
        :param:
    """
    def __init__(self, region_name: str, aws_access_key_id: str, aws_secret_access_key: str, bucket: str,
                 path: str, tmp_dir: str):
        self.s3_client   = boto3.client('s3',
                                         aws_access_key_id=aws_access_key_id,
                                         aws_secret_access_key=aws_secret_access_key)
        self.s3_resource = boto3.resource('s3', aws_access_key_id=aws_access_key_id,
                                          aws_secret_access_key=aws_secret_access_key)
        self.bucket      = bucket
        self.tmp_dir     = tmp_dir

    def upload_file_from_data_object(self, data_obj, remote_path, filename):
        """Upload data from an object to S3 with given filename"""
        try:

            keyname = filename + ".csv"
            #local path is for temp folder filename saved
            local_path = os.path.abspath(os.path.join(self.tmp_dir, str(uuid4()) + keyname))
            #print("local_path",local_path)
            data_obj.to_csv(local_path, sep=',', index=False)
            # logging.info(f"->file_name ::{filename} remote path :: {remote_path}")
            # complete path is S3 path
            complete_path = remote_path + keyname
            self.upload_file_from_local_path(local_path, complete_path)
        except Exception as e:
            logging.error(e.args)
            raise e

    def insert_records(self, **kwargs):
        file_path   = kwargs.get('file_path', None)
        remote_path = kwargs.get('remote_path', None)
        """Upload data from local path to S3 and deletes file on local path""" 
        try:
            self.s3_client.upload_file(file_path, self.bucket, remote_path)
            # logging.debug(f"local file is uploaded at S3 prefix:: {remote_path}")
            # os.remove(local_path)
        except Exception as e:
            logging.error(e.args)
            raise e
    
    def fetch_records(self, **kwargs):
        """downloads file from s3 and returns the data frame without bucket"""
        object_name = kwargs.get("object_name", None)
        local_path = os.path.join(self.tmp_dir, object_name.split("/")[-1].capitalize() + '_' + str(uuid4()) + '.csv')
        remote_path = kwargs.get("remote_path", None)
        self.s3_client.download_file(self.bucket, remote_path, local_path)
        # data_obj = read_csv(local_path)
        # os.remove(local_path)
        return local_path
    def download_file_without_header(self, complete_path):
        """downloads file from s3 and returns the data frame without default first line as header"""
        try:
            local_path = self.tmp_dir + str(uuid4())
            self.s3_client.download_file(self.bucket, complete_path, local_path)
            data_obj = read_csv(local_path,header=None)
            os.remove(local_path)
            return data_obj
        except Exception as e:
            logging.warning(e.args)
            logging.warning(complete_path)
            raise e

    def copy_file(self, src_path, dest_path):
        """Copies all the files from src_prefix to dest_prefix
            VERY IMP AND COOL::
            Replace function is to change old substring to new prefix of dest_path in case of multiple keys found
        """
        try:
            bucket_resource = self.s3_resource.Bucket(self.bucket)
            for obj in bucket_resource.objects.filter(Prefix=src_path):
                source = {'Bucket': self.bucket, 'Key': obj.key}
                # replace the prefix for each file
                dest_key = obj.key.replace(src_path, dest_path)
                bucket_resource.copy(source, dest_key)
        except Exception as e:
            logging.error(e.args)
            raise e

    def exists(self, key):
        """Validates presence of key for a given S3 bucket."""
        logging.info('Validating file presence... %s' % key)
        try:
            contents = self.s3_client.list_objects(Bucket=self.bucket, Prefix=key)['Contents']
            return True
        except KeyError as e :
            logging.error("KeyError File not found %s" % key)
            return False
        except Exception as e:
            logging.error(e.args)
            return False

    def list_files(self, prefix: str) -> list:
        """returns all 'folders' and file names in s3 under the prefix folder"""
        try:
            keys = []
            result = self.s3_client.list_objects(Bucket=self.bucket, Prefix=prefix, Delimiter='/')
            print(result)
            if result.get('CommonPrefixes'):
                for o in result.get('CommonPrefixes'):
                    print("o :: ",o)
                    keys.append(o.get('Prefix'))
            if result.get('Contents'):
                for o in result.get('Contents'):
                    print("o2 :: ",o)
                    print(o.get('Key'))
                    keys.append(o.get('Key'))
            return keys
        except Exception as e:
            logging.error(e.args)
            raise e

    def get_key_names_from_folder(self, prefix: str)->list:
        keys = []
        result = self.s3_client.list_objects(Bucket=self.bucket, Prefix=prefix, Delimiter='/')
        if result.get('Contents'):
            for o in result.get('Contents'):
                # print("o2 :: ", o)
                # print(o.get('Key'))
                keys.append(o.get('Key'))
        get_file_names_only = []
        for s in keys:
            s = s.rsplit('/',1)
            get_file_names_only.append(s[-1])
        return get_file_names_only

    def s3_delete_all_at_prefix(self, prefix:str):
        try:
            bucket_resource = self.s3_resource.Bucket(self.bucket)
            list_of_deleted = list(bucket_resource.objects.filter(Prefix=prefix).delete())
            if list_of_deleted:
                logging.info(f"deleted all objects for S3 prefix :: {prefix}")
            else:
                logging.info(f"no objects found for S3 prefix :: {prefix}")
        except Exception as e:
            logging.error(e.args)
            raise e

    def s3_copy_file(self, filename:str,source:str,destination:str):
        pass

    # def s3_delete_file(filename:str,source:str):
    #     """delete a file from S3 bucket"""
    #     try:
    #         s3 = boto3.resource('s3')
    #         self.Object('your-bucket', 'your-key').delete()
    #     except Exception as e:
    #         logging.error(e.args)
    #         raise e
