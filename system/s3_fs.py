import gzip
import boto3
import s3fs
import pandas as pd

from io import StringIO

class S3:

    def __init__(self, access_key: str, secret_key: str):

        self.client = boto3.client('s3', aws_access_key_id=access_key,
                                   aws_secret_access_key=secret_key)
        self.fs_connection = s3fs.S3FileSystem(key=access_key, secret=secret_key)
        self.access_key = access_key
        self.secret_key = secret_key


    def read(self, filename, rows) -> list:

        if filename.endswith('.gz'):
            return self._read_gz(filename, rows)
        else:
            return self._read_default(filename, rows)


    def _read_default(self, filename, rows) -> list:

        records = []
        with self.fs_connection.open(filename, 'rb') as f:
            for _ in range(rows):
                records.append(f.readline().decode('utf-8'))

        return records


    def _read_gz(self, filename, rows) -> list:

        records = []
        with gzip.open(self.fs_connection.open(filename, 'rb'), 'rt') as f:
            for _ in range(rows):
                records.append(f.readline())

        return records

    def list_objects(self, bucket_name, prefix='', continuation_token=None):

        if continuation_token:
            response = self.client.list_objects_v2(Bucket=bucket_name, Prefix=prefix, ContinuationToken=continuation_token)
        else:
            response = self.client.list_objects_v2(Bucket=bucket_name, Prefix=prefix)

        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            self.awsRegion = response['ResponseMetadata']['HTTPHeaders']['x-amz-bucket-region']

            if response['Contents']:
                metadata = pd.DataFrame(response['Contents'])[['Key','LastModified','Size']]
                metadata['LastModified'] = metadata['LastModified'].apply(lambda x:x.strftime('%Y-%m-%d %H:%M:%S'))
                metadata = metadata[~(metadata['Key'].str.startswith('/') | metadata['Key'].str.endswith('/'))]
                metadata['Key'] = metadata['Key'].str.replace(prefix,'')
                metadata = metadata.to_dict(orient='records')

            if response['IsTruncated'] == True:
                return {'metadata':metadata, 'continuation_token':response['NextContinuationToken']}
            return {'metadata':metadata, 'continuation_token':None}


    def list_folders(self, bucket_name, prefix='', continuation_token=None):

        if continuation_token:
            response = self.client.list_objects_v2(Bucket=bucket_name, Prefix=prefix, ContinuationToken=continuation_token)
        else:
            response = self.client.list_objects_v2(Bucket=bucket_name, Prefix=prefix)

        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            self.awsRegion = response['ResponseMetadata']['HTTPHeaders']['x-amz-bucket-region']

            if response['Contents']:
                metadata = pd.DataFrame(response['Contents'])[['Key','LastModified']]
                metadata['LastModified'] = metadata['LastModified'].apply(lambda x:x.strftime('%Y-%m-%d %H:%M:%S'))
                metadata = metadata[~metadata['Key'].str.startswith('/') & metadata['Key'].str.endswith('/')]
                metadata['Key'] = metadata['Key'].str.replace(prefix,'').str.replace('/','')
                metadata = metadata.to_dict(orient='records')

            if response['IsTruncated'] == True:
                return {'metadata':metadata, 'continuation_token':response['NextContinuationToken']}
            return {'metadata':metadata, 'continuation_token':None}


    def detect_columns(self, bucket_name, object_key, n_dtype_detection_records = 10):
        try:
            file_key = bucket_name + '/' + object_key
            data = self.read(file_key, n_dtype_detection_records)
            delimiter = self.getDelimiter(data[0])
            data = pd.read_csv(StringIO(''.join(data)), sep=delimiter)


            dt = data.dtypes   
            dt = dt.replace('int32','INT')
            dt = dt.replace('int64','INT')
            dt = dt.replace('float32','FLOAT')
            dt = dt.replace('float64','FLOAT')
            dt = dt.replace('object','STRING') 
            dt = dt.replace('bool','BOOL')

            cols = [{'column':col,'datatype':dtype} for col,dtype in zip(data.columns, dt)]

            return {'detected':True, 'columns':cols}

        except Exception as e:
            return  {'detected':False, 'error':str(e)}

    def getDelimiter(self, header_str):

        delimiters = [',','|',';',':','\t']
        detected_delimiter = None
        max_count = 0

        for delimiter in delimiters:
            delimiter_count = header_str.count(delimiter)

            if 0 <= max_count < delimiter_count:
                detected_delimiter = delimiter
                max_count = delimiter_count

        return detected_delimiter
