import os
import pyodbc
# from src.service.config import cnx
import logging
import pandas as pd

class SqlConnector:
    def __init__(self, host, dbname, user, password, tmp_dir):
        self.conn = pyodbc.connect(driver='SQL Server Native Client 11.0',
                                   host=host,
                                   dbname=dbname,
                                   user=user,
                                   password=password,
                                   autocommit = True)
        # self.conn = pyodbc.connect(driver='SQL Server Native Client 11.0',
        #                            host="35.163.66.25",
        #                            database="Jasper_Demo",
        #                            uid="product",
        #                            pwd="product",
        #                            autocommit = True)
        self.cursor = self.conn.cursor()
        self.tmp_dir = tmp_dir

    def __del__(self):
        self.conn.close()

    def execute_query(self, query = "SELECT @@version;") -> bool:
        logging.info('\t\t%s\n' % query)
        try:
            self.cursor.execute(query)
            row = self.cursor.fetchone()
            while row:
                logging.info(row[0])
                row = self.cursor.fetchone()
            return True
        except Exception as e:
            logging.error(e.args)
            raise e
            # return False

    def return_output(self, query: str):
        logging.info('\n\n %s \n' % query)
        try:
            with self.conn.cursor() as cursor:
                cursor.execute(query)
                # self.conn.commit()
                return cursor.fetchall()  # List of Tuples
        except Exception as e:
            logging.error(e.args)
            raise e

    def fetch_records(self, object_name: str, query: str, **kwargs):
        logging.info('\n\n %s \n' % query)
        try:
            df = pd.read_sql(query, self.conn)
            path = os.path.join(self.tmp_dir, obj + '.csv')
            df.to_csv(path, index=False)
            return path
        except Exception as e:
            logging.error(e.args)
            raise e

    ####### function to bulk insert csv file into sql table ##########
    def insert_records(self, **kwargs):
        object_name = kwargs.get('object_name', None)
        file_path   = kwargs.get('file_path', None) 
        ####### Remove null rows from end of csv file
        txt = None
        with open(file_path, 'r') as f:
            txt = f.read().strip()
        with open(file_path, 'w') as f:
            f.write(txt)

        infile_query = f"""BULK INSERT {object_name}
                            FROM '{file_path}'
                            WITH (FIRSTROW = 2, FIELDTERMINATOR = '{delimiter}', ROWTERMINATOR = '\r\n', ERRORFILE = './/Output//Temp_Error.txt');
                        """
        try:
            self.conn.execute(infile_query.format(file_name=file_path, table_name=object_name, delimiter=delimiter))
            logging.debug("**************Bulk Data inserted*****************")
        except Exception as e:
            logging.error(e.args)
            raise e

    def sql_proc_call(self,proc_name,param):
        try:
            self.conn.execute(f"call [{proc_name}] '"+param + "'")
        except Exception as e:
            logging.error(e.args)
            raise e

    # def update(self,table_name,):
# #Sample insert query
# cursor.execute("INSERT SalesLT.Product (Name, ProductNumber, StandardCost, ListPrice, SellStartDate) OUTPUT INSERTED.ProductID VALUES ('SQL Server Express New 20', 'SQLEXPRESS New 20', 0, 0, CURRENT_TIMESTAMP )")
# cnxn.commit()
# row = cursor.fetchone()

# while row:
#     print 'Inserted Product key is ' + str(row[0])
#     row = cursor.fetchone()
#
# @staticmethod