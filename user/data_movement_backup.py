import os 
import json
import logging

from pandas import read_csv
from ast import literal_eval
from datetime import datetime

from user.data_processor import establish_connection

class DataMovement:
    def __init__(self, source: str, dest: str, entity: str, filter_clause: str, 
                 validation: bool, user_mapping: dict, validated_table: str, tmp_dir: str):
        self.source          = source.lower()
        self.dest            = dest.lower()
        self.entity          = entity.lower()
        self.filter_clause   = " WHERE " + filter_clause if filter_clause else ''
        self.validation      = validation
        self.user_mapping    = user_mapping #literal_eval(str(user_mapping).lower())
        self.validated_table = validated_table
        self.tmp_dir         = tmp_dir
       
    def invoke(self):
        try:
            logging.info("\n\n --------------------------------------------- INVOKING DATA MOVEMENT JOB -------------------------------------------------- \n")
            CONFIG = self.load_config()
            if self.validated_table:
                mapping = CONFIG['adapter']
            elif self.user_mapping:
                # for key in self.user_mapping:
                #     self.user_mapping[key]['Mapping'] = {value : k for k, value in self.user_mapping[key]['Mapping'].items()}
                # logging.debug(self.user_mapping)
                mapping = self.user_mapping
            else:
                mapping = CONFIG['adapter']
            if not self.validated_table:
                source_obj      = establish_connection(self.tmp_dir, CONFIG['connector'].split('_to_')[0])
                mapping_name    = 'source_to_adapter'
                # file_name_index = 0
            else:
                source_obj = establish_connection(self.tmp_dir, 'postgres')
                if self.CONFIG['etl']['workflow_id']:
                	mapping_name = 'etl_to_destination'
                else:
                	mapping_name = 'adapter_to_destination'
                # file_name_index = 1
            if self.validated_table:
                dest_obj = establish_connection(self.tmp_dir, CONFIG['connector'].split('_to_')[1])
            else:
                dest_obj = establish_connection(self.tmp_dir, 'postgres')
            files = {}
            tables = {}

            """
            Retrieving records from source. 
            If self.validated_table is blank, it implies the movement is from source to postgres for validation.
            Else, it's from postgres to source.
            returns : 
            If movement is to pg for validation:-
                tables - {file_path : adapter}
                The value is adapter because this is the name of the table created in postgres.
            if movement is from pg (i.e. self.validated_table is not blank):-
                tables - {file_path : objec_name}
                This object name us resolved from the adapter from the adapter_file mapping.
                This object name is used to insert records into destination.
            """
            logging.debug("Validated_tables = {}".format(self.validated_table))
            for adapter in CONFIG['processing_order']:
                if self.validated_table:
                    with open(os.path.join('configurationFiles', 'creds.json')) as f:
                        creds = json.load(f)
                    object_name = creds['schema'] + '.' + self.validated_table[adapter]
                elif self.user_mapping:
                    for key in self.user_mapping:
                        if key.lower() == adapter:
                             adapter = key
                    object_name = self.user_mapping[adapter]['FileName']
                    mapping_name = 'Mapping'
                else:
                    with open(os.path.join('configurationFiles','adapter_to_file.json')) as f:
                        adapter_file_mapping = literal_eval(str(json.load(f)).lower())
                        object_name = adapter_file_mapping[adapter][file_name_index]
                logging.info("Fetching {} FROM {}".format(adapter, self.source))
                query = "SELECT {} FROM {}".format(", ".join(mapping[adapter][mapping_name].keys()) + self.filter_clause, object_name)
                file_path = source_obj.fetch_records(object_name=object_name, query=query, remote_path='DEV/'+object_name)
                df = read_csv(file_path, dtype='object')
                df = df[mapping[adapter][mapping_name].keys()]
                df.columns = [mapping[adapter][mapping_name][col] for col in df.columns]
                df.to_csv(file_path, index=False)
                del df
                if not self.validated_table:
                    files[file_path] = adapter
                else:
                    with open(os.path.join('configurationFiles','adapter_to_file.json')) as f:
                        adapter_file_mapping = literal_eval(str(json.load(f)).lower())
                        object_name = adapter_file_mapping[adapter][file_name_index]
                    files[file_path] = object_name
            logging.debug("Files : {}".format(files))
            # logging.info("Inserting data into {}".format(self.dest.upper()))

            """
            Inserting records into destination.
            In case of movement to postgres for validation, tables created for validation are returned for further
            consumption. 
            return : 
            If data movement is to pg for validation:-
                tables - {adapter_name : (table_created_in_pg, adapter_column_name_list)}
                This info is supplied to Validation_API.
            Else if self.validated_table is not blank (i.e. data movement is from pg to destination), 
            the function returns blank values in the dictionary as only the pg)insert method returns
            the created table name.
            """

            for file_path in files:
                insert_response = dest_obj.insert_records(file_path=file_path, object_name=files[file_path], 
                                                     adapter_columns=[mapping[adapter][mapping_name][i] for i in mapping[adapter][mapping_name]],
                                                     remote_path=self.entity + '_' + self.source + '_' + self.dest + '_' + datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + '.csv')
                if isinstance(insert_response, int):
                    if insert_response > 0:
                        logging.debug("Some records failed to insert")
                        return False
                tables[files[file_path]] = (insert_response, [mapping[adapter][mapping_name][i] for i in mapping[adapter][mapping_name]])
            logging.info("Data Moved Successfully")
            return tables
        except Exception as e:
            logging.exception("ERROR")
            raise e

    def load_config(self):
        logging.info("Loading Configration Files")
        with open(os.path.join('configurationFiles', 'adapters', self.entity + '.json')) as f:
            CONFIG = literal_eval(str(json.load(f)).lower())
        return CONFIG[self.entity][self.source+'_to_'+self.dest]