import os 
import json
import logging

from pandas import read_csv
from ast import literal_eval
from datetime import datetime
from importlib import import_module

from user.data_processor import establish_connection

class DataMovement:
    def __init__(self, source: str, dest: str, entity: str, filter_clause: str, 
                 validation: bool, user_mapping: dict, validated_table: str, transformed_tables, 
                 tmp_dir: str, talend_params: str):
        self.source          = source.lower()
        self.dest            = dest.lower()
        self.entity          = entity.lower()
        self.filter_clause   = filter_clause if filter_clause else ''
        if filter_clause:
            self.filter_clause = filter_clause.replace(' and ', ' AND ').split(' AND ')
        else: 
            self.filter_clause = ''
        self.validation      = validation
        self.user_mapping    = user_mapping #literal_eval(str(user_mapping).lower())
        self.validated_table = validated_table
        self.transformed_tables = transformed_tables
        self.tmp_dir         = tmp_dir
        self.talend_params   = talend_params
        self.schema          = ''
       
    def invoke(self):
        try:
            logging.info("\n\n --------------------------------------------- INVOKING DATA MOVEMENT JOB -------------------------------------------------- \n")
            CONFIG = self.load_config()
            mapping, source_conn, dest_conn, proc_order, tables = self.set_workflow(CONFIG)
            # logging.debug("Mapping : {}".format(mapping))
            # logging.debug("source_conn : {}".format(source_conn))
            # logging.debug("dest_conn : {}".format(dest_conn))
            # logging.debug("proc_order : {}".format(proc_order))
            # logging.debug("tables : {}".format(tables))
            mapp = {}
            for key in mapping:
                mapp[key.lower()] = mapping[key]
            mapping = mapp
            del mapp




            """
            Retrieving records from source. 
            If self.validated_table is blank, it implies the movement is from source to postgres for validation.
            Else, it's from postgres to source.
            returns : 
            If movement is to pg for validation:-
                tables - {file_path : adapter}
                The value is adapter because this is the name of the table created in postgres.
            if movement is from pg (i.e. self.validated_table is not blank):-
                tables - {file_path : objec_name}
                This object name us resolved from the adapter from the adapter_file mapping.
                This object name is used to insert records into destination.
            """

            fetched_elem_files = {}
            pg_tables = {}
            logging.debug("Validated_tables = {}".format(self.validated_table))
            for element in proc_order:
                if element.lower() == 'routine':
                    continue
                object_name = tables[element]
                logging.info("Fetching {} FROM {}".format(element, self.source))
                if self.validated_table or self.transformed_tables:
                    filter_string = self.prepare_filter(mapping[element]['filters'], "source")
                else:
                    filter_string = self.prepare_filter(CONFIG['source_to_adapter'][element]['filters'], "source")
                query = "SELECT {} FROM {}".format(", ".join(mapping[element]['mapping'].keys()), 
                                                   object_name + filter_string)
                file_path = source_conn.fetch_records(object_name=object_name, query=query, remote_path='DEV/'+object_name)
                df = read_csv(file_path, dtype='object')
                # df.columns = [i.lower() for i in df.columns]
                # df = df[list(mapping[element]['mapping'].keys())]
                df.columns = [col.lower() for col in df.columns]
                df = df[[col for col in mapping[element]['mapping']]]
                df.columns = [mapping[element]['mapping'][col] for col in df.columns]
                df.to_csv(file_path, index=False)
                del df
                fetched_elem_files[file_path] = element
            logging.debug("fetched_elem_files : {}".format(fetched_elem_files))
            """
            Inserting records into destination.
            In case of movement to postgres for validation, tables created for validation are returned for further
            consumption. 
            return : 
            If data movement is to pg for validation:-
                tables - {adapter_name : (table_created_in_pg, adapter_column_name_list)}
                This info is supplied to Validation_API.
            Else if self.validated_table is not blank (i.e. data movement is from pg to destination), 
            the function returns blank values in the dictionary as only the pg_insert method returns
            the created table name.
            """
            for elem in proc_order:
                if elem.lower() == 'routine':
                    logging.debug("Initiating routine for {}...".format(self.entity))
                    if self.dest.lower() == 'ic':
                        routine = import_module("user.routines.ic")
                        # logging.debug(mapping)
                        routine.invoke(self.source.lower(), self.dest.lower(), self.talend_params.replace("$filename.csv", file_name), 
                                       self.movement)# establish_connection(self.tmp_dir, 'salesforce'))
                    continue
                for path in fetched_elem_files:
                    if fetched_elem_files[path] == elem:
                        file_path = path 
                        break
                if self.validated_table or self.transformed_tables:
                    dest_object_name = mapping[elem]['object']
                    filter_string = self.prepare_filter(mapping[elem]['filters'], "dest")
                else:
                    dest_object_name = elem
                    filter_string = self.prepare_filter(CONFIG['source_to_adapter'][elem]['filters'], "dest")
                if not (self.validated_table or self.transformed_tables):
                    file_name = ''
                else:
                    file_name =  mapping[elem]['object'].replace(".csv", "_") + datetime.now().strftime("%Y_%m_%d__%H_%M_%S")+'.csv' 
                if self.dest.lower() == 'ic':
                    with open(os.path.join('configurationFiles', 'creds.json')) as f:
                        creds = json.load(f)
                        remote_path = creds['s3']['path'] + file_name
                else:
                    remote_path = file_name
                insert_response = dest_conn.insert_records(file_path=file_path, object_name=dest_object_name, 
                                                           adapter_columns=[mapping[elem]['mapping'][i] for i in mapping[elem]['mapping']],
                                                           key_columns=self.fetch_key_columns(mapping[elem]),
                                                           filter_string=filter_string,
                                                           remote_path=remote_path)
                if isinstance(insert_response, int):
                    if insert_response > 0:
                        logging.debug("Some records failed to insert")
                        return False
                pg_tables[fetched_elem_files[file_path]] = (insert_response, [mapping[elem]['mapping'][i] for i in mapping[elem]['mapping']])
            logging.info("Data Moved Successfully")
            return pg_tables
        except Exception as e:
            logging.exception("ERROR")
            raise e

    def set_workflow(self, CONFIG):
        logging.info("Determining workflow ...")
        if self.transformed_tables:
            mapping    = CONFIG['etl_to_destination']
            source_conn = establish_connection(self.tmp_dir, 'postgres')
            dest_conn   = establish_connection(self.tmp_dir, CONFIG['connector'].split('_to_')[1])
            proc_order = CONFIG['etl']['processing_order']
            tables     = self.transformed_tables
            self.schema = 'brms_instance.'
            self.movement = "dest"
        elif self.validated_table:
            mapping    = CONFIG['adapter_to_destination']
            source_conn = establish_connection(self.tmp_dir, 'postgres')
            dest_conn   = establish_connection(self.tmp_dir, CONFIG['connector'].split('_to_')[1])
            proc_order = CONFIG['processing_order']
            tables     = self.validated_table
            self.schema = 'brms_instance.'
            self.movement = "dest"
        else:
            source_conn = establish_connection(self.tmp_dir, CONFIG['connector'].split('_to_')[0])
            dest_conn   = establish_connection(self.tmp_dir, 'postgres')
            proc_order = CONFIG['processing_order']
            self.movement = "source"
            if self.user_mapping:
                mapping     = DataMovement.prepare_dictionary(self.user_mapping)
                tables     = {adapter.lower() : mapping[adapter]['filename'] for adapter in mapping}
            else:
                mapping    = CONFIG['source_to_adapter']
                tables = {adapter.lower() : CONFIG['source_to_adapter'][adapter]['object'] for adapter in CONFIG['source_to_adapter']}
        return mapping, source_conn, dest_conn, proc_order, tables

    def prepare_filter(self, config_filters: dict, movement_type: str):
        logging.debug("Preparing filters")
        # logging.debug(config_filters)
        # import pdb;pdb.set_trace()
        if not self.filter_clause or ((self.transformed_tables or self.validated_table) and movement_type.lower() == 'source') or (movement_type.lower() == 'dest' and not (self.validated_table or self.transformed_tables)):
            return ''
        filter_string = ' '
        for filter_clause in self.filter_clause:
            logging.debug("Preparing {}".format(filter_clause))
            for key in config_filters:
                # if filter_clause.lower().find(key) != -1:
                if key in [i.strip() for i in filter_clause.lower().split(" ")]:
                    logging.debug("{} found in filter".format(key))
                    filter_clause = " " + filter_clause
                    filter_clause = filter_clause.lower().replace(" " + key.lower(), " " + config_filters[key.lower()][movement_type])
                    filter_string += filter_clause + " AND "
                    break

        filter_string = filter_string[:-5]
        logging.debug("filter_clause prepared : {}".format(filter_string))
        return filter_string

    @staticmethod
    def prepare_dictionary(dictionary: dict):
        logging.debug("Preparing dictionary")
        prepared = {}
        def lower(dictionary: dict, prepared: dict):
            for key in dictionary:
                if isinstance(dictionary[key], dict):
                    prepared[key.lower()] = {}
                    lower(dictionary[key], prepared[key.lower()])
                else:
                    prepared[key.lower()] = dictionary[key]
            return prepared
        prepared = lower(dictionary, prepared)
        logging.debug(prepared)
        return prepared


    def fetch_key_columns(self, element):
        logging.debug("Determining key columns to upsert")
        if not (self.validated_table or self.transformed_tables):
            logging.debug("Inserting data into PG for validation and ETL. Upsert not required")
            return []
        keys = element.get('key_columns', [])
        return keys

    def load_config(self):
        logging.info("Loading Configration Files")
        with open(os.path.join('configurationFiles', 'adapters', self.entity + '.json')) as f:
            CONFIG = literal_eval(str(json.load(f)).lower())
        return CONFIG[self.entity][self.source+'_to_'+self.dest]