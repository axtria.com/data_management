import os
import re
import pdb
import json
import logging
import requests
import pandas as pd 

from time import sleep
from ast import literal_eval
from datetime import datetime

from importlib import import_module
from user.data_movement import DataMovement
from system.salesforce import SalesforceConnector
from user.data_processor import establish_connection

session  = requests.Session()
ETL_URL  = 'http://localhost/data_processor/api/brms/execute_workflow?'
VAL_URL  = 'http://localhost/data_processor/api/brms_validation/execute_validation?'
MASTER   = {   
            "account" : "t_fin_do1",
            "employee" : "t_fin_do2"
            }


class Controller:
    """
    Controller class that drives the DataMovement (including Validation and/or Etl transformations) flow.
    Calls the DataMovement class, performs validation and etl tranformations, marks success or error on Salesforce and returns output.
    returns : Success or failure (string)
    """ 

    def __init__(self, source: str, dest: str, entity: str, filter_clause: str, validation: bool, etl: bool, adapter: str, tmp_dir: str, 
                 move_data: bool, validated_table, plan_id: str, server_address: str, url_prefix: str, talend_params, error_talend_url):
        self.source         = source.lower()
        self.dest           = dest.lower()
        self.entity         = entity.lower()
        self.filter_clause  = filter_clause if filter_clause else ''
        self.validation     = validation
        self.etl            = etl
        self.adapter        = adapter
        self.tmp_dir        = tmp_dir
        self.move_data      = move_data
        self.validated_table= literal_eval(str(validated_table).lower())
        self.plan_id        = plan_id
        self.server_address = server_address
        self.url_prefix     = url_prefix
        self.talend_params  = talend_params
        self.error_talend_url = error_talend_url
        self.val_status     = True
        self.transformed_tables = {}
        
    def invoke(self):
        try:
            logging.info("\n\n -------------------------------------------------------- CONTROLLER SERVICE INVOKED ---------------------------------------------------------\n")
            self.salesforce = establish_connection(self.tmp_dir, 'salesforce')
            self.load_configs()
            if self.validation:
                dm = DataMovement(self.source, self.dest, self.entity, self.filter_clause, self.validation, self.adapter, self.validated_table, 
                                  self.transformed_tables, self.tmp_dir, self.talend_params)
                data_tables = dm.invoke()
                logging.debug(data_tables)
                self.validated_table  = self.validate_files(data_tables)
                logging.debug(self.validated_table)
                # self.update_master_tables()

                # if validation_response.status_code > 201:
                #     return "Validation API called successfully but error sending validation status to backend API"
            if self.etl and self.CONFIG['etl']['workflow_id']:
                status = self.etl_transform()
                if not status:
                    return "ETL Transformation Failed"
                logging.debug("Transformed tables after etl - {}".format(self.transformed_tables))
                if not self.resolve_lookups():
                    self.mark_error("Error in Data Loading", "Lookups could not be resolved")
                    return "Data Insertion Failed" + "Lookups could not be resolved"

            if self.move_data and self.val_status:
                dm = DataMovement(self.source, self.dest, self.entity, self.filter_clause, self.validation, self.adapter, self.validated_table, 
                                self.transformed_tables, self.tmp_dir, self.talend_params)
                response = dm.invoke()
                if not response:
                    msg = "Some records failed to insert into {}".format(self.dest)
                    logging.error(msg)
                    self.mark_error("Error in Data Loading", msg)
                    return msg
                else:
                    self.mark_success("Success")

            # self.etl_transform()
            # dm = DataMovement(source, dest, entity, filter_clause, filter_clause, tmp_dir, refresh)
            # dm.invoke()
            return "Data Moved Succesfully"

        except KeyError as k:
            logging.exception("ERROR")
            self.mark_error("ERROR", "Blank csv file generated. No data fetched")
            return "Blank csv file generated. No data fetched"

        except Exception as e:
            logging.exception("ERROR")
            self.mark_error("Error", "Please contact Salesforce administrator")
            return str(e.args)

    def validate_files(self, data_tables):
        """
        Method to call the Validation API on PG. Uses the tabl
        """ 
        logging.info("Calling Validation API")
        status = {  
                    'ValidationStatus' : {},
                    'MappingStatus' : 'NA',
                    'TableNames'    : {},
                    'tenant' : 'ART',
                    'type' : 'DataMax',
                    'planID' : self.plan_id
                }
        for adapter in self.CONFIG['processing_order']:
            logging.debug(adapter)
            if adapter.lower() == 'routine':
                continue
            for key in data_tables:
                if key.lower() == adapter:
                    pg_adapter = key
            val_endpoint = VAL_URL + "validation_id={}&input_adaptors={}&parameters={}&input_adaptor_columns={}&schema={}".format(self.CONFIG['validation'][adapter]['id'],
                                                                                                                         json.dumps({adapter:data_tables[pg_adapter][0]}),
                                                                                                                         {},
                                                                                                                         json.dumps({adapter:data_tables[pg_adapter][1]}),
                                                                                                                         "brms_instance")
            logging.debug(val_endpoint)
            body = {
                    "validation_id" : self.CONFIG['validation'][adapter]['id'],
                    "input_adapter" : {adapter: data_tables[pg_adapter][0]},
                    'schema'        : "brms_instance",
                    "input_adaptor_columns" :  data_tables[pg_adapter][1]
                   }
            logging.debug(body)
            validation_response = session.request("GET", val_endpoint) #, data=json.dumps(body)).json()).lower())
            validation_response = validation_response.json()
            logging.debug('Validation Response for {}:- \n\n {} \n'.format(adapter, validation_response))
            # if validation_response['invalid_meta'][adapter]['invalid_count'] == 0:
            records = ''
            if validation_response["META"]["status"].lower() != 'passed':
                msg = 'Error in Validation'
                logging.error(msg)
                self.mark_error(msg, "Validation API threw an error")
                return msg
            error_perc = int(100*validation_response["INVALID_META"][adapter]['invalid_count']/validation_response["INVALID_META"][adapter]['total_count'])
            pg = establish_connection(self.tmp_dir, 'postgres')
            if error_perc < self.CONFIG['validation'][adapter]['error_threshold']:
                logging.debug("Error percentage within acceptable limits")
                status['ValidationStatus'][adapter] = "Success"
                status['TableNames'][adapter] = validation_response['OUTPUT']['success_' + adapter]
                success_records_path = pg.fetch_records(query="SELECT * FROM {}".format(validation_response['OUTPUT']['success_' + adapter]),
                                                      object_name=validation_response["OUTPUT"]['success_' + adapter].split(".")[1])
                records += ' Link for success records of {}:- https://{}/download?file_path={}&attachment_name={} \n'.format(adapter, 
                                                                                                     self.server_address + self.url_prefix, 
                                                                                                     success_records_path,
                                                                                                     adapter + ".csv")
                self.update_master_tables(adapter, success_records_path, data_tables[pg_adapter][1])
            else:
                self.val_status = False
                status['ValidationStatus'][adapter] = 'Error in Validation'
                status['TableNames'][adapter] = ''
                # error_records_path = pg.fetch_records(query="SELECT * FROM {} WHERE dmx_message IS NOT NULL".format("brms_instance." + validation_response['invalid_meta'][adapter]['instance_name']),
                #                                       object_name=validation_response['invalid_meta'][adapter]['instance_name'])
                error_records_path = pg.fetch_records(query="SELECT * FROM {}".format(validation_response["OUTPUT"]['error_'+ adapter]),
                                                      object_name=validation_response["OUTPUT"]['error_'+ adapter].split(".")[1])
                records += ' Link for error records of {}:- https://{}/download?file_path={}&attachment_name={} \n'.format(adapter, 
                                                                                                     self.server_address + self.url_prefix, 
                                                                                                     error_records_path,
                                                                                                     adapter + ".csv")
            
            # self.validated_table[adapter] = validation_response['invalid_meta'][adapter]['success_instance']
        # logging.debug("Error_files_csv links - {}".format(records))
        # if self.val_status:
        #     self.mark_success("Validation Success", records)
        # else:
        #     # self.mark_error(records)
        #     self.mark_error("Error in Validation", records)
        logging.debug(status)
        
        data = status.copy()
        data["ValidationStatus"] = json.dumps(data["ValidationStatus"])
        data["TableNames"]       = json.dumps(data["TableNames"])
        return_URL = "https://salesiq.axtria.com/DataMaxUIService/SaveValidation/"
        ret = session.request("POST", return_URL, data=data)
        logging.debug(ret)
        return status['TableNames']
        
    def etl_transform(self):
        logging.debug("Invoking ETL Transformation's API")
        logging.debug(self.validated_table)
        # for adapter in self.CONFIG['etl']:
        etl_endpoint = ETL_URL + 'workflow_id={}&input_adaptors={}&schema=brms_instance&parameters={}&output_adaptors={}'.format(self.CONFIG['etl']['workflow_id'],
                                                                                                                                json.dumps({adapter : self.validated_table[adapter].split(".")[1] for adapter in self.validated_table}),
                                                                                                                                {},
                                                                                                                                json.dumps(self.CONFIG['etl']['output']))
        logging.debug("ETL_endpoint - {}".format(etl_endpoint))
        etl_response = session.request("GET", etl_endpoint)
        etl_response = etl_response.json()
        logging.debug("ETL response :- \n\n {} \n".format(etl_response))
        if etl_response['META']['status'].lower() == 'failed':
            logging.error("Transformation Failed. Halting process.")
            self.mark_error('Transformation Failed', "ETLS API threw an error")
            return False
        else:
            for output in self.CONFIG['etl']['output']:
                table = etl_response['OUTPUT'][output]
                self.transformed_tables[output] = table
            self.mark_success("Transformations successful")
            return True
        # return 

    def resolve_lookups(self):
        logging.debug("Resolving lookups if required")
        if self.CONFIG.get('lookups', None):
            postgres = establish_connection(self.tmp_dir, 'postgres')
            for adapter in self.CONFIG['lookups']:
                output = self.CONFIG['lookups'][adapter]['output']
                path = postgres.fetch_records(query="SELECT {} FROM {}".format(", ".join(self.CONFIG['etl_to_destination'][output]['mapping'].keys()),
                                                                               self.transformed_tables[output]),
                                              object_name=self.transformed_tables[output].split(".")[1])
                logging.debug(path)
                df = pd.read_csv(path, dtype='object')
                merged = df.copy()
                # import pdb
                # pdb.set_trace()
                # df.columns = [self.CONFIG['adapter'][adapter]['etl_to_destination'][key] for key in df.columns]
                for field in self.CONFIG['lookups'][adapter]:
                    lookups = {}
                    for col in re.findall('{[aA-zZ]+}', self.CONFIG['lookups'][adapter][field]['query']):
                        col = col[1:-1]
                        lookups[col] = "'" + "', '".join(df[col].unique()) + "'"
                        logging.debug(col)
                        logging.debug(lookups)
                    query = self.CONFIG['lookups'][adapter][field]['query'].format(**lookups)
                    ids = self.salesforce.query(query)
                    ids.to_csv(os.path.join(self.tmp_dir, "sample.csv"), index=False)
                    ids.columns = [column.lower() for column in ids.columns]
                    merged = pd.merge(merged, ids, left_on=field, right_on=self.CONFIG['lookups'][adapter][field]['join'].lower())
                    # merged.to_csv("merged.csv", index=False) 
                    if self.CONFIG['lookups'][adapter][field]['join'].lower()+'_y' not in merged.columns:
                        merged.drop(columns=[field, self.CONFIG['lookups'][adapter][field]['join'].lower()], inplace=True)
                        merged.rename(columns={"id":field}, inplace=True)
                    else: 
                        merged.drop(columns=[field, self.CONFIG['lookups'][adapter][field]['join'].lower()+'_y'], inplace=True)
                        merged.rename(columns={"id":field, self.CONFIG['lookups'][adapter][field]['join'].lower()+'_x': self.CONFIG['lookups'][adapter][field]['join'].lower()}, inplace=True)
                if self.CONFIG['self_lookups']:
                    merged[self.CONFIG['self_lookups']] = ''
                merged.to_csv(path, index=False)
                # routine = import_module("user.routines.{}".format(self.entity.lower()))
                # if routine:
                postgres.execute_query("TRUNCATE {}".format(self.transformed_tables[output]))
                # if routine.invoke(self.CONFIG['lookups'][adapter], path, self.salesforce):
                postgres.insert_records(object_name=self.transformed_tables[output].split(".")[1], file_path=path,
                                        create_table=False, adapter_columns=merged.columns.to_list())
                #         return True
                #     return False

        return True

    def update_master_tables(self, adapter, success_records_path, columns):
        try:
            if adapter in MASTER:
                logging.debug("Updating Master records tables for {} in PG".format(adapter))
                self.postgres.insert_records(object_name=MASTER[adapter], file_path=success_records_path,
                                             adapter_columns=columns, create_table=False)
        except Exception as e:
            logging.error("Unable to update records in master table", exc_info=True)


    def mark_success(self, status, log=''):
        if self.dest.lower() == 'ic':
            return
        logging.info('Marking success on Salesforce')
        t_plan_id = self.salesforce.sf.query("SELECT Id FROM t_plan__c WHERE plan_uuid__c = '{}'".format(self.plan_id))
        t_plan_id = t_plan_id['records'][0]['Id']
        self.salesforce.compound_update('t_plan__c', t_plan_id, {
                                                                'execution_status__c' : status,
                                                                'log__c'              : log
                                                              })
    def mark_error(self, error, log):
        logging.error("marking Error on salesforce")
        # if self.validation or self.move_data:
        if not self.dest.lower() == 'ic':
            t_plan_id = self.salesforce.sf.query("SELECT Id FROM t_plan__c WHERE plan_uuid__c = '{}'".format(self.plan_id))
            t_plan_id = t_plan_id['records'][0]['Id']
            getattr(self.salesforce.sf, 't_plan__c').update(t_plan_id, { 
                                                                    'execution_status__c' : error,
                                                                    'log__c' : log
                                                                })
        else:
            logging.debug("Marking error for IC by calling talend URL")
            # error_talend = self.talend_params.replace("python_webservice_main_job", "dm_sfdc_status_update")
            # error_talend = error_talend.replace("S3", "dm_sfdc_status_update")
            # with open(os.path.join('configurationFiles', 'creds.json')) as f:
            #     creds = json.load(f)
            # error_talend = "".join([error_talend.split(creds['s3']['bucket'])[0],error + ' ' + log])
            self.error_talend_url += ',' + error+ ' ' + log
            logging.debug(self.error_talend_url)
            r = requests.get(self.error_talend_url)
            logging.debug(r)

            # getattr(self.salesforce.sf, 'Instance __c').update(self.plan_id, { 
            #                                                         'execution_status__c' : error,
            #                                                         'log__c'              : log
            #                                                     })
            # ds_id = self.salesforce.sf.query("SELECT data_set__c FROM Instance__c WEHRE Id = '{}'".format(self.plan_id))['records'][0]['id']
            # getattr(self.salesforce.sf, 'Data_Set__c').update(ds_id, { 
            #                                                         'status' : "Incomplete"
            #                                                          })

    def load_configs(self):
        with open(os.path.join('configurationFiles', 'adapter_to_file.json')) as f:
            self.adapter_file_mapping = literal_eval(str(json.load(f)).lower())
        with open(os.path.join('configurationFiles', 'adapters', self.entity+'.json')) as f:
            self.CONFIG = literal_eval(str(json.load(f)).lower())[self.entity][self.source + '_to_' + self.dest]