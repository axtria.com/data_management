import os
import re
import json
import logging

from importlib import import_module

# from mapping import object_mapping, field_mapping
# from exclusions import unmanaged_fields, unmanaged_objects


def establish_connection(tmp_dir: str, connector: str):
    logging.info("Establishing connection to {}".format(connector))
    with open(os.path.join('configurationFiles', 'creds.json')) as f:
        creds = json.load(f)
    creds = creds[connector]
    # if connector.lower() == 'salesforce':
    #     creds = creds[self.org_id]
    conn = import_module("system.{}".format(connector.lower()))
    creds['tmp_dir'] = tmp_dir
    return getattr(conn, connector.lower().capitalize() + "Connector")(**creds)

def retrieve_lookup_field(sf, object_name , parent_object_name):    
    if object_name not in unmanaged_objects:
        object_name = sf._prepare_object_name(object_name)
    if parent_object_name not in unmanaged_objects:
        parent_object_name = sf._prepare_object_name(parent_object_name)
    # inverted_obj_mapping = {v:k for k,v in object_mapping.items()
    description = getattr(sf.sf,object_name).describe()
    fields      = description['fields']
    for field in fields:
        if field['referenceTo'] == [parent_object_name]:
            return field['name']

def append_namespace_string(namespace, object_name, columns_str=None):
    # object_name = object_name.replace(namespace, '')
    if not columns_str:
        columns_str = object_name
    columns_str = columns_str.strip(' ')
    columns_str = ' ' + columns_str
    for custom_field in re.findall('( [0-9aA-zZ_]+__[cC]| [0-9aA-zZ_]+__[rR])', columns_str):
        if object_name in unmanaged_fields:
            # logging.debug("Object is not completely managed. Checking for unmanaged fields")
            if custom_field.strip() in unmanaged_fields[object_name]:
                # logging.debug("Column {} is unmanaged. Skipping namespace attachment for it.".format(custom_field))
                continue
            else:
                columns_str = columns_str.replace(custom_field, ' ' + prepare_object_name(namespace, custom_field[1:]))
        else:
            columns_str = columns_str.replace(custom_field, ' ' + prepare_object_name(namespace, custom_field[1:]))
    for custom_field in re.findall('(\.[0-9aA-zZ_]+__[cC])', columns_str):
        columns_str = columns_str.replace(custom_field, '.' + prepare_object_name(namespace, custom_field[1:]))
    for custom_field in re.findall('(\.[0-9aA-zZ_]+__[rR])', columns_str):
        columns_str = columns_str.replace(custom_field, '.' + prepare_object_name(namespace, custom_field[1:]))
    return columns_str.strip(' ')

def prepare_object_name(namespace: str, object_name:str):
    if object_name.find(namespace) == -1 and (object_name.lower().endswith('__c') or object_name.endswith('__r')):
        return namespace + object_name
    return object_name

def get_object_map_value(key):
    key = key.replace('','')
    if key in object_mapping:
        return object_mapping[key].strip(' ')
    else:
        return key.strip(' ')

def get_field_map_value(object_name, field_name):
        if object_name in field_mapping:
            if field_name in field_mapping[object_name]:
                return field_mapping[object_name][field_name]
        return field_name

def __init__(self):
    pass

def resolve_lookup(self):
    for column_name, object_name in self.relationship.items():
            parent_df = read_csv(os.path.join("tmp", object_name + ".csv"))
            merged_df = merge(df[[column_name]], parent_df[['Id_old', 'Id']],
                              left_on=column_name, right_on='Id_old', how='left')
            df[column_name] = merged_df["Id"]

    
def resolve_mapping(self):
    pass
def resolve_append_object(self):
    pass    