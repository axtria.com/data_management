import os 
import json
import logging

from pandas import read_csv
from ast import literal_eval
from datetime import datetime

from user.data_processor import establish_connection


class Inbound:
    def __init__(self, dest: str, entity: str, filter_clause: str, validation: bool, user_adapter: dict, validated_table: str, 
    			 tmp_dir: str):
        self.dest         	 = dest.lower()
        self.entity       	 = entity.lower()
        self.filter_clause	 = " WHERE " + filter_clause if filter_clause else ''
        self.validation   	 = validation
        self.user_adapter 	 = literal_eval(str(user_adapter).lower())
        self.validated_table = validated_table
        self.tmp_dir      	 = tmp_dir

    def invoke(self):
        try:
            logging.info("\n\n --------------------------------------------- INVOKING INBOUND LOAD JOB -------------------------------------------------- \n")
            CONFIG = self.load_config()

    def load_config(self):
        logging.info("Loading Configration Files")
        with open(os.path.join('configurationFiles', self.entity + '.json')) as f:
            CONFIG = literal_eval(str(json.load(f)).lower())
        return CONFIG[self.entity]['s3'+'_to_'+ self.dest]