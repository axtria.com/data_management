import os
import sys
import json
import logging

import pandas as pd

def invoke(lookups, file_path, salesforce):
	logging.debug("Invoking routine for position_hierarchy....")
	logging.debug(file_path)
	try:
		for field in lookups:
			getattr(sys.modules[__name__], "resolve_%s" % field)(lookups[field], file_path, salesforce)
		return True
	except Exception as e:
		logging.exception("Error while resolving lookups")
		return False

def resolve_team_id(lookups, file_path, salesforce):
	logging.debug("Resolving team_id")
	df = pd.read_csv(file_path, dtype='object')
	logging.debug(df["lookup_team_name"])
	team_id = salesforce.query("SELECT Id FROM {} WHERE {} = '{}'".format(lookups['parent_object'], 
																	 lookups['parent_field'], 
																	 df.at[0, lookups['field_name']]))
	df[lookups['field_name']] = team_id.iloc[0,0]
	logging.debug(df["lookup_team_name"])
	df.to_csv(file_path, index=False)


def resolve_team_instance(lookups, file_path, salesforce):
	logging.debug("Resolving team_instance")
	df = pd.read_csv(file_path,dtype='object')
	team_instance_id = salesforce.query("SELECT Id FROM {} WHERE {} = '{}'".format(lookups['parent_object'], 
																	 lookups['parent_field'], 
																	 df.at[0, lookups['field_name']]))
	df[lookups['field_name']] = team_instance_id.iloc[0,0]
	df["parent_position_name"] = ''
	df.to_csv(file_path, index=False)

def resolve_self_lookups():
	pass
