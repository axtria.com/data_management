import json
import logging
from logging.handlers import TimedRotatingFileHandler


from flask import Flask, request
logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("D:\\Templatization\\Datamax.txt", when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(levelname)s - %(message)s')

app = Flask(__name__)
from Code.ParseJSON import ParseJSON
from Code.DataMax import DataMax
ServiceName= 'DataMaxUIService'

@app.route("/"+ServiceName+"/hello1/")
def Hello():
    logging.debug("k65")
    return json.dumps({"position_hierarchy":{"col1":"col2"}})

@app.route("/"+ServiceName+"/Plan/")
def GetPlanUIFile():
    logging.debug("here1")
    PJ = ParseJSON()
    tenant = request.args.get('tenant', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    planIDFolder = request.args.get('planID', default=0, type=str)
    FilePath = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\FilePath.txt")
    logging.debug(FilePath)
    typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
    configurationFile = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\" + typeModule)
    logging.debug('type==='+type)
    logging.debug('typemod==='+typeModule)
    if (planIDFolder != None):
        if type=='Plan':
            file='UI_File_Plan'
        else :
            file = 'UI_File_SC'



        allPlanInstanceFolder = PJ.GetValueFromKeyPath(configurationFile, "Instances_Folder.Plan")

        jsonData = PJ.LoadJSONFromFile(allPlanInstanceFolder + "\\" + planIDFolder + "\\" + file+".txt")

        jsonDataList = PJ.GetValueFromKeyPath(jsonData, "Parameters")
        appendedFileName = {"Parameters": jsonDataList, "filename": file}
        return (json.dumps(appendedFileName))

@app.route("/"+ServiceName+"/SaveMapping/"  ,methods=['GET', 'POST'])
def SaveMapping():
    logging.debug('using this')
    tenant = request.form['tenant']
    logging.debug(tenant + '=***************************')
    planId = request.form['planID']
    logging.debug(planId + '=***************************')
    type = request.form['type']
    logging.debug(type + '=***************************')
    dataVal=request.form['dataValue']
    logging.debug(dataVal)
    logging.debug(planId)
    IC= DataMax(tenant,type)

    logging.info(dataVal)
    IC.saveMapping(planId,dataVal)
    logging.debug(tenant + 'called' + type)
    return "Success"

@app.route("/"+ServiceName+"/Save/", methods=['GET', 'POST'])
def Save():
    logging.debug("here1 in Save")
    tenant = request.form['tenant']
    data = request.form['data']
    fileName =request.form['filename']
    planIDFolder =request.form['planID']
    type=request.form['type']
    Saved=request.form['Saved']
    logging.debug("here===="+Saved)
    logging.debug(data)
    IC = DataMax(tenant,type)
    if data!='':
        logging.debug(request.url)
        UFC = DataMax(tenant,type)
        logging.debug(fileName)
        dataUpdateChange = UFC.fillValuesfromUIintoUIFile(data, planIDFolder, fileName)
        logging.debug('dataUpdateChange', dataUpdateChange)
        PJ = ParseJSON()
        FilePath = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\FilePath.txt")
        typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
        configurationFile = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\" + typeModule)
        #configurationFile = PJ.LoadJSONFromFile("C:\\Static_Resources\\Templates_config.txt")
        allPlanInstanceFolder = PJ.GetValueFromKeyPath(configurationFile, "Instances_Folder.Plan")

        jsonData = json.dumps(dataUpdateChange, indent=3)
        # print(jsonData)
        f = open(allPlanInstanceFolder + "\\" + planIDFolder + "\\" + fileName + ".txt", "w")
        f.write(jsonData)
        f.close()

        IC.FillWithValuesFile(planIDFolder, fileName)

    return "Success"

@app.route("/"+ServiceName+"/CreateDataMax/")
def CreateDataMax():
    tenant = request.args.get('tenant', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    ModuleType = request.args.get('ModuleType', default=0, type=str)
    DM= DataMax(tenant,type)
    IDFolder = DM.createNewInstance(ModuleType)
    return IDFolder

@app.route("/"+ServiceName+"/getListingDataMax/")
def getListingDataMax():
    type = request.args.get('type', default=0, type=str)
    tenant = request.args.get('tenant', default=0, type=str)
    DM= DataMax(tenant,type)
    jsonListing = DM.getListing()
    return jsonListing

@app.route("/"+ServiceName+"/getMappingDataMax/")
def getMappingDataMax():
    type = request.args.get('type', default=0, type=str)
    tenant = request.args.get('tenant', default=0, type=str)
    planID=request.args.get('planID', default=0, type=str)
    DM= DataMax(tenant,type)
    jsonMapping = DM.getMapping(planID)
    return jsonMapping

@app.route("/"+ServiceName+"/getStatusValidation/")
def getStatusValidation():
    type = request.args.get('type', default=0, type=str)
    tenant = request.args.get('tenant', default=0, type=str)
    planID=request.args.get('planID', default=0, type=str)
    DM= DataMax(tenant,type)
    StatusJSON= DM.getStatusValidation(planID)
    return json.dumps(StatusJSON)

@app.route("/"+ServiceName+"/SaveValidation/"  ,methods=['GET', 'POST'])
def SaveValidation():
    logging.debug('=================Validation')
    planId = request.form['planID']
    # planId = fetch_parameter(request, "planID")
    logging.debug('=================Validation2' + planId)
    type = request.form['type']
    tenant =request.form['tenant']
    validationStatus=request.form['ValidationStatus']
    logging.debug('=================Validation2' +validationStatus)
    mappingstatus=request.form['MappingStatus']
    logging.debug('=================Validation2' +mappingstatus)
    TableNames=request.form['TableNames']
    # fetch_parameter(request, "TableNames", type=dict)
    logging.debug('=================Validation2'+TableNames)
    DM = DataMax(tenant, type)
    logging.debug('=================Validation2'+tenant+type)
    DM.saveStatus(planId,mappingstatus,validationStatus,TableNames)
    return  "Success"

@app.route("/"+ServiceName+"/getMappingValidationService/")
def getMappingValidationService():
    planId = request.args.get('planID', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    tenant = request.args.get('tenant', default=0, type=str)
    DM = DataMax(tenant, type)
    mappingJSON = DM.getMappingValidationService(planId)
    return json.dumps(mappingJSON)

@app.route("/"+ServiceName+"/createExecuteInstance/")
def createExecuteInstance():
    oldId = request.args.get('oldID', default=0, type=str)
    newId = request.args.get('newID', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    tenant = request.args.get('tenant', default=0, type=str)
    DM = DataMax(tenant, type)
    DM.createExecuteInstance(oldId,newId)
    return "Success"

if __name__ == '__main__':
	app.run(debug=True)