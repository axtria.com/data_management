import requests

class Webrequest:

    @staticmethod
    def get_request(url):
        r = requests.get(url)
        try:
            r.raise_for_status()
            return r.json()
        except requests.exceptions.HTTPError as e:
            print(str(e))
            return e