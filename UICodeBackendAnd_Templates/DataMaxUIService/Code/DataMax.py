import json
from Code.ParseJSON import ParseJSON
import logging
import uuid
import os
from shutil import copyfile
from distutils.dir_util import copy_tree
from Code.Webrequest import Webrequest
import configparser
test_config = configparser.ConfigParser()
test_config._interpolation = configparser.ExtendedInterpolation()
test_config.optionxform = str
test_config.read('Code\config.ini')

class DataMax:
    def __init__(self, tenant,type):
        PJ = ParseJSON()


        FilePath = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\FilePath.txt")
        typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
        self.configurationFile = PJ.LoadJSONFromFile("C:\\Static_Resources\\" + tenant + "\\" + typeModule)
        self.PJ=ParseJSON()
        self.tenant=tenant

    def getListing(self):
        #jsonListing = self.PJ.LoadJSONFromFile(
         #   self.PJ.GetValueFromKeyPath(self.configurationFile, "Templates_Folder.Others")) + "\\Listing_DataMax.txt"
        jsonListing= self.PJ.LoadJSONFromFile("D:\\Templatization\\ART\\DataMax\\Templates\\Queries\\Listing.txt")

        return json.dumps(jsonListing)

    def createNewInstance(self,Moduletype):
        planTemplatesFolderLocation = self.PJ.GetValueFromKeyPath(self.configurationFile, "Templates_Folder.Component")
        allPlanInstanceFolder = self.PJ.GetValueFromKeyPath(self.configurationFile, "Instances_Folder.Plan")
        planInstanceID = "SC_ID_" + str(uuid.uuid1())
        planInstanceJSON = {planInstanceID: ''}
        self.PJ.UpdateKeyValueFromFile(planInstanceJSON, planInstanceID,
                                       planTemplatesFolderLocation + "\\" + Moduletype + ".txt", "")
        newPathForInstance = allPlanInstanceFolder + "\\" + planInstanceID
        if not os.path.exists(newPathForInstance):
            os.makedirs(newPathForInstance)
        jsonData = json.dumps(planInstanceJSON, indent=3)
        f = open(newPathForInstance + "\\" + planInstanceID + ".txt", "w")
        f.write(jsonData)
        f.close()

        copyfile(planTemplatesFolderLocation + "\\UI_File_" + Moduletype + ".txt",
                 allPlanInstanceFolder + "\\" + planInstanceID + "\\" + "UI_File_SC.txt")

        entitylist=''
        for path in self.PJ.GetAllPaths(planInstanceJSON,["Info_Entity.Value"]):
            entitylist=entitylist+path.split(":->")[1]+','
        entitylist=entitylist[:-1]
        url = test_config[self.tenant]['serverName']+'/'+test_config[self.tenant]['serviceName']+'/'+test_config[self.tenant]['methodName']+'?adapter_name='+entitylist
        logging.debug('url==='+url)
        jsonValue= Webrequest.get_request(url)
        logging.debug(jsonValue)
        for entity in entitylist.split(','):
            columns=''
            typeCol=''
            for colPath in self.PJ.GetAllPaths(jsonValue,[entity],False):
                logging.debug('colPath=='+colPath)
                columns=columns+colPath.split(":->")[0].split('.')[1]+','
                typeCol=typeCol+colPath.split(":->")[1]+','
            if columns.endswith(','):
                columns=columns[:-1]
                typeCol=typeCol[:-1]
            self.PJ.UpdateKeyValue(planInstanceJSON,
                                   self.PJ.GetAllPaths(planInstanceJSON,
                                                       ["Info_Entity.Value",entity],False)[0].split("Info_Entity")[0]+"Info_Table_Columns.Value",
                                   columns
                                   )
            self.PJ.UpdateKeyValue(planInstanceJSON,
                                   self.PJ.GetAllPaths(planInstanceJSON,
                                                       ["Info_Entity.Value", entity],False)[0].split(
                                       "Info_Entity")[0] + "Info_Mapping",
                                   columns
                                   )
            self.PJ.UpdateKeyValue(planInstanceJSON,
                                   self.PJ.GetAllPaths(planInstanceJSON,
                                                       ["Info_Entity.Value", entity],False)[0].split(
                                       "Info_Entity")[0] + "Info_Type_Columns",
                                   typeCol
                                   )

        jsonData = json.dumps(planInstanceJSON, indent=3)
        f = open(newPathForInstance + "\\" + planInstanceID + "_with_values.txt", "w")
        f.write(jsonData)
        f.close()
        return planInstanceID

    def getMapping(self,planID):
        planInstanceJSONValues = self.PJ.LoadJSONFromFile(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                 "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt")
        columnsMap={}
        for path in self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Mapping"]):
            node = {"MappingCol": path.split(":->")[1],
                "TableCol": self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                        path.split("Info_Mapping")[
                                                            0] + "Info_Table_Columns.Value"),
                "TypeCol": self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                        path.split("Info_Mapping")[
                                                            0] + "Info_Type_Columns")}
            self.PJ.UpdateKeyValue(columnsMap,
                                   self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                               path.split("Info_Mapping")[
                                                                   0] + "Info_Entity.Value"),
                                   node)
        return json.dumps(columnsMap)

    def getStatusValidation(self,planID):
        planInstanceJSONValues = self.PJ.LoadJSONFromFile(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                  "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt")

        MappingStatusJSON=[]
        ValidationStatusJSON=[]
        TableNames=[]
        for path in self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Entity.Value"]):
            MappingStatusJSON.append({path.split(":->")[1]:self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                               path.split("Info_Entity")[0]+"Mapping")})
            ValidationStatusJSON.append({path.split(":->")[1]:self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                               path.split("Info_Entity")[0] + "Validation")})
            TableNames.append({path.split(":->")[1]:self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                               path.split("Info_Entity")[0] + "TableNames")})
        StatusJSON = {"MappingStatus": MappingStatusJSON, "ValidationStatus": ValidationStatusJSON,
                      "TableNames":TableNames}
        return StatusJSON

    def saveStatus(self,planID,mappingStatus,validationStatus,TableNames):
        planInstanceJSONValues = self.PJ.LoadJSONFromFile(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                  "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt")
        logging.debug('ABC')
        if mappingStatus!='NA':
            mappingStatusJSON= eval(mappingStatus)
            for path in self.PJ.GetAllPaths(mappingStatusJSON):
                logging.debug('pathmapping====='+path.split(":->")[1])

                entityPath= self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Entity.Value",path.split(":->")[0]],False)[0]
                logging.debug(entityPath.split("Info_Entity")[0] + "Mapping")
                self.PJ.UpdateKeyValue(planInstanceJSONValues,
                                       entityPath.split("Info_Entity")[0]+"Mapping",
                                       path.split(":->")[1])
        if validationStatus!='NA':
            validationStatusJSON= eval(validationStatus)
            for path in self.PJ.GetAllPaths(validationStatusJSON):
                logging.debug(path)
                entityPath= self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Entity.Value",path.split(":->")[0]],False)[0]
                self.PJ.UpdateKeyValue(planInstanceJSONValues,
                                       entityPath.split("Info_Entity")[0]+"Validation",
                                       path.split(":->")[1])
        if TableNames!='NA':
            TableNamesJSON= eval(TableNames)
            for path in self.PJ.GetAllPaths(TableNamesJSON):
                logging.debug(path)
                entityPath= self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Entity.Value",path.split(":->")[0]],False)[0]
                self.PJ.UpdateKeyValue(planInstanceJSONValues,
                                       entityPath.split("Info_Entity")[0]+"TableNames",
                                       path.split(":->")[1])

        logging.debug(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                  "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt")
        jsonData = json.dumps(planInstanceJSONValues, indent=3)
        f = open(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                  "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt", "w")
        f.write(jsonData)
        f.close()

    def getMappingValidationService(self,planID):
        planInstanceJSONValues = self.PJ.LoadJSONFromFile(self.PJ.GetValueFromKeyPath(self.configurationFile,
                                  "Instances_Folder.Plan") + "\\" + planID + '\\' + planID + "_with_values.txt")
        MappingJSON={"Parameters":{}}
        for path in self.PJ.GetAllPaths(planInstanceJSONValues,["Info_Mapping"]):
            mappingcols = path.split(':->')[1].split(',')
            tablecols = self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                    path.split("Info_Mapping")[0] + "Info_Table_Columns.Value")
            Mapping={}
            i=0
            for col in tablecols.split(','):
                self.PJ.UpdateKeyValue(Mapping,
                                       mappingcols[i],
                                       col
                                       )
                i=i+1
            self.PJ.UpdateKeyValue(MappingJSON,
                                   "Parameters."+self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                                             path.split("Info_Mapping")[0]+"Info_Entity.Value"),
                                   {"Mapping":Mapping,"FileName":self.PJ.GetValueFromKeyPath(planInstanceJSONValues,
                                                                             path.split("Info_Mapping")[0]+"Param_Object_Id")})
        return self.PJ.GetValueFromKeyPath(MappingJSON,"Parameters")

    def createExecuteInstance(self,oldID,newID):
        planInstanceFolderLocation =self.PJ.GetValueFromKeyPath(self.configurationFile,
                                                                                      "Instances_Folder.Plan")
        if not os.path.exists(planInstanceFolderLocation + "\\" + newID):
            os.makedirs(planInstanceFolderLocation + "\\" + newID)
        copy_tree(planInstanceFolderLocation + "\\" + oldID, planInstanceFolderLocation + "\\" + newID)
        os.rename(planInstanceFolderLocation + "\\" + newID + "\\" + oldID + "_with_values.txt",
                  planInstanceFolderLocation + "\\" + newID + "\\" + newID + "_with_values.txt")
        instanceWithValues = self.PJ.LoadJSONFromFile(planInstanceFolderLocation + "\\" +
                                                      newID + "\\" + newID + "_with_values.txt")
        newInstanceWithVales = {newID: self.PJ.GetValueFromKeyPath(instanceWithValues, oldID)}
        jsonData = json.dumps(newInstanceWithVales, indent=3)
        f = open(planInstanceFolderLocation + "\\" + newID + "\\" + newID + "_with_values.txt",
                 "w")
        f.write(jsonData)
        f.close()

    def saveMapping(self, planID, dataVal):
        logging.debug(type(dataVal))
        planInstanceFile = self.PJ.GetValueFromKeyPath(self.configurationFile, "Instances_Folder.Plan") \
                           + "\\" + planID + "\\"
        planInstanceJSONWithValues = self.PJ.LoadJSONFromFile(planInstanceFile + planID + "_with_values.txt")

        datastr = eval(dataVal)
        logging.debug(self.tenant+'---')
        logging.debug('============================')
        if self.tenant!='ART':
            for path in self.PJ.GetAllPaths(datastr, ["MappingCol"]):
                logging.debug('path==' + path)
                mappingPath = self.PJ.GetAllPaths(planInstanceJSONWithValues
                                                  , ["Resultants", "Tables", "Info_Table_id.Value",
                                                     path.split(".MappingCol")[0]])[0]
                logging.debug('to update' + mappingPath.split("Info_Table_id")[0] + "Info_Mapping")

                self.PJ.UpdateKeyValue(planInstanceJSONWithValues,
                                       mappingPath.split("Info_Table_id")[0] + "Info_Mapping",
                                       path.split(":->")[1])
        else:
            for path in self.PJ.GetAllPaths(datastr, ["MappingCol"]):
                logging.debug('path==' + path)
                mappingPath = self.PJ.GetAllPaths(planInstanceJSONWithValues
                                                  , ["Resultants", "Tables", "Info_Entity.Value",
                                                     path.split(".MappingCol")[0]])[0]
                logging.debug('to update' + mappingPath.split("Info_Entity")[0] + "Info_Mapping")

                self.PJ.UpdateKeyValue(planInstanceJSONWithValues,
                                       mappingPath.split("Info_Entity")[0] + "Info_Mapping",
                                       path.split(":->")[1])
        jsonData = json.dumps(planInstanceJSONWithValues, indent=3)
        f = open(planInstanceFile + planID + "_with_values.txt", "w")
        f.write(jsonData)
        f.close()

    def FillWithValuesFile(self, planInstanceID, UI_File_Name):
        planInstanceFolderLocation = self.PJ.GetValueFromKeyPath(self.configurationFile,
                                                                 "Instances_Folder.Plan") + "\\" + planInstanceID
        # if 'Plan' in UI_File_Name:
        #    copyfile(planInstanceFolderLocation+"\\"+planInstanceID+".txt" ,
        #         planInstanceFolderLocation+"\\"+planInstanceID+"_with_values.txt")
        planInstanceWithValue = self.PJ.LoadJSONFromFile(planInstanceFolderLocation + "\\" + planInstanceID +
                                                         "_with_values.txt")
        UIFile = self.PJ.LoadJSONFromFile(planInstanceFolderLocation + "\\" + UI_File_Name + ".txt")

        if 'Plan' in UI_File_Name or '_SC' in UI_File_Name:
            for path in self.PJ.GetAllPaths(UIFile, ["Parameters", "Path"]):
                self.PJ.UpdateKeyValue(planInstanceWithValue,
                                       planInstanceID + "." + path.split(":->")[1],
                                       self.PJ.GetValueFromKeyPath(UIFile, path.split("Path")[0] + "Value"))
        else:
            logging.debug('reached Save')
            componentName = UI_File_Name.split("UI_File_")[1]
            path = self.PJ.GetAllPaths(planInstanceWithValue,
                                       ["Plan_Components", componentName, "Info_Component_Version", "Value"])[0]
            for values in self.PJ.GetAllPaths(UIFile, ["Path"]):
                logging.debug('values=='+values)
                self.PJ.UpdateKeyValue(planInstanceWithValue,
                                       planInstanceID +
                                       ".Plan_Components[" + path.split("Plan_Components[")[1].split("].C_")[0]
                                       + "]." + componentName + "." + values.split(":->")[1],
                                       self.PJ.GetValueFromKeyPath(UIFile,
                                                                   values.split("Path")[0] + "Value"))

        jsonData = json.dumps(planInstanceWithValue, indent=3)
        f = open(planInstanceFolderLocation + "\\" + planInstanceID + "_with_values.txt", "w")
        f.write(jsonData)
        f.close()

    def fillValuesfromUIintoUIFile(self,data, planIDFolder, fileName):
        logging.debug(self.configurationFile)
        filePath = self.PJ.GetValueFromKeyPath(self.configurationFile, "Instances_Folder.Plan")
        originalData = self.PJ.LoadJSONFromFile(filePath +"\\"+ planIDFolder +"\\"+ fileName + ".txt")
        logging.debug("hereinUI")
        logging.debug(type(data))
        dataJSON=eval(data)
        for path in self.PJ.GetAllPaths(originalData, [".Path"]):
            for pathData in self.PJ.GetAllPaths(dataJSON, [path.split(":->")[1]]):
                if self.PJ.GetValueFromKeyPath(dataJSON, pathData.split(".Path")[0] + ".Value")!=None:
                    self.PJ.UpdateKeyValue(originalData, path.split(".Path")[0] + ".Value",
                                      self.PJ.GetValueFromKeyPath(dataJSON, pathData.split(".Path")[0] + ".Value"))
                    if self.PJ.GetAllPaths(originalData,[path.split(".Path")[0],".Name"])!=[]:

                        self.PJ.UpdateKeyValue(originalData, path.split(".Path")[0] + ".Name",
                                               self.PJ.GetValueFromKeyPath(dataJSON, pathData.split(".Path")[0] + ".Name"))
        return originalData


