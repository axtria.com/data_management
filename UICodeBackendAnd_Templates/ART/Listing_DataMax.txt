{
	"Parameters": [
		{
			"Category": "Roster",
			"Module": "ART",
			"Sub-Child": [
			{
				"Inputs": "1-Employee-Territory",
				"Data_Object": "Roster Info",
				"TemplateID": "Inbound_Full_Alignment"
		    }
			]
		},
		{
			"Category": "Master",
			"Module": "ART",
			"Sub-Child": [
			{
				"Inputs": "2-Accounts,Account Address",
				"Data_Object": "Account Universe",
				"TemplateID": "Inbound_Full_Alignment"
		    }
			]
		},
		{
			"Category": "Alignment",
			"Module": "ART",
			"Sub-Child": [
			{
				"Inputs": "3-Position Hierarchy,Cus-Terr,Zipp-Terr",
				"Data_Object": "Full Alignment",
				"TemplateID": "Inbound_Full_Alignment"
		    },
			{
				"Inputs": "1-Position Hierarchy",
				"Data_Object": "Position",
				"TemplateID": "Inbound_Full_Alignment"
		    },
			{
				"Inputs": "1-Cus-Terr",
				"Data_Object": "Account Alignment",
				"TemplateID": "Inbound_Full_Alignment"
		    },
			{
				"Inputs": "1-Zipp-Terr",
				"Data_Object": "Geography Alignment",
				"TemplateID": "Inbound_Full_Alignment"
		    }
			]
		}
	]
}