{
	"Info_Component_Version": {
		"Value": "Inbound_Account_Universe"
	},
	"Info_Template_Name": {
		"Value": "Inbound Account Universe"
	},
	"Param_Component_Name": {
		"Sequence": "1",
		"Label": "Name",
		"Value": "{Component_Name}",
		"Input": "Text",
		"Section": "Component Info__1",
		"Required": "True"
	},
	"Param_Module": {
		"Sequence": "2",
		"Label": "Module",
		"Value": "ART",
		"Input": "Text",
		"Section": "Component Info__1",
		"Required": "False"
	},
	"Param_Category": {
		"Sequence": "3",
		"Label": "Category",
		"Value": "Master",
		"Input": "Text",
		"Section": "Component Info__1",
		"Required": "False"
	},
	"Param_DataObject": {
		"Sequence": "4",
		"Label": "Data Objects",
		"Value": "Account Universe",
		"Input": "Text",
		"Section": "Component Info__1",
		"Required": "True"
	},
	"Param_Team": {
		"Sequence": "5",
		"Label": "Team",
		"Value": "{Param_Team}",
		"Section": "Component Info__1",
		"Input": "PickList_Team_Full_Alignment",
		"Required": "True"
	},
	"Param_Scenario": {
		"Sequence": "6",
		"Label": "Scenario",
		"Value": "{Scenario}",
		"Section": "Component Info__1",
		"Input": "PickList_Scenario_Full_Alignment",
		"Required": "True"
	},
	
	"Resultants": {
		"J_1": {
		  "Tables": [
		    {
				"Info_Entity": {
					"Value": "Account_Address"
				},
				"Param_Object_Id": {
					"Sequence": "1",
					"Label": "Account_Address",
					"Value": "{Position_hierarchy_Object_Id}",
					"Section": "Input files__3",
					"Required": "True",
					"Input": "Action_Account_Address",
					"Action": "Account_Address",
					"Name": ""
				},
				"Info_Table_Columns": {
					"Value": "Account_Number,Address_Type,City,Country,External_ID,State,Staus,Street"
				},
				"Info_Type_Columns":"Text,Text,Text,Text,Text,Text,Text,Text",
				"Info_Mapping": "Account_Number,Address_Type,City,Country,External_ID,State,Staus,Street",
				"Validation":"",
				"Mapping":"",
				"TableNames":""
			 
			},
			{
				"Info_Entity": {
					"Value": "Accounts"
				},
				"Param_Object_Id": {
					"Sequence": "2",
					"Label": "Accounts",
					"Value": "{Position_hierarchy_Object_Id}",
					"Section": "Input files__3",
					"Required": "True",
					"Input": "Action_Accounts",
					"Action": "Accounts",
					"Name": ""
				},
				"Info_Table_Columns": {
					"Value": "AccountNumber,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,BillingLatitude,BillingLongitude,First_Name,Last_Name,Middle_Name,Speciality,External_Account_Number,Account_Type,Parent_Account_Number,Parent_External_Account_Number,Country"
				},
				"Info_Type_Columns":"Number,Text,Text,Text,Text,Number,Text,Number,Number,Text,Text,Text,Text,Number,Text,Number,Number,Text",
				"Info_Mapping": "AccountNumber,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,BillingLatitude,BillingLongitude,First_Name,Last_Name,Middle_Name,Speciality,External_Account_Number,Account_Type,Parent_Account_Number,Parent_External_Account_Number,Country",
				"Validation":"",
				"Mapping":"",
				"TableNames":""
			 
			}]
		}
    }    
}