{
   "SC_ID_12f63446-f109-11e9-b7c5-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Intermodule"
      },
      "Info_Template_Name": {
         "Value": "Intermodule"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "From",
         "Value": "ART",
         "Input": "PickList_Module",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_To": {
         "Sequence": "3",
         "Label": "To",
         "Value": "ART",
         "Input": "PickList_Module",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "{}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "Action_Team_Intermodule",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "4",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "Action_Scenario_Intermodule",
         "Required": "True"
      },
      "Param_Time_Period": {
         "Sequence": "4",
         "Label": "Time Period",
         "Value": "{Param_Time_Period}",
         "Section": "Component Info__1",
         "Input": "Calender",
         "Required": "True"
      }
   }
}