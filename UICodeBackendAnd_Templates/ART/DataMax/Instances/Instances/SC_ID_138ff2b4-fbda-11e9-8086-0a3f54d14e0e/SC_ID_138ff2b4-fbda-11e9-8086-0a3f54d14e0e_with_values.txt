{
   "SC_ID_138ff2b4-fbda-11e9-8086-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Geography_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Geography Alignment"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Geography_Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Zipp-Terr"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Zipp-Terr",
                     "Value": "{Zipp-Terr}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Zipp-Terr",
                     "Action": "Zipp-Terr",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}