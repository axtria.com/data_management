{
   "SC_ID_1a593da8-f54f-11e9-9da9-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Full_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Full Alignment"
      },
      "Param_Component_Name": "Ankit_Test_1",
      "Param_Module": "ART",
      "Param_Category": "Alignment",
      "Param_DataObject": "Full_Alignment",
      "Param_Team": "Team1",
      "Param_Scenario": "Team1Scenario",
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Position_hierarchy"
                  },
                  "Param_Object_Id": "Scenario_Input/accnt_algnmnt_a1Y1N000004XFBJUA4.csv",
                  "Info_Table_Columns": {
                     "Value": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Date,Date,Text,Text,Text,Text,Text,Text",
                  "Info_Mapping": "accountnumber,accounttype,billinglongitude,billinglatitude,active,firstname,first_name,billingstreet,firstname",
                  "Validation": "",
                  "Mapping": "Mapping Failed",
                  "TableNames": ""
               },
               {
                  "Info_Entity": {
                     "Value": "Cus-Terr"
                  },
                  "Param_Object_Id": "Scenario_Input/accnt_algnmnt_a1Y1N000004XFBJUA4.csv",
                  "Info_Table_Columns": {
                     "Value": "Account,Account_Alignment_Type,Affiliation_Based_Alignment,Change_Status,Effective_End_Date,Effective_Start_Date,IsShared,Metric1,Metric_1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Text,Date,Date,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Account,Account_Alignment_Type,Affiliation_Based_Alignment,Change_Status,Effective_End_Date,Effective_Start_Date,IsShared,Metric1,Metric_1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Team_Instance",
                  "Validation": "",
                  "Mapping": "Mapping Failed",
                  "TableNames": ""
               },
               {
                  "Info_Entity": {
                     "Value": "Zipp-Terr"
                  },
                  "Param_Object_Id": "Scenario_Input/accnt_algnmnt_a1Y1N000004XFBJUA4.csv",
                  "Info_Table_Columns": {
                     "Value": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "accountnumber,external_account_number,billinglongitude,billingcity,billinglongitude,billinglatitude,billingstate,alignment_type,first_name,billinggeocodeaccuracy,billingaddress,billinggeocodeaccuracy,billingpostalcode,billingcity,accounttype",
                  "Validation": "",
                  "Mapping": "Manually Mapped",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}