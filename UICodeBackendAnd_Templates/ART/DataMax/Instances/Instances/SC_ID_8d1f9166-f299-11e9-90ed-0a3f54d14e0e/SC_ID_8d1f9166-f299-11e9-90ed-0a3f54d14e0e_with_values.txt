{
   "SC_ID_8d1f9166-f299-11e9-90ed-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Full_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Full Alignment"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Full_Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Position_hierarchy"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Position_hierarchy",
                     "Value": "{Position_hierarchy_Object_Id}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Position_hierarchy",
                     "Action": "Position_hierarchy",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Date,Date,Text,Text,Text,Text,Text,Text",
                  "Info_Mapping": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               },
               {
                  "Info_Entity": {
                     "Value": "Cus-Terr"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Cus-Terr",
                     "Value": "{Cus-Terr}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Cus-Terr",
                     "Action": "Cus-Terr",
                     "Name": " "
                  },
                  "Info_Table_Columns": {
                     "Value": "Account,Account_Alignment_Type,Affiliation_Based_Alignment,Change_Status,Effective_End_Date,Effective_Start_Date,IsShared,Metric1,Metric_1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Text,Date,Date,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Account,Account_Alignment_Type,Affiliation_Based_Alignment,Change_Status,Effective_End_Date,Effective_Start_Date,IsShared,Metric1,Metric_1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Team_Instance",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               },
               {
                  "Info_Entity": {
                     "Value": "Zipp-Terr"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Zipp-Terr",
                     "Value": "{Zipp-Terr}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Zipp-Terr",
                     "Action": "Zipp-Terr",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}