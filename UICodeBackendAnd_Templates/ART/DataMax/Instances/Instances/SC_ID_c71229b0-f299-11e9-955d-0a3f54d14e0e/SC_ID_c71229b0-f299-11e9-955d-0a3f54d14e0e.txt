{
   "SC_ID_c71229b0-f299-11e9-955d-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Employee_Territory"
      },
      "Info_Template_Name": {
         "Value": "Inbound Employee_Territory"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Roster Info",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Employee-Territory",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Employee-Territory"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Employee-Territory",
                     "Value": "{Position_hierarchy_Object_Id}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Employee-Territory",
                     "Action": "Employee-Territory",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Assignment_Type,Effective_End_Date,Effective_Start_Date,Employee,Position,Reason_Code"
                  },
                  "Info_Type_Columns": "Text,Date,Date,Text,Text,Text",
                  "Info_Mapping": "Assignment_Type,Effective_End_Date,Effective_Start_Date,Employee,Position,Reason_Code",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}