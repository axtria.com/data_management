{
   "SC_ID_04f5419c-fc99-11e9-9416-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Geography_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Geography Alignment"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Geography_Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Zipp-Terr"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Zipp-Terr",
                     "Value": "{Zipp-Terr}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Zipp-Terr",
                     "Action": "Zipp-Terr",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Team_name__c,Team_Instance_Name,EFFECTIVE_END_DATE__C, EFFECTIVE_START_DATE__C , NAME , Territory_ID__C , POSITION__R_NAME , CENTROID__LATITUDE__S , CENTROID__LONGITUDE__S , CITY__C , METRIC1__C , METRIC2__C , METRIC3__C , METRIC4__C , METRIC5__C , METRIC6__C , METRIC7__C , METRIC8__C, METRIC9__C , METRIC10__C , METRIC11__C , PARENT_ZIP_CODE__R_NAME , STATE__C, X_MAX__C , X_MIN__C , Y_MAX__C , Y_MIN__C , ZIP_NAME__C , ZIP_TYPE__C, Geo_type__c"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}