{
   "SC_ID_d6fdddca-f24f-11e9-ba66-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Intermodule"
      },
      "Info_Template_Name": {
         "Value": "Intermodule"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Navigate From ->To",
         "Value": "ART",
         "Input": "PickList_Module",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "{}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "Action_Team_Intermodule",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "4",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "Action_Scenario_Intermodule",
         "Required": "True"
      }
   }
}