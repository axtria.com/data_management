{
	"Parameters": [{
			"Params": [{
					"Sequence": "1",
					"Required": "True",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Zipp-Terr",
					"Path": "Resultants.J_1.Tables[0].Param_Object_Id",
					"Value": "{Zipp-Terr}",
					"Type": "Action_Zipp-Terr",
					"Action": "Zipp-Terr",
					"Name": " ",
					"Options": "NA"
				}
			],
			"Name": "Input files",
			"Tab_Seq": "3"
		},
		{
			"Params": [{
					"Sequence": "3",
					"Required": "False",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Category",
					"Path": "Param_Category",
					"Value": "Alignment",
					"Type": "Text",
					"Action": "NA",
					"Name": " ",
					"Options": "NA",
					"SFDC":"Category__c"
				},
				{
					"Sequence": "6",
					"Required": "True",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Scenario",
					"Path": "Param_Scenario",
					"Value": "{Scenario}",
					"Type": "PickList_Scenario_Full_Alignment",
					"Action": "NA",
					"Name": " ",
					"Options": "Team1Scenario"

				},
				{
					"Sequence": "2",
					"Required": "False",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Module",
					"Path": "Param_Module",
					"Value": "ART",
					"Type": "Text",
					"Action": "NA",
					"Name": " ",
					"Options": "NA",
					"SFDC" :"Module__c"
				},
				{
					"Sequence": "5",
					"Required": "True",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Team",
					"Path": "Param_Team",
					"Value": "{Param_Team}",
					"Type": "PickList_Team_Full_Alignment",
					"Action": "NA",
					"Name": " ",
					"Options": "Team1"
				},
				{
					"Sequence": "4",
					"Required": "True",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Data Objects",
					"Path": "Param_DataObject",
					"Value": "Geography_Alignment",
					"Type": "Text",
					"Action": "NA",
					"Name": " ",
					"Options": "NA",
					"SFDC":"Data_Object__c"
				},
				{
					"Sequence": "1",
					"Required": "True",
					"Queries": "NA",
					"Onchange": "NA",
					"Business_Term": "Name",
					"Path": "Param_Component_Name",
					"Value": "{Component_Name}",
					"Type": "Text",
					"Action": "NA",
					"Name": " ",
					"Options": "NA",
					"SFDC": "Name"
				}
			],
			"Name": "Component Info",
			"Tab_Seq": "1"
		}
	]
}