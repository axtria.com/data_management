{
   "SC_ID_1bf30422-fca3-11e9-ab92-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Account_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Account Alignment"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Account_Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Cus-Terr"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Cus-Terr",
                     "Value": "{Cus-Terr}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Cus-Terr",
                     "Action": "Cus-Terr",
                     "Name": " "
                  },
                  "Info_Table_Columns": {
                     "Value": "Team_name__c,Team_Instance_Name,Account_Alignment_Type__c,EFFECTIVE_END_DATE__C ,EFFECTIVE_START_DATE__C ,Team_Type,AccountNumber__c ,Territory_ID__C ,POSITION__R_NAME ,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,BillingLatitude ,BillingLongitude ,FirstName__c,LastName__c,Metric1__c ,Metric2__c,Metric3__c ,Metric4__c ,Metric5__c ,Metric6__c,Metric7__c ,Metric8__c,Metric9__c,Metric10__c,Metric11__c ,MiddleName__c,No_of_Target__c,Picklist1__c,Picklist2__c ,Segment1__c ,Segment2__c,Segment3__c,Segment4__c,Segment5__c,Speciality__c,Checkbox10__c,Checkbox1__c,Checkbox2__c ,Checkbox3__c ,Checkbox4__c ,Checkbox5__c ,Checkbox6__c ,Checkbox7__c ,Checkbox8__c ,Checkbox9__c ,Segment10__c,Segment6__c,Segment7__c,Segment8__c,Segment9__c,External_Account_Number__c"
                  },
                  "Info_Type_Columns": "varchar, varchar, varchar, date, date, integer, integer, varchar, varchar, varchar, varchar, varchar, varchar, integer, varchar, float, float, varchar, varchar, float, integer, integer, float, float, integer, integer, varchar, integer, integer, integer, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, varchar, integer",
                  "Info_Mapping": "Team_name__c,Team_Instance_Name,Account_Alignment_Type__c,EFFECTIVE_END_DATE__C ,EFFECTIVE_START_DATE__C ,Team_Type,AccountNumber__c ,Territory_ID__C ,POSITION__R_NAME ,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,BillingLatitude ,BillingLongitude ,FirstName__c,LastName__c,Metric1__c ,Metric2__c,Metric3__c ,Metric4__c ,Metric5__c ,Metric6__c,Metric7__c ,Metric8__c,Metric9__c,Metric10__c,Metric11__c ,MiddleName__c,No_of_Target__c,Picklist1__c,Picklist2__c ,Segment1__c ,Segment2__c,Segment3__c,Segment4__c,Segment5__c,Speciality__c,Checkbox10__c,Checkbox1__c,Checkbox2__c ,Checkbox3__c ,Checkbox4__c ,Checkbox5__c ,Checkbox6__c ,Checkbox7__c ,Checkbox8__c ,Checkbox9__c ,Segment10__c,Segment6__c,Segment7__c,Segment8__c,Segment9__c,External_Account_Number__c",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}