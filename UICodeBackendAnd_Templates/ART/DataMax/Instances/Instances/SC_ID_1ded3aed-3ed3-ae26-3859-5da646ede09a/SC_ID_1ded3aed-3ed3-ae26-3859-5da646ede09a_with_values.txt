{
   "SC_ID_1ded3aed-3ed3-ae26-3859-5da646ede09a": {
      "Info_Component_Version": {
         "Value": "Inbound_Full_Alignment"
      },
      "Info_Template_Name": {
         "Value": "Inbound Full Alignment"
      },
      "Param_Component_Name": "Alignment_1",
      "Param_Module": "ART",
      "Param_Category": "Alignment",
      "Param_DataObject": "Full Alignment",
      "Param_Team": "Team1",
      "Param_Scenario": "Team1Scenario",
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Table_Label": {
                     "Value": "DS1"
                  },
                  "Info_Table_id": {
                     "Value": "DS1"
                  },
                  "Info_Output": {
                     "Value": "False"
                  },
                  "Info_Entity": {
                     "Value": "Position_hierarchy"
                  },
                  "Param_Object_Id": "call_plan_summ_scenario_1136.csv",
                  "Info_Table_Columns": {
                     "Value": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Date,Date,Text,Text,Text,Text,Text,Text",
                  "Info_Mapping": "Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code,Client_Position_Code",
                  "Validation": "",
                  "Mapping": "Linked"
               },
               {
                  "Info_Table_Label": {
                     "Value": "DS2"
                  },
                  "Info_Table_id": {
                     "Value": "DS2"
                  },
                  "Info_Output": {
                     "Value": "False"
                  },
                  "Info_Entity": {
                     "Value": "Cus-Terr"
                  },
                  "Param_Object_Id": "call_plan_summ_scenario_1136.csv",
                  "Info_Table_Columns": {
                     "Value": "Account,Account_Alignment_Type,Affiliation_Based_Alignment,Change_Status,Effective_End_Date,Effective_Start_Date,IsShared,Metric1,Metric_1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Team_Instance"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Text,Date,Date,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account,Account",
                  "Validation": "",
                  "Mapping": "Linked"
               },
               {
                  "Info_Table_Label": {
                     "Value": "DS3"
                  },
                  "Info_Table_id": {
                     "Value": "DS3"
                  },
                  "Info_Output": {
                     "Value": "False"
                  },
                  "Info_Entity": {
                     "Value": "Zipp-Terr"
                  },
                  "Param_Object_Id": "call_plan_summ_scenario_1136.csv",
                  "Info_Table_Columns": {
                     "Value": "Change_Status,Effective_Start_Date,Effective_End_Date,Metric1,Metric10,Metric2,Metric3,Metric4,Metric5,Metric6,Metric7,Metric8,Metric9,Position,Restrict_Position_Geography_Trigger"
                  },
                  "Info_Type_Columns": "Text,Text,Text,Number,Number,Number,Number,Number,Number,Number,Number,Number,Number,Text,Text",
                  "Info_Mapping": "Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status,Change_Status",
                  "Validation": "",
                  "Mapping": "Linked"
               }
            ]
         }
      }
   }
}