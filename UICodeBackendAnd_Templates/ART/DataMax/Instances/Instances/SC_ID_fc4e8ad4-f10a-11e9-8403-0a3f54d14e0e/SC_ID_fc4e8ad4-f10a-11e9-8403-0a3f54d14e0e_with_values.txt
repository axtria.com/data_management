{
   "SC_ID_fc4e8ad4-f10a-11e9-8403-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Outbound"
      },
      "Info_Template_Name": {
         "Value": "Outbound"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Navigate From ->To",
         "Value": "ART",
         "Input": "PickList_Module",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_S3_Path": {
         "Sequence": "3",
         "Label": "S3 Path",
         "Value": "{}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "{}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "Action_Team_Outbound",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "4",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "Action_Scenario_Outbound",
         "Required": "True"
      }
   }
}