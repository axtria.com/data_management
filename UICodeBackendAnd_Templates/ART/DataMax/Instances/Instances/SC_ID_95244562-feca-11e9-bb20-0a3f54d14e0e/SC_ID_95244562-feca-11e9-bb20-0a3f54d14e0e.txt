{
   "SC_ID_95244562-feca-11e9-bb20-0a3f54d14e0e": {
      "Info_Component_Version": {
         "Value": "Inbound_Position"
      },
      "Info_Template_Name": {
         "Value": "Inbound Position"
      },
      "Param_Component_Name": {
         "Sequence": "1",
         "Label": "Name",
         "Value": "{Component_Name}",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Module": {
         "Sequence": "2",
         "Label": "Module",
         "Value": "ART",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_Category": {
         "Sequence": "3",
         "Label": "Category",
         "Value": "Alignment",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "False"
      },
      "Param_DataObject": {
         "Sequence": "4",
         "Label": "Data Objects",
         "Value": "Position",
         "Input": "Text",
         "Section": "Component Info__1",
         "Required": "True"
      },
      "Param_Team": {
         "Sequence": "5",
         "Label": "Team",
         "Value": "{Param_Team}",
         "Section": "Component Info__1",
         "Input": "PickList_Team_Full_Alignment",
         "Required": "True"
      },
      "Param_Scenario": {
         "Sequence": "6",
         "Label": "Scenario",
         "Value": "{Scenario}",
         "Section": "Component Info__1",
         "Input": "PickList_Scenario_Full_Alignment",
         "Required": "True"
      },
      "Resultants": {
         "J_1": {
            "Tables": [
               {
                  "Info_Entity": {
                     "Value": "Position_hierarchy"
                  },
                  "Param_Object_Id": {
                     "Sequence": "1",
                     "Label": "Position_hierarchy",
                     "Value": "{Position_hierarchy_Object_Id}",
                     "Section": "Input files__3",
                     "Required": "True",
                     "Input": "Action_Position_hierarchy",
                     "Action": "Position_hierarchy",
                     "Name": ""
                  },
                  "Info_Table_Columns": {
                     "Value": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance,Lookup_Team_Name,Lookup_Team_Instance_name,Team_ID,Related_position_type,parent_position_name,Assignment_status"
                  },
                  "Info_Type_Columns": "Text,Date,Date,Text,Text,Text,Text,Text,Text,Text,Text,Text,Text,Text,Text",
                  "Info_Mapping": "Client_Position_Code,Effective_End_Date,Effective_Start_Date,Hierarchy_Level,Parent_Position_Code,Position_Type,RGB,NAME,Team_Instance,Lookup_Team_Name,Lookup_Team_Instance_name,Team_ID,Related_position_type,parent_position_name,Assignment_status",
                  "Validation": "",
                  "Mapping": "",
                  "TableNames": ""
               }
            ]
         }
      }
   }
}