import logging
import pandas as pd

from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver.support import expected_conditions as EC

option = Options()

option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
option.add_experimental_option("prefs", { 
    "profile.default_content_setting_values.notifications": 2
})

browser = webdriver.Chrome(options=option,
                           executable_path=r'C:\DataManagement\Selenium\chromedriver.exe')
def sf_login():
    with open(r"C:\DataManagement\configurationFiles\creds.json") as f:
        creds = json.load(f)
        browser.get("https://login.salesforce.com")
        user = browser.find_element_by_id("username")
        user.send_keys(creds['salesforce']['username'])
        password = browser.find_element_by_id("password")
        password.send_keys(creds['salesforce']['password'])
        password.send_keys(Keys.ENTER)

sf_login()
Alignment_xpath = ".//one-app-nav-bar-item-root[@class='navItem slds-context-bar__item slds-shrink-none'][a[span[text()='Alignments']]]"
WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.XPATH, Alignment_xpath)))
browser.find_element_by_xpath(Alignment_xpath).click()
sleep(10)
browser.switch_to.default_content()
browser.switch_to.frame(0)
WebDriverWait(browser, 100).until(EC.presence_of_element_located((By.CSS_SELECTOR, "[title='List View']")))
browser.find_element_by_css_selector("[title='List View']").click()
browser.find_element_by_id("dropdownMenuButton").click()
browser.find_element_by_css_selector("[title='Account']").click()
df = pd.read_csv(r"C:\Users\shashank.makkar\Downloads\Bulk_Account_Movement.csv")
df["Source_to_Dest"] = df["Source Position"] + "_to_" + df["Destination Position"] + "_to_" + df["Reason Code"]
for movement in df["Source_to_Dest"].unique():
    source_pos, dest_pos, reason = movement.split('_to_')
    e = browser.find_element_by_css_selector(".flex-item_custom.flex-item_rest")
    e.find_element_by_css_selector("[title='Tree Hierarchy']").click()
    browser.find_element_by_partial_link_text(source_pos).click()
#    el = browser.find_element_by_id("1NN00000")
#    el.find_element_by_css_selector("[title='Select to Toggle Visibility']").click()
#    el.find_element_by_css_selector("[title='Select to Toggle Visibility']").click()
#    el = browser.find_element_by_id(terrcode)
#    el.find_element_by_css_selector("[title='Select to Toggle Visibility']").click()
    browser.find_element_by_css_selector("[title='Tree Hierarchy']").click()

#    browser.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[1]/input")
    #browser.find_element_by_xpath("//input[@class='input-large search-query slds-input ng-pristine ng-untouched ng-valid ng-empty']").send_keys(' ').clear()
    accs = df.loc[df["Source_to_Dest"] == movement] 
    # logging.debug(accs.shape[0])
    for idx, row in accs.iterrows():
            browser.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[1]/input").send_keys(row['Account Number'])
            try:
                browser.find_element_by_xpath("//input[@class='getCheckbox']").click()
            except:
                logging.warning("Account {} not found for territory {}".format(row["Account Number"], source_pos))
            browser.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[1]/div[1]/input").clear()
#    browser.find_element_by_css_selector("title=['Move']").click()
    browser.find_element_by_xpath("//button[@title='Move']").click()
    sleep(1)
    browser.find_element_by_xpath("/html/body/div[1]/div[5]/div/div[3]/div[6]/div/div[1]").click()
#    browser.find_element_by_id("account-tab").click()
#    browser.find_element_by_css_selector("title=['dropDown_chosen']").click()
    browser.find_element_by_id('dropDown_chosen').click()
#    browser.find_element_by_id("chsn-search-Icn").send_keys(movement.split("_to_")[1])
    browser.find_element_by_xpath("//input[@class='chosen-search-input']").send_keys(dest_pos)
#    browser.find_element_by_partial_link_text(movement.split("_to_")[1]).click()
    browser.find_element_by_xpath("//li[@class='active-result highlighted']").click()
    browser.find_element_by_xpath("//button[@title='Submit']").click()
    browser.find_element_by_id("reasonCodeDropDown").click()
    browser.find_element_by_xpath("//option[@value='{}']".format(reason)).click()
    browser.find_element_by_xpath('//*[@id="changeRequestModal"]/div/div/footer/div/button[3]').click()
    sleep(2)
    try:
        browser.find_element_by_xpath('//*[@id="changeRequestModal"]/div/div/footer/div/button[4]').click()
    except ElementNotInteractableException:
        browser.find_element_by_xpath('//*[@id="changeRequestModal"]/div/div/footer/div/button[4]').click()
    sleep(10)