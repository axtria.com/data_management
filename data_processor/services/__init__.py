#   datamax/api/services/__init__.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import importlib
import logging
from abc import abstractmethod, ABCMeta
from inspect import signature

from flask import jsonify

from utils.exceptions import CustomException
from utils.helpers import timer
from vendor.appdb import AppDb


class Service(metaclass=ABCMeta):
    def __init__(self):
        """

        """
        self.log = logging.getLogger(self.__class__.__name__)
        self.response = {}

    @abstractmethod
    def execute(self):
        pass

    def add_response(self, key, value):
        self.response[key] = value

    def get_response(self, key):
        return self.response.get(key)


class ServiceFactory:
    def __init__(self, module_name: str, service_name: str, request, response=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.module_name = module_name
        self.service_name = service_name
        self.params = request.args.to_dict(flat=True)
        self.params.update(request.form.to_dict(flat=True))
        if response:
            self.params.update(response)

    @timer
    def invoke(self):
        self.log.debug("+++Executing+++ " + self.module_name + "." + self.service_name)
        session = AppDb()
        module = importlib.import_module("services." + self.module_name)
        try:
            service = getattr(module, self.service_name)(session=session, **self.params)
            service.execute()
            return jsonify(service.response)
        except TypeError as e:
            self.log.exception(e)
            return jsonify({"message": "Missing or Invalid parameters. Expected signature: " +
                                       str(signature(getattr(module, self.service_name))),
                            "traceback": str(e)}), 500
        except CustomException as e:
            return jsonify({"message": e.message, "traceback": str(e)}), 500
        finally:
            self.log.debug('+++Closing+++ connection for ' + self.service_name)
