#### Data Management - Data Processor

##### Getting started:

##### Prerequisites:
* Python 3.7
* Packages are mentioned in [requirements.txt](./requirements.txt)

##### Installing

* Copy the file `config.sample` to `config.py`.
* Copy the folder `data_sample` to your customized name.
* Make sure the customized folder has the following folders:
    * workflow
    * phrases
    * workflow_validation
    * phrases_validation
* Also, the customized folder should have the WRITE permission.
* Changes in `config.py`
  * Add the above customized name in the `config.py` under `DATABASE_PATH` 
  * Rename the prefix to the deployment (IIS) application name.

##### Running the tests

* install `pytest` package.
* in cmd, run the following command.
```bat
 data_processor>  pytest 
```

##### Deployment

* Recommended deployment on IIS server.
* `prefix` to be set unique in the `config.py` when compared with other installed applications.

##### Built with

```bat
 data_processor> python app.py
```

##### Contributing guidelines


##### Authors

* Akshay Saini <akshay.saini@axtria.com>
* Varun Singhal <varun.singhal@axtria.com>

##### Versioning
