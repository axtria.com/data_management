#   data_management/controller/phrases.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request

from services import ServiceFactory

phrases = Blueprint('phrases', __name__)


@phrases.route('/')
def index():
    """Phrases Engine.
    """
    return "Phrases Engine"


@phrases.route('/add')
def add_phrases():
    """Add phrases in the system.

    Parameters:
        component_name (str): unique component name
        phrases (list): comma separated list of phrases.

    Returns:
        dict: a JSON object which has the structured definition of the added component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases/add?component_name=Filter&phrases=["filter Sales'
        ...              'using columns c1, c2", "filter Sales using columns c1, c2 where(c2 > 5)", '
        ...              '"Filter RS1 where AND((c3 > 5),(c2 = ''BRAND''))", "Filter DS1 where OR(AND((c3 > 5)'
        ...              ',(c2 = ''BRAND'')),(c5 = ''segment_1''))"]')
        {
          "component_name": "Filter",
          "phrases": [
            "filter Sales using columns c1, c2",
            "filter Sales using columns c1, c2 where(c2 > 5)",
            "Filter RS1 where AND((c3 > 5),(c2 = 'BRAND'))",
            "Filter DS1 where OR(AND((c3 > 5),(c2 = 'BRAND')),(c5 = 'segment_1'))"
          ]
        }

    """
    service = ServiceFactory('phrases', 'AddPhrase', request)
    return service.invoke()


@phrases.route('/update')
def update_phrases():
    """Update phrases in the system.

    Parameters:
        component_name (str): unique component name
        phrases (list): comma separated list of phrases.

    Returns:
        dict: a JSON object which has the structured definition of the updated component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases/update?component_name=Filter&phrases=["filter Sales'
        ...              'using columns c1, c2", "filter Sales using columns c1, c2 where(c2 > 5)", '
        ...              '"Filter RS1 where AND((c3 > 5),(c2 = ''BRAND''))", "Filter DS1 where OR(AND((c3 > 5)'
        ...              ',(c2 = ''BRAND'')),(c5 = ''segment_1''))"]')
        {
          "component_name": "Filter",
          "phrases": [
            "filter Sales using columns c1, c2",
            "filter Sales using columns c1, c2 where(c2 > 5)",
            "Filter RS1 where AND((c3 > 5),(c2 = 'BRAND'))",
            "Filter DS1 where OR(AND((c3 > 5),(c2 = 'BRAND')),(c5 = 'segment_1'))"
          ]
        }

    """
    service = ServiceFactory('phrases', 'UpdatePhrase', request)
    return service.invoke()


@phrases.route('/delete')
def delete_phrases():
    """Delete phrases for a given component.

    Parameters:
        component_name (str): unique component name

    Returns:
        dict: a JSON object which has the message.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases/delete?component_name=Filter')
        {
          "message": "Deleted successfully"
        }

    """
    service = ServiceFactory('phrases', 'DeletePhrase', request)
    return service.invoke()


@phrases.route('/get')
def get_phrases():
    """Get phrases of the component.

    Parameters:
        component_name (str): unique component name

    Returns:
        dict: a JSON object which has the structured definition of the requested component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases/get?component_name=Filter')
        {
          "component_name": "Filter",
          "phrases": [
            "filter Sales using columns c1, c2",
            "filter Sales using columns c1, c2 where(c2 > 5)",
            "Filter RS1 where AND((c3 > 5),(c2 = 'BRAND'))",
            "Filter DS1 where OR(AND((c3 > 5),(c2 = 'BRAND')),(c5 = 'segment_1'))"
          ]
        }

    """
    service = ServiceFactory('phrases', 'GetPhrase', request)
    return service.invoke()


@phrases.route('/list')
def list_phrases():
    """List all the file adaptor definitions from the system.

     Returns:
         dict: a JSON object which has all the adaptor names and their meta data.

     Examples:
         >>> import requests
         >>> requests.get('http://localhost:5001/api/phrases/list')
         {
          "phrases": [
            {
              "name": "Filter",
              "phrases": [
                ....
                ]
            },
            {
                "name": "Aggregate",
                "phrases": [
                    ...
                ]
            }
            ]
         }

     """
    service = ServiceFactory('phrases', 'ListPhrase', request)
    return service.invoke()
