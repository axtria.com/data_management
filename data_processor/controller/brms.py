#   data_management/controller/brms.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>

"""Business Rule Management API.

Rules can be thought of as declarative statements of fact - both simple and complex.
Rules can produce outcomes ranging from a simple yes/no decision to all the way to complex
scoring based on multiple criteria.

Type of rules
    - Statements
    - Complex statement - tabular input
    - Sequential
    - Expression (derived columns)

See Also:

    :func:`~controller.exchange` API Exchange module to carry out database operations.

Notes:
    To allow construction of rules by the non-programmers.
    A rule designer that gives a text area for a person to type in statements -
    is really a programming environment, not a business user-focused tool.
"""
from flask import Blueprint, request

from services import ServiceFactory

brms = Blueprint('brms', __name__)


@brms.route('/')
def index():
    """BRMS Engine.
    """
    return "BRMS Engine"


@brms.route('/create_workflow')
def create_workflow():
    """Create workflow for the BRMS.

    Workflow is defined for a specific workspace which is used to align them with the project context.
    It is required to select the file adaptors for the workflow to enable polling of metadata
    (column names and their data type) from  in-application database.

    Each workflow is a template of rules in the saved state.

    Parameters:
        workspace_id (int): Workspace id for logical separation of workflows.

    Returns:
        dict: a JSON object which has the ``workflow_id`` generated for the given request.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/create_workflow?workspace_id=1')
        {
          "workflow_id": "46a0aea31273498a896b7761561c34de"
        }
    """
    service = ServiceFactory('brms', 'CreateWorkflow', request)
    return service.invoke()


@brms.route('/add_workflow_meta')
def add_workflow_meta():
    """Add meta information to the workflow.

    Meta information has the following attributes:
        - name: name of the workflow
        - description: description of the workflow
        - status: can be DRAFT or PUBLISHED
        - workflow_id: string based unique Id
        - workspace_id: workspace identifier

    Structure of the selected_datasets to be received is::

        [{"name": "adaptor_name", "adaptor_meta": [{"column_name": "c1", "data_type": "integer"}, { ... }]},
         {"name": "adaptor_name", "adaptor_meta": [ ... ]}]

    Parameters:
        workflow_id (str): unique Id of the workflow
        name (str): name of the workflow
        description (str): description of the workflow
        selected_datasets (list): list of dict with key names as adaptor names and value as list of
        column names mentioned above

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/add_workflow_meta?name=hello&description=some%20'
        ...              'desc&workflow_id=46a0aea31273498a896b7761561c34de'
        ...              '&selected_datasets=[{"name": "ds1", "adaptor_meta": [{"column_name": "c1",'
        ...              '"data_type": "integer"}, {"column_name": "c2", "data_type": "varchar"}]},'
        ...              '{"name": "ds2", "adaptor_meta": [{"column_name": "c2", "data_type": "varchar"}]}]')
        {
          "DATASETS": {
             ...
          },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 2
          },
          "PARAMETERS": {},
          "RULES": [],
          "SELECTED_DATASETS": {
            ...
          }
        }

    """
    service = ServiceFactory('brms', 'AddWorkflowMeta', request)
    return service.invoke()


@brms.route('/add_rule')
def add_rule():
    """Add a rule to the workflow.

    Each business rule in BRMS is a component. All components have the following schema:
        * Rule: which carries the parameters required for prepared SQL query
        * Input: defines the metadata of the input dataset
        * Output: defines the metadata of the output dataset

    All components can be configured using text input.

    Parameters:
        workflow_id (str): unique Id of the workflow
        rule_name (str): business name given to the rule
        output (str): table in which output has to be populated
        user_input (str): user text based on components being configured in the system.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.

    Examples:
        Add a rule without a parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/add_rule?output=rs1&rule_name=first%20rule'
        ...              '&user_input=filter%20ds1%20where%20(c3%20>%200)'
        ...              '&workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 2
          },
          "PARAMETERS": {},
          "RULES": [
            {
              "filter": {
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "0"
                  }
                ]
              },
              "input": "ds1",
              "output": "rs1",
              "parameters": [],
              "query": "select * from ds1 where c3 > 0",
              "rule_id": "9a8a8b2d3c504e99859f6de291b90fcc",
              "rule_name": "first rule",
              "user_input": "filter ds1 where (c3 > 0)"
            }
          ],
          "SELECTED_DATASETS": { ... }
        }

        Add a rule with a parameter starting with $ sign.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/add_rule?output=rs1&rule_name=first%20rule'
        ...              '&user_input=filter%20ds1%20where%20(c3%20>%200)'
        ...              '&workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 2
          },
          "PARAMETERS": {
            "$threshold": {
              "config_type": "",
              "config_value": "",
              "description": "",
              "help_text": "",
              "name": "$threshold"
            }
          },
          "RULES": [
            {
              "filter": {
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "0"
                  }
                ]
              },
              "input": "ds1",
              "output": "rs1",
              "parameters": [],
              "query": "select * from ds1 where c3 > 0",
              "rule_id": "9a8a8b2d3c504e99859f6de291b90fcc",
              "rule_name": "first rule",
              "user_input": "filter ds1 where (c3 > 0)"
            },
            {
              "filter": {
                "columns": [
                  "c1",
                  "c2"
                ],
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "$threshold"
                  }
                ]
              },
              "input": "rs1",
              "output": "rs2",
              "parameters": [
                "$threshold"
              ],
              "query": "select c1, c2 from rs1 where c3 > $threshold",
              "rule_id": "b0c83f310bca4041a966a22cf1d30e87",
              "rule_name": "second rule",
              "user_input": "filter rs1 using columns c1, c2 where (c3 > $threshold)"
            }
          ],
          "SELECTED_DATASETS": { ... }
        }
    """
    service = ServiceFactory('brms', 'AddRule', request)
    return service.invoke()


@brms.route('/update_rule')
def update_rule():
    """Update a rule to the workflow.

    Each business rule in BRMS is a component. All components have the following schema:
        * Rule: which carries the parameters required for prepared SQL query
        * Input: defines the metadata of the input dataset
        * Output: defines the metadata of the output dataset

    All components can be configured using text input.

    Parameters:
        workflow_id (str): unique Id of the workflow
        rule_name (str): business name given to the rule
        output (str): table in which output has to be populated
        user_input (str): user text based on components being configured in the system.
        rule_id (str): rule Id used to recognize the rule which has to be updated.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.

    Examples:
        Add a rule without a parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/update_rule?output=rs1&rule_name=first%20rule'
        ...              '&user_input=filter rs1 using columns c1, c2, c3 where (c3 > $threshold)'
        ...              '&workflow_id=46a0aea31273498a896b7761561c34de&rule_id=b0c83f310bca4041a966a22cf1d30e87')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": {
            "$threshold": {
              "config_type": "numeric",
              "config_value": "",
              "description": "describe the threshold",
              "help_text": "some threshold numeric value",
              "name": "$threshold"
            }
          },
          "RULES": [
            {
              "filter": {
                "columns": [
                  "c1",
                  "c2"
                ],
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "$threshold"
                  }
                ]
              },
              "input": "rs1",
              "output": "rs2",
              "parameters": [
                "$threshold"
              ],
              "query": "select c1, c2 from rs1 where c3 > $threshold",
              "rule_id": "b0c83f310bca4041a966a22cf1d30e87",
              "rule_name": "second rule",
              "user_input": "filter rs1 using columns c1, c2 where (c3 > $threshold)"
            }
          ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms', 'UpdateRule', request)
    return service.invoke()


@brms.route('/delete_rule')
def delete_rule():
    """Delete a rule within the workflow.

    Parameters:
        workflow_id (str): unique Id of the workflow
        rule_id (str): rule Id used to recognize the rule which has to be deleted.

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.
        BRMSDatasetInUse: this exception is raised when the output of the rule is in use by another rule.

    Examples:
        Add a rule without a parameter.

        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/delete_rule?'
        ...              '&workflow_id=46a0aea31273498a896b7761561c34de&rule_id=b0c83f310bca4041a966a22cf1d30e87')
        {
          "DATASETS": { ... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": {
          },
          "RULES": [
          ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms', 'DeleteRule', request)
    return service.invoke()


@brms.route('/add_parameter_definition')
def add_parameter_definition():
    """Add parameter definition in the workflow.

    Parameters can be configured within rule composer in which the user has the option to
    select one of the following:

        * Picklist - dedicated input like Boolean values
        * Database column values - distinct values from the column
        * Freehand text with alphanumeric or numeric constraint

    Parameters:
        parameter_name (str): name given to the parameter with $ sign.
        help_text (str): business specific help text to be displayed in scenario screen.
        description (str): describe the parameter being configured by the user.
        config_type (str): picklist, column, numeric or text.
        config_value (str): will vary based on the config_type
            * picklist - string with comma-separated options
            * numeric - None
            * text - None
            * column - based on the API call which will fetch table and column relationship to the UI (linked sub task)

    Returns:
        dict: a JSON object of the workflow generated for the given request.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.


    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/add_parameter_definition?parameter_name=$threshold'
        ...              '&config_type=numeric&config_value=&workflow_id=46a0aea31273498a896b7761561c34de'
        ...              '&help_text=some%20threshold%20numeric%20value&description=describe%20the%20threshold')
        {
          "DATASETS": {... },
          "META": {
            "description": "some desc",
            "name": "hello",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 2
          },
          "PARAMETERS": {
            "$threshold": {
              "config_type": "numeric",
              "config_value": "",
              "description": "describe the threshold",
              "help_text": "some threshold numeric value",
              "name": "$threshold"
            }
          },
          "RULES": [
            {
              "filter": {
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "0"
                  }
                ]
              },
              "input": "ds1",
              "output": "rs1",
              "parameters": [],
              "query": "select * from ds1 where c3 > 0",
              "rule_id": "9a8a8b2d3c504e99859f6de291b90fcc",
              "rule_name": "first rule",
              "user_input": "filter ds1 where (c3 > 0)"
            },
            {
              "filter": {
                "columns": [
                  "c1",
                  "c2"
                ],
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "$threshold"
                  }
                ]
              },
              "input": "rs1",
              "output": "rs2",
              "parameters": [
                "$threshold"
              ],
              "query": "select c1, c2 from rs1 where c3 > $threshold",
              "rule_id": "b0c83f310bca4041a966a22cf1d30e87",
              "rule_name": "second rule",
              "user_input": "filter rs1 using columns c1, c2 where (c3 > $threshold)"
            },
            {
              "filter": {
                "columns": [
                  "c1",
                  "c2",
                  "c3"
                ],
                "rules": [
                  {
                    "COLUMN": "c3",
                    "OPERATOR": ">",
                    "VALUE": "$threshold"
                  }
                ]
              },
              "input": "rs1",
              "output": "rs2",
              "parameters": [
                "$threshold"
              ],
              "query": "select c1, c2, c3 from rs1 where c3 > $threshold",
              "rule_id": "b0c83f310bca4041a966a22cf1d30e87",
              "rule_name": "second rule",
              "user_input": "filter rs1 using columns c1, c2, c3 where (c3 > $threshold)"
            }
          ],
          "SELECTED_DATASETS": { ... }
          }
        }



    """
    service = ServiceFactory('brms', 'AddParameterDefinition', request)
    return service.invoke()


@brms.route('/get_workflow')
def get_workflow():
    """Get a workflow as JSON.

    Structure of JSON::

    {"DATASETS": {}, "META": {}, "SELECTED_DATASETS": {}, "RULES": [], "PARAMETERS": {}}

    Parameters:
        workflow_id (str): Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the data of the workflow.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/get_workflow?workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms', 'GetWorkflow', request)
    return service.invoke()


@brms.route('/save_workflow')
def save_workflow():
    """Save BRMS workflow.

    Parameters:
        workflow_id (str):  Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow
        is edited.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/save_workflow?workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "message" : "Saved successfully"
        }

    """
    service = ServiceFactory('brms', 'SaveWorkflow', request)
    return service.invoke()


@brms.route('/publish_workflow')
def publish_workflow():
    """Publish the BRMS workflow.

    Parameters:
        workflow_id (str):  Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/publish_workflow?workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "PUBLISHED",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

        """
    service = ServiceFactory('brms', 'PublishWorkflow', request)
    return service.invoke()


@brms.route('/edit_workflow')
def edit_workflow():
    """Edit the BRMS workflow.

    An API which raises BRMS based exception when requested with published workflow id.

    Parameters:
        workflow_id (str):  Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Raises:
        BRMSPublishedWorkflowModified: this exception is raised when a published workflow is edited.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/edit_workflow?workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "46a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms', 'EditWorkflow', request)
    return service.invoke()


@brms.route('/copy_workflow')
def copy_workflow():
    """Copy the BRMS workflow.

    An API which creates a new workflow in DRAFT state using the copy of the workflow
    of the id with which it has been requested.

    Parameters:
        workflow_id (str):  Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the message that the workflow has been saved.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/copy_workflow?workflow_id=46a0aea31273498a896b7761561c34de')
        {
          "DATASETS": { ... },
          "META": {
            "description": "",
            "name": "",
            "status": "DRAFT",
            "workflow_id": "72a0aea31273498a896b7761561c34de",
            "workspace_id": 1
          },
          "PARAMETERS": { ... },
          "RULES": [ ... ],
          "SELECTED_DATASETS": { ... }
        }

    """
    service = ServiceFactory('brms', 'CopyWorkflow', request)
    return service.invoke()


@brms.route('/delete_workflow')
def delete_workflow():
    """Delete workflow for the BRMS.

    Each workflow is a template of rules in the saved state.

    Parameters:
        workflow_id (str): Unique workflow id used to identify the workflow.

    Returns:
        dict: a JSON object which has the ``message`` generated for the given request.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/delete_workflow?workflow_id=72a0aea31273498a896b7761561c34de')
        {
          "message": "Deleted successfully."
        }
    """
    service = ServiceFactory('brms', 'DeleteWorkflow', request)
    return service.invoke()


@brms.route('/list_workflow')
def list_workflow():
    """List the workflows currently configured within the system.

    Returns:
        dict: A JSON object with all the workflows

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/list_workflow')
        {
          "workflows": [
            {
              ...
            },
            {
              ...
            }
          ]
        }
    """
    service = ServiceFactory('brms', 'ListWorkflow', request)
    return service.invoke()


@brms.route('/execute_workflow')
def execute_workflow():
    """Execute the published workflow.

    Assumption:
        Adaptor name should not be exactly same as actual table name.

    Parameters:
        workflow_id (str): unique workflow id used to identify the workflow.
        input_adaptors (dict): pair of adaptor name with the actual table name with schema (if any).
        parameters (dict): pair of parameter name with its value defined for the current execution.
        output_adaptors (list): list of adaptors which are expected to be created from the workflow.

    Returns:
        dict: A JSON object with exact queries and output table names for reference.

    See Also:
        :func:`~controller.brms.publish_workflow` Change the state of the workflow to published.

    Raises:
        BRMSDraftWorkflowExecuted: This exception is raised when a draft version of the workflow
        is triggered to execute.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/brms/execute_workflow?workflow_id=46a0aea31273498a896b7761561c34de'
        ...              '&input_adaptors={"ds1": "actual_table_ds1"}&output_adaptors=["rs1"]')
        {
          'META': {
            'scenario_id': '2e9b40ba85df415c892146165f848e1e',
            'workflow_id': 'aa3476bd45fc466683361bb45224a380',
            'status': 'passed'
          },
          'QUERY': [
            '...'
          ],
          'OUTPUT': {
            'rs1': 'rs1_2e9b40ba85df415c892146165f848e1e'
          }
        }
    """
    service = ServiceFactory('brms', 'ExecuteWorkflow', request)
    return service.invoke()
