#   data_management/controller/docs.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
"""Documentation render module."""
from flask import Blueprint, send_from_directory

docs = Blueprint('docs', __name__)


@docs.route('/', defaults={'filename': 'index.html'})
@docs.route('/<path:filename>')
def documentation(filename):
    """Delivering mechanism for the docs folder HTML to the web protocol."""
    return send_from_directory(r'.\docs\_build', filename)
