#   data_management/controller/phrases_validation.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from flask import Blueprint, request

from services import ServiceFactory

phrases_validation = Blueprint('phrases_validation', __name__)


@phrases_validation.route('/')
def index():
    """Phrases Validation Engine.
    """
    return "Phrases Validation Engine"


@phrases_validation.route('/add')
def add_phrases():
    """Add phrases in the system.

    Parameters:
        component_name (str): unique component name
        phrases (list): comma separated list of phrases.

    Returns:
        dict: a JSON object which has the structured definition of the added component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases_validation/add?component_name=allowed values'
        ...              '&phrases=["allowed values in table1.c1 are alphanumeric", "allowed values in table2.c1'
        ...              ' are email", "allowed values in table3.c1 are alpha", "allowed values in table1.c1 are '
        ...              'upper case", "allowed values in table1.c1 are q1, q2, q3"]')
        {
          "component_name": "allowed values",
          "phrases": [
            "allowed values in table1.c1 are alphanumeric",
            "allowed values in table2.c1 are email",
            "allowed values in table3.c1 are alpha",
            "allowed values in table1.c1 are upper case",
            "allowed values in table1.c1 are q1, q2, q3"
            ]
        }

    """
    service = ServiceFactory('phrases_validation', 'AddPhrase', request)
    return service.invoke()


@phrases_validation.route('/update')
def update_phrases():
    """Update phrases in the system.

    Parameters:
        component_name (str): unique component name
        phrases (list): comma separated list of phrases.

    Returns:
        dict: a JSON object which has the structured definition of the updated component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases_validation/update?component_name=allowed values'
        ...              '&phrases=["allowed values in table1.c1 are alphanumeric", "allowed values in table2.c1'
        ...              ' are email", "allowed values in table3.c1 are alpha", "allowed values in table1.c1 are '
        ...              'upper case", "allowed values in table1.c1 are q1, q2, q3"]')
        {
          "component_name": "allowed values",
          "phrases": [
            "allowed values in table1.c1 are alphanumeric",
            "allowed values in table2.c1 are email",
            "allowed values in table3.c1 are alpha",
            "allowed values in table1.c1 are upper case",
            "allowed values in table1.c1 are q1, q2, q3"
            ]
        }

    """
    service = ServiceFactory('phrases_validation', 'UpdatePhrase', request)
    return service.invoke()


@phrases_validation.route('/delete')
def delete_phrases():
    """Delete phrases for a given component.

    Parameters:
        component_name (str): unique component name

    Returns:
        dict: a JSON object which has the message.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases_validation/delete?component_name=allowed values')
        {
          "message": "Deleted successfully"
        }

    """
    service = ServiceFactory('phrases_validation', 'DeletePhrase', request)
    return service.invoke()


@phrases_validation.route('/get')
def get_phrases():
    """Get phrases of the component.

    Parameters:
        component_name (str): unique component name

    Returns:
        dict: a JSON object which has the structured definition of the requested component.

    Examples:
        >>> import requests
        >>> requests.get('http://localhost:5001/api/phrases_validation/get?component_name=allowed values')
        {
          "component_name": "allowed values",
          "phrases": [
            "allowed values in table1.c1 are alphanumeric",
            "allowed values in table2.c1 are email",
            "allowed values in table3.c1 are alpha",
            "allowed values in table1.c1 are upper case",
            "allowed values in table1.c1 are q1, q2, q3"
            ]
        }

    """
    service = ServiceFactory('phrases_validation', 'GetPhrase', request)
    return service.invoke()


@phrases_validation.route('/list')
def list_phrases():
    """List all the file adaptor definitions from the system.

     Returns:
         dict: a JSON object which has all the adaptor names and their meta data.

     Examples:
         >>> import requests
         >>> requests.get('http://localhost:5001/api/phrases_validation/list')
         {
          "phrases": [
            {
              "name": "allowed values",
              "phrases": [
                ....
                ]
            },
            {
                "name": "mandatory column",
                "phrases": [
                    ...
                ]
            }
            ]
         }

     """
    service = ServiceFactory('phrases_validation', 'ListPhrase', request)
    return service.invoke()
