#   datamax/api/entity/tests/test_etl_scenario.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.scenario import ETLScenario
from entity.workflow import Workflow


@pytest.fixture
def ds_scenario():
    db_workflow = {
        'META': {'workflow_id': '1209b0dfa7524a67a4a81b70f4e0c572', 'name': 'sd', 'description': 'sdd',
                 'workspace_id': 1,
                 'status': 'DRAFT'},
        'DATASETS': {
            'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
            'ds2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs3': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs4': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}]},
        'SELECTED_DATASETS': {
            'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'}],
                    "schema": "brms_instance"},
            'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'},
                             {'column_name': 'c5', 'data_type': 'varchar'}],
                    "schema": "brms_instance"}
        },
        'PARAMETERS': {
            '$threshold': {'name': '$threshold', 'help_text': 'threshold value', 'description': 'some new description',
                           'config_type': 'numeric', 'config_value': ''},
            '$quarter': {'name': '$quarter', 'help_text': 'quarter value', 'description': 'some new description',
                         'config_type': 'picklist', 'config_value': 'q1,q2'}},
        'RULES': [
            {'rule_id': 'a7df3bda85f04387869754fb2e711fae', 'rule_name': 'rule_1', 'input': 'ds2',
             'user_input': 'filter ds2 where ( c2 > $threshold)', 'output': 'rs1',
             'query': 'select * from {schema}.{ds2} where c2 > $threshold', 'parameters': ['$threshold'],
             'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': 'd782c526675c40e78d966af388dc91f9', 'rule_name': 'rule_2', 'input': 'ds2',
             'user_input': 'filter ds2 where ( c2 != null)', 'output': 'rs2',
             'query': 'select * from {schema}.{ds2} where c2 != null', 'parameters': [],
             'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '!=', 'VALUE': 'null'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': '04d17d8e88bc4ac7a651f9c02a991cd1', 'rule_name': 'rule_3', 'input': 'ds2',
             'user_input': 'filter ds2 where OR(( c2 > $threshold)(c1 = $quarter))', 'output': 'rs3',
             'query': 'select * from {schema}.{ds2} where (c2 > $threshold) or (c1 = $quarter)',
             'parameters': ['$threshold', '$quarter'], 'filter': {'condition': 'OR', 'rules': [
                {'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'},
                {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': '$quarter'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': '4268ce78f69f4263b904f9678f724061', 'rule_name': 'rule_4', 'input': 'rs2',
             'user_input': "filter rs2 where OR(( c2 > 5)(c1 = 'q1'))", 'output': 'rs4',
             'query': "select * from {schema}.{rs2} where (c2 > 5) or (c1 = 'q1')", 'parameters': [],
             'filter': {'condition': 'OR', 'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '5'},
                                                     {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': "'q1'"}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]}]}
    scenario_meta = {"parameters": [{"name": "$threshold", "value": "0.5"}, {"name": "$quarter", "value": "Q2"},
                                    {"name": "$quarter_1", "value": "Q3"}],
                     "output_adaptors": ["rs4"]}
    return ETLScenario.initialize_with_dataset(1, 's1', scenario_meta, True, Workflow.initialize(db_workflow),
                                               execution_schema='brms_instance', output_schema='brms_output')


@pytest.fixture
def ad_scenario():
    db_workflow = {
        'META': {'workflow_id': '1209b0dfa7524a67a4a81b70f4e0c572', 'name': 'sd', 'description': 'sdd',
                 'workspace_id': 1,
                 'status': 'DRAFT'},
        'DATASETS': {
            'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
            'ds2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs3': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}],
            'rs4': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                    {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                    {'column_name': 'c5', 'data_type': 'varchar'}]},
        'SELECTED_DATASETS': {
            'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'}],
                    "schema": None},
            'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                             {'column_name': 'c2', 'data_type': 'varchar'},
                             {'column_name': 'c3', 'data_type': 'numeric'},
                             {'column_name': 'c4', 'data_type': 'varchar'},
                             {'column_name': 'c5', 'data_type': 'varchar'}],
                    "schema": None}
        },
        'PARAMETERS': {
            '$threshold': {'name': '$threshold', 'help_text': 'threshold value', 'description': 'some new description',
                           'config_type': 'numeric', 'config_value': ''},
            '$quarter': {'name': '$quarter', 'help_text': 'quarter value', 'description': 'some new description',
                         'config_type': 'picklist', 'config_value': 'q1,q2'}},
        'RULES': [
            {'rule_id': 'a7df3bda85f04387869754fb2e711fae', 'rule_name': 'rule_1', 'input': 'ds2',
             'user_input': 'filter ds2 where ( c2 > $threshold)', 'output': 'rs1',
             'query': 'select * from {schema}.{ds2} where c2 > $threshold', 'parameters': ['$threshold'],
             'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': 'd782c526675c40e78d966af388dc91f9', 'rule_name': 'rule_2', 'input': 'ds2',
             'user_input': 'filter ds2 where ( c2 != null)', 'output': 'rs2',
             'query': 'select * from {schema}.{ds2} where c2 != null', 'parameters': [],
             'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '!=', 'VALUE': 'null'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': '04d17d8e88bc4ac7a651f9c02a991cd1', 'rule_name': 'rule_3', 'input': 'ds2',
             'user_input': 'filter ds2 where OR(( c2 > $threshold)(c1 = $quarter))', 'output': 'rs3',
             'query': 'select * from {schema}.{ds2} where (c2 > $threshold) or (c1 = $quarter)',
             'parameters': ['$threshold', '$quarter'], 'filter': {'condition': 'OR', 'rules': [
                {'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'},
                {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': '$quarter'}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]},
            {'rule_id': '4268ce78f69f4263b904f9678f724061', 'rule_name': 'rule_4', 'input': 'rs2',
             'user_input': "filter rs2 where OR(( c2 > 5)(c1 = 'q1'))", 'output': 'rs4',
             'query': "select * from {schema}.{rs2} where (c2 > 5) or (c1 = 'q1')", 'parameters': [],
             'filter': {'condition': 'OR', 'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '5'},
                                                     {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': "'q1'"}]},
             '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                  {'column_name': 'c2', 'data_type': 'varchar'},
                                  {'column_name': 'c3', 'data_type': 'numeric'},
                                  {'column_name': 'c4', 'data_type': 'varchar'},
                                  {'column_name': 'c5', 'data_type': 'varchar'}]}]}
    scenario_meta = {"parameters": {"$threshold": "0.5", "$quarter": "Q2", "$quarter_1": "Q3"},
                     "output_adaptors": ["rs4"]}
    return ETLScenario.initialize_with_adaptor('brms_instance', Workflow.initialize(db_workflow),
                                               {'ds1': 'actual_ds1', 'ds2': 'actual_ds2'},
                                               scenario_meta['parameters'], scenario_meta['output_adaptors'])


def test_scenario_id(ds_scenario):
    assert ds_scenario.scenario_id == '1'


def test_output_dataset(ds_scenario):
    assert ds_scenario.serialize['OUTPUT']['rs4'] == 'brms_output.rs4_1'


def test_input_dataset(ds_scenario):
    assert ds_scenario.serialize['OUTPUT']['ds1'] == 'brms_instance.ds1'
    assert ds_scenario.serialize['OUTPUT']['ds2'] == 'brms_instance.ds2'


def test_intermediate_dataset(ds_scenario):
    assert ds_scenario.serialize['OUTPUT']['rs1'] == 'brms_instance.rs1_1'
    assert ds_scenario.serialize['OUTPUT']['rs2'] == 'brms_instance.rs2_1'
    assert ds_scenario.serialize['OUTPUT']['rs3'] == 'brms_instance.rs3_1'


def test_prepare_query_to_adds_to_serialize(ds_scenario):
    ds_scenario.prepare_query('create table {schema_rs1}.{rs1} as select * from {schema_ds1}.{ds1} where c1 > 0')
    assert 'QUERY' in ds_scenario.serialize
    assert 'OUTPUT' in ds_scenario.serialize
    assert len(ds_scenario.serialize['QUERY']) == 1


def test_filter_scenario(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs1}.{rs1} as select * from {schema_ds1}.{ds1} '
                                       'where c1 > 0')
    expected = 'create table brms_instance.rs1_{0} as select * from brms_instance.ds1 where c1 > 0'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_filter_scenario_on_resultant(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs2}.{rs2} as select * from '
                                       '{schema_rs1}.{rs1} where c1 > 0')
    expected = 'create table brms_instance.rs2_{0} as select * from brms_instance.rs1_{0} where c1 > 0'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_filter_scenario_with_param(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs1}.{rs1} as select * from '
                                       '{schema_ds1}.{ds1} where c1 > $threshold')
    expected = 'create table brms_instance.rs1_{0} as select * from brms_instance.ds1 where c1 > 0.5'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_adaptor_input(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs1}.{rs1} as select {schema_ds1}.{ds1}.c1 as ds1#c1, '
                                       '{schema_ds1}.{ds1}.c2 as ds1#c2, '
                                       '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} '
                                       'left join {schema_ds2}.{ds2} on '
                                       '{schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3')
    expected = 'create table brms_instance.rs1_{0} as select brms_instance.ds1.c1 as "ds1#c1", ' \
               'brms_instance.ds1.c2 as "ds1#c2", brms_instance.ds2.C5 as "ds2#C5" from brms_instance.ds1 ' \
               'left join brms_instance.ds2 on brms_instance.ds1.c2 = brms_instance.ds2.c3'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_one_resultant_dataset_as_input(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as select {schema_ds1}.{ds1}.c2 as ds1#c2, '
                                       '{schema_rs2}.{rs2}.c1 as rs2#c1, '
                                       '{schema_rs2}.{rs2}.C5 as rs2#C5 from {schema_ds1}.{ds1} '
                                       'left join {schema_rs2}.{rs2} on {schema_ds1}.{ds1}.c2 = '
                                       '{schema_rs2}.{rs2}.c1')
    expected = 'create table brms_instance.rs3_{0} as select brms_instance.ds1.c2 as "ds1#c2", ' \
               'brms_instance.rs2_{0}.c1 as "rs2#c1", brms_instance.rs2_{0}.C5 as "rs2#C5" ' \
               'from brms_instance.ds1 left join brms_instance.rs2_{0} ' \
               'on brms_instance.ds1.c2 = brms_instance.rs2_{0}.c1'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_both_resultant_dataset_as_input(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as select {schema_rs1}.{rs1}.c2 '
                                       'as rs1#c2, '
                                       '{schema_rs2}.{rs2}.c1 as rs2#c1, '
                                       '{schema_rs2}.{rs2}.C5 as rs2#C5 from {schema_rs1}.{rs1} '
                                       'left join {schema_rs2}.{rs2} '
                                       'on {schema_rs1}.{rs1}.c2 = {schema_rs2}.{rs2}.c1')
    expected = 'create table brms_instance.rs3_{0} as select brms_instance.rs1_{0}.c2 as "rs1#c2", ' \
               'brms_instance.rs2_{0}.c1 as "rs2#c1", brms_instance.rs2_{0}.C5 as "rs2#C5" ' \
               'from brms_instance.rs1_{0} left join brms_instance.rs2_{0} ' \
               'on brms_instance.rs1_{0}.c2 = brms_instance.rs2_{0}.c1'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_one_resultant_dataset_as_input_and_adaptor_based_columns(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as select {schema_ds1}.{ds1}.c2 as ds1#c2, '
                                       '{schema_rs2}.{rs2}.ds2#c1 as ds2#c1, '
                                       '{schema_rs2}.{rs2}.ds2#C5 as ds2#C5 from {schema_ds1}.{ds1} '
                                       'left join {schema_rs2}.{rs2} '
                                       'on {schema_ds1}.{ds1}.c2 = {schema_rs2}.{rs2}.ds2#c1')
    expected = 'create table brms_instance.rs3_{0} as select brms_instance.ds1.c2 as "ds1#c2", ' \
               'brms_instance.rs2_{0}."ds2#c1" as "ds2#c1", brms_instance.rs2_{0}."ds2#C5" as "ds2#C5" ' \
               'from brms_instance.ds1 left join brms_instance.rs2_{0} ' \
               'on brms_instance.ds1.c2 = brms_instance.rs2_{0}."ds2#c1"'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_both_resultant_dataset_as_input_and_adaptor_based_columns(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as select '
                                       '{schema_rs1}.{rs1}.ds1#c2 as ds1#c2, '
                                       '{schema_rs2}.{rs2}.ds2#c1 as ds2#c1, '
                                       '{schema_rs2}.{rs2}.ds2#C5 as ds2#C5 from {schema_ds1}.{ds1} '
                                       'left join {schema_rs2}.{rs2} '
                                       'on {schema_rs1}.{rs1}.ds1#c2 = {schema_rs2}.{rs2}.ds2#c1')
    expected = 'create table brms_instance.rs3_{0} as select brms_instance.rs1_{0}."ds1#c2" as "ds1#c2", ' \
               'brms_instance.rs2_{0}."ds2#c1" as "ds2#c1", brms_instance.rs2_{0}."ds2#C5" as "ds2#C5" ' \
               'from brms_instance.ds1 left join brms_instance.rs2_{0} on brms_instance.rs1_{0}."ds1#c2" = ' \
               'brms_instance.rs2_{0}."ds2#c1"'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_filter_scenario_with_column_name_same_as_adaptor_name(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as select ds1, c2 from '
                                       '{schema_ds1}.{ds1} where c1 > 0')
    expected = 'create table brms_instance.rs3_{0} as select ds1, c2 from brms_instance.ds1 where c1 > 0'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_join_scenario_with_adaptor_same_as_column_name(ds_scenario):
    actual = ds_scenario.prepare_query('create table {schema_rs3}.{rs3} as '
                                       'select {schema_rs1}.{rs1}.ds1#ds1 as ds1#ds1, '
                                       '{schema_rs2}.{rs2}.ds2#c1 as ds2#c1, '
                                       '{schema_rs2}.{rs2}.ds2#C5 as ds2#C5 from {schema_ds1}.{ds1} '
                                       'left join {schema_rs2}.{rs2} '
                                       'on {schema_rs1}.{rs1}.ds1#c2 = {schema_rs2}.{rs2}.ds2#c1')
    expected = 'create table brms_instance.rs3_{0} as select brms_instance.rs1_{0}."ds1#ds1" as "ds1#ds1", ' \
               'brms_instance.rs2_{0}."ds2#c1" as "ds2#c1", brms_instance.rs2_{0}."ds2#C5" as "ds2#C5" ' \
               'from brms_instance.ds1 left join brms_instance.rs2_{0} on brms_instance.rs1_{0}."ds1#c2" ' \
               '= brms_instance.rs2_{0}."ds2#c1"'
    assert actual == expected.format(ds_scenario.scenario_id)


def test_ad_scenario_id(ad_scenario):
    assert ad_scenario.scenario_id is not None


def test_ad_scenario_output_dataset(ad_scenario):
    assert ad_scenario.serialize['OUTPUT']['rs4'] == 'brms_instance.rs4_' + ad_scenario.scenario_id


def test_ad_scenario_input_dataset(ad_scenario):
    assert ad_scenario.serialize['OUTPUT']['ds1'] == 'brms_instance.actual_ds1'
    assert ad_scenario.serialize['OUTPUT']['ds2'] == 'brms_instance.actual_ds2'


def test_adpator_scenario_intermediate_dataset(ad_scenario):
    assert ad_scenario.serialize['OUTPUT']['rs1'] == 'brms_instance.rs1_' + ad_scenario.scenario_id
    assert ad_scenario.serialize['OUTPUT']['rs2'] == 'brms_instance.rs2_' + ad_scenario.scenario_id
    assert ad_scenario.serialize['OUTPUT']['rs3'] == 'brms_instance.rs3_' + ad_scenario.scenario_id


def test_parameter_single_replacement(ds_scenario):
    actual = ds_scenario.prepare_query("create table {schema_rs2}.{rs2} as select * from "
                                       "{schema_rs1}.{rs1} where c1 = '$quarter'")
    expected = "create table brms_instance.rs2_1 as select * from brms_instance.rs1_1 where c1 = 'Q2'"
    assert actual == expected


def test_parameter_multi_replacement(ds_scenario):
    actual = ds_scenario.prepare_query("create table {schema_rs2}.{rs2} as select * from "
                                       "{schema_rs1}.{rs1} where c1 = '$quarter' or c1 = '$quarter_1'")
    expected = "create table brms_instance.rs2_1 as select * from brms_instance.rs1_1 where c1 = 'Q2' or c1 = 'Q3'"
    assert actual == expected
