#   datamax/api/entity/tests/test_workflow.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import pytest

from entity.workflow import Workflow
from utils.exceptions import BRMSDatasetInUse

db_workflow = {
    'META': {'workflow_id': '1209b0dfa7524a67a4a81b70f4e0c572', 'name': 'sd', 'description': 'sdd', 'workspace_id': 1,
             'status': 'DRAFT'},
    'DATASETS': {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                         {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                 'ds2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                         {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                         {'column_name': 'c5', 'data_type': 'varchar'}],
                 'test': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                          {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                          {'column_name': 'c5', 'data_type': 'varchar'}],
                 'test2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                           {'column_name': 'c5', 'data_type': 'varchar'}],
                 'test3': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                           {'column_name': 'c5', 'data_type': 'varchar'}],
                 'test4': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                           {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                           {'column_name': 'c5', 'data_type': 'varchar'}]},
    'SELECTED_DATASETS': {
        'ds1': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                 {'column_name': 'c2', 'data_type': 'varchar'},
                                 {'column_name': 'c3', 'data_type': 'numeric'},
                                 {'column_name': 'c4', 'data_type': 'varchar'}],
                "schema": "brms_instance"},
        'ds2': {"meta": [{'column_name': 'c1', 'data_type': 'varchar'},
                                 {'column_name': 'c2', 'data_type': 'varchar'},
                                 {'column_name': 'c3', 'data_type': 'numeric'},
                                 {'column_name': 'c4', 'data_type': 'varchar'},
                                 {'column_name': 'c5', 'data_type': 'varchar'}],
                "schema": "brms_instance"}
    },
    'PARAMETERS': {
        '$threshold': {'name': '$threshold', 'help_text': 'threshold value', 'description': 'some new description',
                       'config_type': 'numeric', 'config_value': ''},
        '$quarter': {'name': '$quarter', 'help_text': 'quarter value', 'description': 'some new description',
                     'config_type': 'picklist', 'config_value': 'q1,q2'}},
    'RULES': [
        {'rule_id': 'a7df3bda85f04387869754fb2e711fae', 'rule_name': 'rule_1', 'input': 'ds2',
         'user_input': 'filter ds2 where ( c2 > $threshold)', 'output': 'test',
         'query': 'select * from {schema}.{ds2} where c2 > $threshold', 'parameters': ['$threshold'],
         'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'}]},
         '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                              {'column_name': 'c2', 'data_type': 'varchar'},
                              {'column_name': 'c3', 'data_type': 'numeric'},
                              {'column_name': 'c4', 'data_type': 'varchar'},
                              {'column_name': 'c5', 'data_type': 'varchar'}]},
        {'rule_id': 'd782c526675c40e78d966af388dc91f9', 'rule_name': 'rule_2', 'input': 'ds2',
         'user_input': 'filter ds2 where ( c2 != null)', 'output': 'test2',
         'query': 'select * from {schema}.{ds2} where c2 != null', 'parameters': [],
         'filter': {'rules': [{'COLUMN': 'c2', 'OPERATOR': '!=', 'VALUE': 'null'}]},
         '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                              {'column_name': 'c2', 'data_type': 'varchar'},
                              {'column_name': 'c3', 'data_type': 'numeric'},
                              {'column_name': 'c4', 'data_type': 'varchar'},
                              {'column_name': 'c5', 'data_type': 'varchar'}]},
        {'rule_id': '04d17d8e88bc4ac7a651f9c02a991cd1', 'rule_name': 'rule_3', 'input': 'ds2',
         'user_input': 'filter ds2 where OR(( c2 > $threshold)(c1 = $quarter))', 'output': 'test3',
         'query': 'select * from {schema}.{ds2} where (c2 > $threshold) or (c1 = $quarter)',
         'parameters': ['$threshold', '$quarter'], 'filter': {'condition': 'OR', 'rules': [
            {'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'},
            {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': '$quarter'}]},
         '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                              {'column_name': 'c2', 'data_type': 'varchar'},
                              {'column_name': 'c3', 'data_type': 'numeric'},
                              {'column_name': 'c4', 'data_type': 'varchar'},
                              {'column_name': 'c5', 'data_type': 'varchar'}]},
        {'rule_id': '4268ce78f69f4263b904f9678f724061', 'rule_name': 'rule_4', 'input': 'test2',
         'user_input': "filter test2 where OR(( c2 > 5)(c1 = 'q1'))", 'output': 'test4',
         'query': "select * from {schema}.{test2} where (c2 > 5) or (c1 = 'q1')", 'parameters': [],
         'filter': {'condition': 'OR', 'rules': [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '5'},
                                                 {'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': "'q1'"}]},
         '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                              {'column_name': 'c2', 'data_type': 'varchar'},
                              {'column_name': 'c3', 'data_type': 'numeric'},
                              {'column_name': 'c4', 'data_type': 'varchar'},
                              {'column_name': 'c5', 'data_type': 'varchar'}]}]}

db_repetitive_workflow = {
    "META": {
        "workflow_id": "a470d42ad77a4ec38ccc424ad77b4608",
        "name": "hello",
        "description": "some desc",
        "workspace_id": 2,
        "status": "DRAFT"
    },
    "DATASETS": {
        "ds1": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            }
        ],
        "rs1": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            },
            {
                "column_name": "new_c3",
                "data_type": "numeric",
                "derive": True
            },
            {
                "column_name": "sum_nrx",
                "data_type": "numeric",
                "derive": True
            }
        ]
    },
    "SELECTED_DATASETS": {
        "ds1": {"meta": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            }
        ], "schema": None}
    },
    "PARAMETERS": {},
    "RULES": [
        {
            "rule_id": "4cf3be67c9eb43a19cca40ae9ffa70bd",
            "rule_name": "rule_1",
            "input": "ds1",
            "user_input": "filter ds1 where (c3 > 0)",
            "output": "rs1",
            "query": "select * from {schema}.{ds1} where c3 > 0",
            "parameters": [],
            "filter": {
                "rules": [
                    {
                        "COLUMN": "c3",
                        "OPERATOR": ">",
                        "VALUE": "0"
                    }
                ]
            },
            "_output_metadata": [
                {
                    "column_name": "c1",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c2",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c3",
                    "data_type": "numeric"
                },
                {
                    "column_name": "c4",
                    "data_type": "varchar"
                }
            ]
        },
        {
            "rule_id": "28f7725241f842c2a4be1a9337611921",
            "rule_name": "rule_2",
            "input": "rs1",
            "user_input": "derive columns in rs1 with 0.5*c3 as new_c3 of type numeric",
            "output": "rs1",
            "query": "select *, cast(0.5*c3 as numeric) as new_c3 from {schema}.{rs1}",
            "parameters": [],
            "derived": [
                {
                    "formula": "0.5*c3",
                    "column_alias": "new_c3",
                    "data_type": "numeric"
                }
            ],
            "_output_metadata": [
                {
                    "column_name": "c1",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c2",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c3",
                    "data_type": "numeric"
                },
                {
                    "column_name": "c4",
                    "data_type": "varchar"
                },
                {
                    "column_name": "new_c3",
                    "data_type": "numeric",
                    "derived": True
                }
            ]
        },
        {
            "rule_id": "658a78f5e7cf40c5a5a7599c3b781cee",
            "rule_name": "rule_3",
            "input": "rs1",
            "user_input": "partition rs1 to derive sum(nrx) as sum_nrx over (c1, c2)",
            "output": "rs1",
            "query": "select *, sum(nrx) over (partition by c1, c2) as sum_nrx from {schema}.{rs1}",
            "parameters": [],
            "partition": [
                {
                    "formula": "sum(nrx)",
                    "column_alias": "sum_nrx",
                    "partition_by": {
                        "columns": [
                            "c1",
                            "c2"
                        ]
                    }
                }
            ],
            "_output_metadata": [
                {
                    "column_name": "c1",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c2",
                    "data_type": "varchar"
                },
                {
                    "column_name": "c3",
                    "data_type": "numeric"
                },
                {
                    "column_name": "c4",
                    "data_type": "varchar"
                },
                {
                    "column_name": "new_c3",
                    "data_type": "numeric",
                    "derive": True
                },
                {
                    "column_name": "sum_nrx",
                    "data_type": "numeric",
                    "derive": True
                }
            ]
        }
    ]
}


@pytest.fixture()
def workflow():
    return Workflow.initialize(db_workflow)


@pytest.fixture()
def repetitive_workflow():
    return Workflow.initialize(db_repetitive_workflow)


def test_empty_workflow():
    workflow = Workflow()
    assert workflow.serialize is not None


def test_workflow_initialize_meta(workflow):
    assert workflow.workflow_id == '1209b0dfa7524a67a4a81b70f4e0c572'
    assert workflow.name == 'sd'
    assert workflow.description == 'sdd'
    assert workflow.workspace_id == 1
    assert workflow.status == 'DRAFT'


def test_workflow_initialize_dataset(workflow):
    assert workflow.datasets.serialize == db_workflow['DATASETS']


def test_workflow_initialize_selected_dataset(workflow):
    assert workflow.selected_datasets.serialize == db_workflow['SELECTED_DATASETS']


def test_workflow_initialize_rules(workflow):
    assert workflow.rules.serialize == db_workflow['RULES']


def test_workflow_initialize_parameters(workflow):
    assert workflow.parameters.serialize == db_workflow['PARAMETERS']


def test_workflow_add_selected_dataset():
    workflow = Workflow()
    workflow.add_selected_dataset('ds1', [], '')
    assert workflow.datasets.serialize == {'ds1': []}
    assert workflow.selected_datasets.serialize == {'ds1': {'meta': [], 'schema': ''}}


def test_workflow_replace_selected_dataset(workflow):
    workflow.truncate_selected_dataset()
    assert len(workflow.datasets.serialize) == 4


def test_workflow_remove_dataset(workflow):
    rule_id = db_workflow['RULES'][0]['rule_id']
    assert len(workflow.datasets.serialize) == 6
    workflow.remove_dataset(rule_id)
    assert len(workflow.datasets.serialize) == 5


def test_workflow_get_dataset(workflow):
    rule = db_workflow['RULES'][0]
    dataset_name = rule['output']
    rule_id = rule['rule_id']
    assert dataset_name == workflow.get_dataset(rule_id)


def test_workflow_add_rule_with_parameter():
    workflow = Workflow()
    workflow.add_rule(db_workflow['RULES'][0])
    assert len(workflow.parameters.serialize) == 1


def test_workflow_add_rule_without_parameter():
    workflow = Workflow()
    workflow.add_rule(db_workflow['RULES'][1])
    assert len(workflow.parameters.serialize) == 0


def test_workflow_update_rule_with_same_parameter_then_should_not_change_definition(workflow):
    rule = db_workflow['RULES'][2].copy()
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], rule)
    workflow.unlock(rule['rule_id'])
    assert workflow.parameters.serialize == db_workflow['PARAMETERS']


def test_workflow_update_rule_with_no_parameter_should_remove_old_parameter(workflow):
    rule = db_workflow['RULES'][2].copy()
    rule['parameters'] = []
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], rule)
    workflow.unlock(rule['rule_id'])
    assert len(workflow.parameters.serialize) == 1


def test_workflow_update_rule_with_shared_parameter_should_not_change_parameter_definition(workflow):
    rule = db_workflow['RULES'][0].copy()
    rule['parameters'] = []
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], rule)
    workflow.unlock(rule['rule_id'])
    assert workflow.parameters.serialize == db_workflow['PARAMETERS']


def test_workflow_update_rule_with_new_parameter_should_add_parameter(workflow):
    rule = db_workflow['RULES'][1].copy()
    rule['parameters'] = ['$new_param']
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], rule)
    workflow.unlock(rule['rule_id'])
    assert len(workflow.parameters.serialize) == 3


def test_workflow_delete_rule(workflow):
    rule_id = db_workflow['RULES'][0]['rule_id']
    workflow.delete_rule(rule_id)
    assert len(workflow.rules.serialize) == 3


def test_workflow_delete_rule_raise_exception(workflow):
    rule_id = db_workflow['RULES'][1]['rule_id']
    with pytest.raises(BRMSDatasetInUse):
        workflow.delete_rule(rule_id)


def test_workflow_delete_rule_with_no_parameter_should_do_nothing(workflow):
    rule = db_workflow['RULES'][3]
    workflow.delete_rule(rule['rule_id'])
    assert len(workflow.parameters.serialize) == 2
    assert len(workflow.rules.serialize) == 3
    assert workflow.parameters.serialize == db_workflow['PARAMETERS']


def test_workflow_delete_rule_with_shared_parameter_should_not_change_parameter_definition(workflow):
    rule = db_workflow['RULES'][0]
    workflow.delete_rule(rule['rule_id'])
    assert workflow.parameters.serialize == db_workflow['PARAMETERS']
    assert len(workflow.rules.serialize) == 3


def test_workflow_delete_rule_with_no_shared_parameter_should_remove_parameter_definition(workflow):
    rule = db_workflow['RULES'][2]
    workflow.delete_rule(rule['rule_id'])
    assert len(workflow.parameters.serialize) == 1
    assert len(workflow.rules.serialize) == 3


def test_workflow_delete_last_rule(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-1]
    repetitive_workflow.delete_rule(rule['rule_id'])
    assert len(repetitive_workflow.rules.serialize) == 2


def test_workflow_delete_impacted_rule(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][0]
    with pytest.raises(BRMSDatasetInUse):
        repetitive_workflow.delete_rule(rule['rule_id'])


def test_workflow_delete_latest_rule_should_refresh_dataset(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-1]
    repetitive_workflow.delete_rule(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c3', 'data_type': 'numeric', "derived": True}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_chain_delete_latest_rule_should_refresh_dataset(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-1]
    repetitive_workflow.delete_rule(rule['rule_id'])
    rule = db_repetitive_workflow['RULES'][-2]
    repetitive_workflow.delete_rule(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_update_rule_with_new_output_dataset(workflow):
    rule = db_workflow['RULES'][-1]
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], {
        "filter": {"condition": "OR",
                   "rules": [
                       {"COLUMN": "c2", "OPERATOR": ">", "VALUE": "50"},
                       {"COLUMN": "c1", "OPERATOR": "=", "VALUE": "'q1'"}]},
        "input": "test2",
        "query": "select * from test2 where c2 > 50 or c1 = 'q1'",
        "parameters": [],
        "user_input": "filter test2 where OR(( c2 > 50)(c1 = 'q1'))",
        "output": "test5", "rule_name": "test", "rule_id": '12345hgfds21s245rwuisa',
        "_output_metadata": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            },
            {
                "column_name": "c5",
                "data_type": "varchar"
            }
        ]})
    workflow.unlock(rule['rule_id'])
    assert "test4" not in workflow.get_dataset_names()


def test_workflow_update_rule_with_same_output_dataset(workflow):
    rule = db_workflow['RULES'][-1]
    workflow.lock(rule['rule_id'])
    workflow.update_rule(rule['rule_id'], {
        "filter": {"condition": "OR",
                   "rules": [
                       {"COLUMN": "c2", "OPERATOR": ">", "VALUE": "50"},
                       {"COLUMN": "c1", "OPERATOR": "=", "VALUE": "'q1'"}]},
        "input": "test2",
        "query": "select * from test2 where c2 > 50 or c1 = 'q1'",
        "parameters": [],
        "user_input": "filter test2 where OR(( c2 > 50)(c1 = 'q1'))",
        "output": "test4", "rule_name": "test", "rule_id": '12345hgfds21s245rwuisa',
        "_output_metadata": [
            {
                "column_name": "c1",
                "data_type": "varchar"
            },
            {
                "column_name": "c2",
                "data_type": "varchar"
            },
            {
                "column_name": "c3",
                "data_type": "numeric"
            },
            {
                "column_name": "c4",
                "data_type": "varchar"
            },
            {
                "column_name": "c5",
                "data_type": "varchar"
            }
        ]
    })
    workflow.unlock(rule['rule_id'])
    assert "test4" in workflow.get_dataset_names()


def test_workflow_update_rule_with_same_dataset_name_in_repetitive_workflow(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-1]
    output_metadata = [{"column_name": "c1", "data_type": "varchar"},
                       {"column_name": "c2", "data_type": "varchar"},
                       {"column_name": "c3", "data_type": "numeric"},
                       {"column_name": "c4", "data_type": "varchar"},
                       {"column_name": "new_c3", "data_type": "numeric"},
                       {"column_name": "sum_nrx2", "data_type": "numeric"
                        }]
    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "rule_3", "input": "rs1",
        "user_input": "partition rs1 to derive sum(nrx) as sum_nrx2 over (c1, c2)",
        "output": "rs1",
        "query": "select *, sum(nrx) over (partition by c1, c2) as sum_nrx2 from {schema}.{rs1}",
        "parameters": [],
        "partition": [{"formula": "sum(nrx)", "column_alias": "sum_nrx2", "partition_by": {"columns": ["c1", "c2"]}}],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c3', 'data_type': 'numeric'},
                        {'column_name': 'sum_nrx2', 'data_type': 'numeric'}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_update_rule_with_new_output_dataset_in_repetitive_workflow(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-1]
    output_metadata = [{"column_name": "c1", "data_type": "varchar"},
                       {"column_name": "c2", "data_type": "varchar"},
                       {"column_name": "c3", "data_type": "numeric"},
                       {"column_name": "c4", "data_type": "varchar"},
                       {"column_name": "new_c3", "data_type": "numeric", "derived": True},
                       {"column_name": "sum_nrx2", "data_type": "numeric", "derived": True}]
    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "rule_3", "input": "rs1",
        "user_input": "partition rs1 to derive sum(nrx) as sum_nrx2 over (c1, c2)",
        "output": "rs2",
        "query": "select *, sum(nrx) over (partition by c1, c2) as sum_nrx2 from {schema}.{rs1}",
        "parameters": [],
        "partition": [{"formula": "sum(nrx)", "column_alias": "sum_nrx2", "partition_by": {"columns": ["c1", "c2"]}}],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c3', 'data_type': 'numeric', "derived": True}],
                'rs2': [{"column_name": "c1", "data_type": "varchar"}, {"column_name": "c2", "data_type": "varchar"},
                        {"column_name": "c3", "data_type": "numeric"}, {"column_name": "c4", "data_type": "varchar"},
                        {"column_name": "new_c3", "data_type": "numeric", "derived": True},
                        {"column_name": "sum_nrx2", "data_type": "numeric", "derived": True}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_update_rule_with_new_dataset_name_in_middle_of_repetitive_workflow(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-2]
    output_metadata = [{"column_name": "c1", "data_type": "varchar"},
                       {"column_name": "c2", "data_type": "varchar"},
                       {"column_name": "c3", "data_type": "numeric"},
                       {"column_name": "c4", "data_type": "varchar"},
                       {"column_name": "new_c32", "data_type": "numeric", "derived": True}]
    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "second rule",
        "input": "rs1",
        "user_input": "derive columns in rs1 with 0.5*c3 as new_c32 of type numeric",
        "output": "rs2",
        "query": "select *, cast(0.5*c3 as numeric) as new_c32 from {schema}.{rs1}",
        "parameters": [],
        "derived": [
            {
                "formula": "0.5*c3",
                "column_alias": "new_c32",
                "data_type": "numeric"
            }
        ],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'sum_nrx', 'data_type': 'numeric', "derived": True}],
                'rs2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c32', 'data_type': 'numeric', "derived": True}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_update_rule_with_same_dataset_in_middle_of_repetitive_workflow(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-2]
    output_metadata = [{"column_name": "c1", "data_type": "varchar"},
                       {"column_name": "c2", "data_type": "varchar"},
                       {"column_name": "c3", "data_type": "numeric"},
                       {"column_name": "c4", "data_type": "varchar"},
                       {"column_name": "new_c32", "data_type": "numeric", "derived": True}]
    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "second rule",
        "input": "rs1",
        "user_input": "derive columns in rs1 with 0.5*c3 as new_c32 of type numeric",
        "output": "rs1",
        "query": "select *, cast(0.5*c3 as numeric) as new_c32 from {schema}.{rs1}",
        "parameters": [],
        "derived": [
            {
                "formula": "0.5*c3",
                "column_alias": "new_c32",
                "data_type": "numeric"
            }
        ],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c32', 'data_type': 'numeric', "derived": True},
                        {"column_name": "sum_nrx", "data_type": "numeric", "derived": True}]}
    assert repetitive_workflow.datasets.serialize == expected


def test_workflow_update_rule_with_other_then_same_dataset_in_middle_of_repetitive_workflow(repetitive_workflow):
    rule = db_repetitive_workflow['RULES'][-2]
    output_metadata = [{"column_name": "c1", "data_type": "varchar"},
                       {"column_name": "c2", "data_type": "varchar"},
                       {"column_name": "c3", "data_type": "numeric"},
                       {"column_name": "c4", "data_type": "varchar"},
                       {"column_name": "new_c32", "data_type": "numeric", "derived": True}]
    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "second rule",
        "input": "rs1",
        "user_input": "derive columns in rs1 with 0.5*c3 as new_c32 of type numeric",
        "output": "rs2",
        "query": "select *, cast(0.5*c3 as numeric) as new_c32 from {schema}.{rs1}",
        "parameters": [],
        "derived": [
            {
                "formula": "0.5*c3",
                "column_alias": "new_c32",
                "data_type": "numeric"
            }
        ],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {"column_name": "sum_nrx", "data_type": "numeric", "derived": True}],
                'rs2': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c32', 'data_type': 'numeric', "derived": True}]
                }
    assert repetitive_workflow.datasets.serialize == expected

    repetitive_workflow.lock(rule['rule_id'])
    repetitive_workflow.update_rule(rule['rule_id'], {
        "rule_name": "second rule",
        "input": "rs1",
        "user_input": "derive columns in rs1 with 0.5*c3 as new_c32 of type numeric",
        "output": "rs1",
        "query": "select *, cast(0.5*c3 as numeric) as new_c32 from {schema}.{rs1}",
        "parameters": [],
        "derived": [
            {
                "formula": "0.5*c3",
                "column_alias": "new_c32",
                "data_type": "numeric"
            }
        ],
        "_output_metadata": output_metadata
    })
    repetitive_workflow.unlock(rule['rule_id'])
    expected = {'ds1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'}],
                'rs1': [{'column_name': 'c1', 'data_type': 'varchar'}, {'column_name': 'c2', 'data_type': 'varchar'},
                        {'column_name': 'c3', 'data_type': 'numeric'}, {'column_name': 'c4', 'data_type': 'varchar'},
                        {'column_name': 'new_c32', 'data_type': 'numeric', "derived": True},
                        {"column_name": "sum_nrx", "data_type": "numeric", "derived": True}]}
    assert repetitive_workflow.datasets.serialize == expected
