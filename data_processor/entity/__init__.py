import logging
from abc import abstractmethod


class Entity:
    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)

    @property
    @abstractmethod
    def serialize(self):
        pass

    def __repr__(self):
        return str(self.serialize)
