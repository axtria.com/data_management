#   datamax/api/app.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Flask, request, jsonify
from flask.logging import default_handler
from werkzeug.local import LocalProxy

app = Flask(__name__)
app.config.from_pyfile('config.py')
app.url_map.strict_slashes = False
prefix = app.config.get('APPLICATION_URL')


class RequestFormatter(logging.Formatter):
    def format(self, record):
        record.url = request.url
        record.remote_addr = request.remote_addr
        return super().format(record)


formatter = RequestFormatter('[%(asctime)s]  - %(levelname)s  - [%(remote_addr)s] -  %(name)s - %(message)s')
default_handler.setFormatter(formatter)

logger = LocalProxy(lambda: app.logger)


@app.route(prefix + "/")
def app_index():
    app.logger.info("enter")
    return "DataMax"


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return jsonify(message="API does not exist.", traceback="404"), 404


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.errorhandler(Exception)
def all_exception_handler(error):
    logging.exception(error)
    return jsonify({"message": "run-time exception", "traceback": str(error)}), 500


@app.before_request
def before_request():
    logging.info('Request ::: %r' % request.url)


from controller.docs import docs
from controller.brms import brms
from controller.brms_validation import brms_validation
from controller.phrases import phrases
from controller.phrases_validation import phrases_validation

# APIs
app.register_blueprint(docs, url_prefix=prefix + '/api/docs')
app.register_blueprint(brms, url_prefix=prefix + '/api/brms')
app.register_blueprint(brms_validation, url_prefix=prefix + '/api/brms_validation')
app.register_blueprint(phrases, url_prefix=prefix + '/api/phrases')
app.register_blueprint(phrases_validation, url_prefix=prefix + '/api/phrases_validation')

logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs/system.log", when="midnight", interval=1),
                              logging.StreamHandler()],
                    format='%(asctime)s  - %(levelname)s  - %(name)s - %(message)s')

logging.getLogger('matplotlib').setLevel(logging.CRITICAL)

if __name__ == '__main__':
    app.run(port=app.config.get('PORT'), debug=True)
