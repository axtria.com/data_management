import logging
from logging.handlers import TimedRotatingFileHandler

from migration import m_01_2019_27nov_15dec

logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs/system.log", when="midnight", interval=1),
                              logging.StreamHandler()],
                    format='%(asctime)s  - %(levelname)s  - %(name)s - %(message)s')

if __name__ == '__main__':
    m_01_2019_27nov_15dec.invoke()
