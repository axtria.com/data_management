#   datamax/api/utils/tests/test_brms_transformer_join.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'C2', 'data_type': 'varchar'},
            {'column_name': 'C6', 'data_type': 'numeric'}],
    'ds4': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}],
    'rs1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar', 'derived': True}],
    'rs2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar', 'derived': True},
            {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}],
}
datasets = Datasets.initialize(dict_datasets)
datasets.merging_strategy = '#'


def test_join_rule_single_condition():
    tree = tokenize('join ds1 and ds2 using left join on ds1.c2 = ds2.c3 to get ds1.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'left',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} left join {schema_ds2}.{ds2} on '
                         '{schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_join_rule_single_condition_with_unalike_case():
    tree = tokenize('join DS1 and DS2 using left join on ds1.C2 = ds2.C3 to get dS1.*, dS2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'left',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 '
                         'from {schema_ds1}.{ds1} left join '
                         '{schema_ds2}.{ds2} on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_inner_join_rule_single_condition():
    tree = tokenize('join ds1 and ds2 using inner join on ds1.c2 = ds2.c3 to get ds1.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'inner',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} inner join '
                         '{schema_ds2}.{ds2} on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_inner_join_rule_multi_condition():
    tree = tokenize('join ds1 and ds2 using inner join on ds1.c2 = ds2.c3, ds1.c1 = ds2.c1 '
                    'to get ds1.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'inner',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3'), ('ds1.c1', '=', 'ds2.c1')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} inner join '
                         '{schema_ds2}.{ds2} on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3 and '
                         '{schema_ds1}.{ds1}.c1 = {schema_ds2}.{ds2}.c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_inner_join_rule_specific_columns():
    tree = tokenize('join ds1 and ds2 using inner join on ds1.c2 = ds2.c3, '
                    'ds1.c1 = ds2.c1 to get ds1.c1, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'inner',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3'), ('ds1.c1', '=', 'ds2.c1')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds2}.{ds2}.C5 as ds2#C5 '
                         'from {schema_ds1}.{ds1} inner join '
                         '{schema_ds2}.{ds2} on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3 and '
                         '{schema_ds1}.{ds1}.c1 = {schema_ds2}.{ds2}.c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_multi_join_rule_single_condition():
    tree = tokenize('join ds1 and ds2 using left join on ds1.c2 = ds2.c3 '
                    'and ds3 using right join on ds2.c2 = ds3.c2 '
                    'to get ds1.*, ds2.c5, ds3.c6', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_input': 'ds2', 'join_type': 'left', 'join_expr': [('ds1.c2', '=', 'ds2.c3')]},
                                   {'join_input': 'ds3', 'join_type': 'right',
                                    'join_expr': [('ds2.c2', '=', 'ds3.C2')]}],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'), ('ds2.C5', 'ds2#C5'),
                                     ('ds3.C6', 'ds3#C6')]
                         },
                'input': ['ds1', 'ds2', 'ds3'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5, {schema_ds3}.{ds3}.C6 as ds3#C6 from '
                         '{schema_ds1}.{ds1} left join {schema_ds2}.{ds2} on '
                         '{schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3 right join {schema_ds3}.{ds3} '
                         'on {schema_ds2}.{ds2}.c2 = {schema_ds3}.{ds3}.C2',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'ds3#C6', 'data_type': 'numeric'}]
                }
    assert actual == expected


def test_join_rule_single_condition_with_hashed_columns_should_not_add_hash():
    tree = tokenize('join ds1 and ds4 using left join on ds1.c2 = ds4.ds1#C1 to get ds1.c2, ds4.*', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'left',
                                    'join_expr': [('ds1.c2', '=', 'ds4.ds1#c1')],
                                    'join_input': 'ds4'
                                    }],
                         'columns': [('ds1.c2', 'ds1#c2'), ('ds4.ds1#c1', 'ds1#c1'),
                                     ('ds4.ds2#C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds4'],
                'query': 'select {schema_ds1}.{ds1}.c2 as ds1#c2, {schema_ds4}.{ds4}.ds1#c1 as ds1#c1, '
                         '{schema_ds4}.{ds4}.ds2#C5 as ds2#C5 from {schema_ds1}.{ds1} '
                         'left join {schema_ds4}.{ds4} on {schema_ds1}.{ds1}.c2 = {schema_ds4}.{ds4}.ds1#c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_join_rule_single_condition_with_dashed_dataset():
    tree = tokenize('join d-s4 and ds2 using left join on d-s4.ds#c2 = ds2.c3 to get d-s4.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': ['d-s4', 'ds2'],
                'join': {
                    'rules': [{'join_input': 'ds2', 'join_type': 'left', 'join_expr': [('d-s4.ds#c2', '=', 'ds2.c3')]}],
                    'columns': [('d-s4.d-s5#c1', 'd-s5#c1'), ('d-s4.ds#c2', 'ds#c2'), ('ds2.C5', 'ds2#C5')]},
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
                'query': 'select {schema_d-s4}.{d-s4}.d-s5#c1 as d-s5#c1, {schema_d-s4}.{d-s4}.ds#c2 as ds#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_d-s4}.{d-s4} left join {schema_ds2}.{ds2} '
                         'on {schema_d-s4}.{d-s4}.ds#c2 = {schema_ds2}.{ds2}.c3',
                'parameters': []}
    assert actual == expected


def test_join_with_different_join_expr():
    tree = tokenize('join ds1 and ds2 using left join on ds1.c2 >= ds2.c3 to get ds1.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'left',
                                    'join_expr': [('ds1.c2', '>=', 'ds2.c3')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')]
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} left join {schema_ds2}.{ds2} on '
                         '{schema_ds1}.{ds1}.c2 >= {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_join_rule_single_condition_with_derived_columns_hashed_strategy():
    tree = tokenize('join ds1 and rs2 using left join on ds1.c2 = ds2.c3 to get ds1.*, rs2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': ['ds1', 'rs2'],
                'join': {'rules': [{'join_input': 'rs2',
                                    'join_type': 'left',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3')]}],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'), ('rs2.C5', 'C5')]},
                'query': 'select {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_rs2}.{rs2}.C5 as C5 '
                         'from {schema_ds1}.{ds1} left join {schema_rs2}.{rs2} '
                         'on {schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}]}
    assert actual == expected


def test_join_rule_with_both_derived_columns_hashed_strategy():
    tree = tokenize('join rs1 and rs2 using left join on rs1.c2 = rs2.c3 to get rs1.*, rs2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': ['rs1', 'rs2'],
                'join': {'rules': [{'join_input': 'rs2',
                                    'join_type': 'left',
                                    'join_expr': [('rs1.c2', '=', 'rs2.c3')]}],
                         'columns': [('rs1.c1', 'rs1#c1'), ('rs1.c2', 'c2'), ('rs2.C5', 'C5')]},
                '_output_metadata': [{'column_name': 'rs1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar', 'derived': True},
                                     {'column_name': 'C5', 'data_type': 'varchar', 'derived': True}],
                'query': 'select {schema_rs1}.{rs1}.c1 as rs1#c1, {schema_rs1}.{rs1}.c2 as c2, '
                         '{schema_rs2}.{rs2}.C5 as C5 '
                         'from {schema_rs1}.{rs1} left join {schema_rs2}.{rs2} '
                         'on {schema_rs1}.{rs1}.c2 = {schema_rs2}.{rs2}.c3',
                'parameters': []}
    assert actual == expected


def test_join_rule_single_condition_with_distinct():
    tree = tokenize('join ds1 and ds2 using left join on ds1.c2 = ds2.c3 with distinct to get ds1.*, ds2.c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_type': 'left',
                                    'join_expr': [('ds1.c2', '=', 'ds2.c3')],
                                    'join_input': 'ds2'
                                    }],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'),
                                     ('ds2.C5', 'ds2#C5')],
                         'distinct': 'distinct'
                         },
                'input': ['ds1', 'ds2'],
                'query': 'select distinct {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5 from {schema_ds1}.{ds1} left join {schema_ds2}.{ds2} on '
                         '{schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_multi_join_rule_single_condition_with_distinct():
    tree = tokenize('join ds1 and ds2 using left join on ds1.c2 = ds2.c3 '
                    'and ds3 using right join on ds2.c2 = ds3.c2 '
                    'with distinct to get ds1.*, ds2.c5, ds3.c6', datasets)
    actual = to_json(tree, datasets)
    expected = {'join': {'rules': [{'join_input': 'ds2', 'join_type': 'left', 'join_expr': [('ds1.c2', '=', 'ds2.c3')]},
                                   {'join_input': 'ds3', 'join_type': 'right',
                                    'join_expr': [('ds2.c2', '=', 'ds3.C2')]}],
                         'columns': [('ds1.c1', 'ds1#c1'), ('ds1.c2', 'ds1#c2'), ('ds2.C5', 'ds2#C5'),
                                     ('ds3.C6', 'ds3#C6')],
                         'distinct': 'distinct'
                         },
                'input': ['ds1', 'ds2', 'ds3'],
                'query': 'select distinct {schema_ds1}.{ds1}.c1 as ds1#c1, {schema_ds1}.{ds1}.c2 as ds1#c2, '
                         '{schema_ds2}.{ds2}.C5 as ds2#C5, {schema_ds3}.{ds3}.C6 as ds3#C6 from '
                         '{schema_ds1}.{ds1} left join {schema_ds2}.{ds2} on '
                         '{schema_ds1}.{ds1}.c2 = {schema_ds2}.{ds2}.c3 right join {schema_ds3}.{ds3} '
                         'on {schema_ds2}.{ds2}.c2 = {schema_ds3}.{ds3}.C2',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds1#c2', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'},
                                     {'column_name': 'ds3#C6', 'data_type': 'numeric'}]
                }
    assert actual == expected
