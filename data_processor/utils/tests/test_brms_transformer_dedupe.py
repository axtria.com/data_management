#   datamax/api/utils/tests/test_brms_transformer_dedupe.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}

datasets = Datasets.initialize(dict_datasets)


def test_dedupe_on_single_column():
    tree = tokenize('dedupe ds1 on c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1', 'dedupe': ['c1'],
                'query': 'select A.* from {schema_ds1}.{ds1} A inner join (select c1, count(*) from {schema_ds1}.{ds1}'
                         ' group by c1 '
                         'having count(*) = 1) B on A.c1 = B.c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                }
    assert actual == expected


def test_dedupe_on_multi_column():
    tree = tokenize('dedupe ds1 on c1, c2', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1', 'dedupe': ['c1', 'c2'],
                'query': 'select A.* from {schema_ds1}.{ds1} A inner join (select c1, c2, count(*) from '
                         '{schema_ds1}.{ds1} group by '
                         'c1, c2 having count(*) = 1) B on A.c1 = B.c1 and A.c2 = B.c2',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_dedupe_on_single_column_alternate_case_in_dataset_name():
    tree = tokenize('dedupe ds1 on C1', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds1', 'dedupe': ['c1'],
                'query': 'select A.* from {schema_ds1}.{ds1} A inner join (select c1, count(*) from {schema_ds1}.{ds1}'
                         ' group by c1 having count(*) = 1) B on A.c1 = B.c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                }
    assert actual == expected


def test_dedupe_on_multi_column_alternate_case_in_column_name():
    tree = tokenize('dedupe ds2 on c1, C2, c5', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds2', 'dedupe': ['c1', 'c2', 'C5'],
                'query': 'select A.* from {schema_ds2}.{ds2} A inner join (select c1, c2, C5, '
                         'count(*) from {schema_ds2}.{ds2} '
                         'group by c1, c2, C5 having count(*) = 1) B on A.c1 = B.c1 and A.c2 = B.c2 and A.C5 = B.C5',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_dedupe_on_single_column_with_hash_column_name():
    tree = tokenize('dedupe ds3 on ds1#c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'ds3', 'dedupe': ['ds1#c1'],
                'query': 'select A.* from {schema_ds3}.{ds3} A inner join (select ds1#c1, '
                         'count(*) from {schema_ds3}.{ds3}'
                         ' group by ds1#c1 '
                         'having count(*) = 1) B on A.ds1#c1 = B.ds1#c1',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_dedupe_on_single_column_with_dashed_dataset():
    tree = tokenize('dedupe d-s4 on d-s5#c1', datasets)
    actual = to_json(tree, datasets)
    expected = {'input': 'd-s4', 'dedupe': ['d-s5#c1'],
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'}],
                'query': 'select A.* from {schema_d-s4}.{d-s4} A inner join (select d-s5#c1, count(*) from '
                         '{schema_d-s4}.{d-s4} group by d-s5#c1 having count(*) = 1) B on A.d-s5#c1 = B.d-s5#c1',
                'parameters': []}
    assert actual == expected
