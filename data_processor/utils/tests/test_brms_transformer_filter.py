#   datamax/api/utils/tests/test_brms_transformer_filter.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
from entity.datasets import Datasets
from utils.brms_grammar import tokenize
from utils.brms_transformer import to_json

dict_datasets = {
    'ds1': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'}],
    'ds2': [{'column_name': 'c1', 'data_type': 'varchar'},
            {'column_name': 'c2', 'data_type': 'varchar'},
            {'column_name': 'c3', 'data_type': 'numeric'},
            {'column_name': 'c4', 'data_type': 'varchar'},
            {'column_name': 'C5', 'data_type': 'varchar'}],
    'ds3': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
            {'column_name': 'ds2#C5', 'data_type': 'varchar'}],
    'd-s4': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
             {'column_name': 'ds#c2', 'data_type': 'varchar'}]
}
datasets = Datasets.initialize(dict_datasets)


def test_filter_rule_single_condition():
    tree = tokenize('filter ds1 where (c1 > 0)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c1 > 0',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_single_condition_unalike_case():
    tree = tokenize('filter ds1 where (C1 > 0)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c1 > 0',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_single_condition_with_parameters():
    tree = tokenize('filter ds1 where (c2 > $threshold)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c2 > $threshold',
                'parameters': ['$threshold'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_single_condition_with_parameters_unalike_case():
    tree = tokenize('filter ds1 where (C2 > $threshold)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c2', 'OPERATOR': '>', 'VALUE': '$threshold'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c2 > $threshold',
                'parameters': ['$threshold'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_nested_conditions_without_parameters():
    tree = tokenize('filter ds2 where OR((c1 > 0) (c3 = true))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'condition': 'OR',
                     'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'},
                          {'COLUMN': 'c3', 'OPERATOR': '=', 'VALUE': 'true'}]
                     },
                'input': 'ds2',
                'query': 'select * from {schema_ds2}.{ds2} where (c1 > 0) or (c3 = true)',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_nested_conditions_with_comma():
    tree = tokenize('filter ds1 where OR((c1 > 0),(c2 = true))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'condition': 'OR',
                     'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'},
                          {'COLUMN': 'c2', 'OPERATOR': '=', 'VALUE': 'true'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where (c1 > 0) or (c2 = true)',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_nested_conditions_with_parameters():
    tree = tokenize('filter ds1 where OR((c1 > $threshold) (C2 = true))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'condition': 'OR',
                     'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '$threshold'},
                          {'COLUMN': 'c2', 'OPERATOR': '=', 'VALUE': 'true'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where (c1 > $threshold) or (c2 = true)',
                'parameters': ['$threshold'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]

                }
    assert actual == expected


def test_filter_rule_double_nested_conditions_with_parameters():
    tree = tokenize('filter ds2 where AND(OR((c1 > $threshold)(C2 = true)) (C3 > 0))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'condition': 'AND',
                           'rules': [{'condition': 'OR',
                                      'rules': [{'COLUMN': 'c1', 'OPERATOR': '>',
                                                 'VALUE': '$threshold'},
                                                {'COLUMN': 'c2', 'OPERATOR': '=',
                                                 'VALUE': 'true'}]},
                                     {'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '0'}]},
                'input': 'ds2',
                'query': 'select * from {schema_ds2}.{ds2} where ((c1 > $threshold) or (c2 = true)) and (c3 > 0)',
                'parameters': ['$threshold'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_double_nested_conditions_with_parameters_with_comma():
    tree = tokenize('filter ds2 where AND(OR((C1 > $threshold), (C2 = true)), (c3 > 0))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'condition': 'AND',
                           'rules': [{'condition': 'OR',
                                      'rules': [{'COLUMN': 'c1', 'OPERATOR': '>',
                                                 'VALUE': '$threshold'},
                                                {'COLUMN': 'c2', 'OPERATOR': '=',
                                                 'VALUE': 'true'}]},
                                     {'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '0'}]},
                'input': 'ds2',
                'query': 'select * from {schema_ds2}.{ds2} where ((c1 > $threshold) or (c2 = true)) and (c3 > 0)',
                'parameters': ['$threshold'],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'},
                                     {'column_name': 'c3', 'data_type': 'numeric'},
                                     {'column_name': 'c4', 'data_type': 'varchar'},
                                     {'column_name': 'C5', 'data_type': 'varchar'}]

                }
    assert actual == expected


def test_filter_rule_on_columns():
    tree = tokenize('filter ds1 using columns C1, C2', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'columns': ['c1', 'c2']
                     },
                'input': 'ds1',
                'query': 'select c1, c2 from {schema_ds1}.{ds1}',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_on_columns_and_rows():
    tree = tokenize('filter ds1 using columns C1, C2 where (C1 > 0)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '>', 'VALUE': '0'}],
                     'columns': ['c1', 'c2']
                     },
                'input': 'ds1',
                'query': 'select c1, c2 from {schema_ds1}.{ds1} where c1 > 0',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_null_condition():
    tree = tokenize('filter ds1 where (C1 is null)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': 'is', 'VALUE': 'null'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c1 is null',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]

                }
    assert actual == expected


def test_filter_rule_not_null_condition():
    tree = tokenize('filter ds1 where (c2 is not null)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c2', 'OPERATOR': 'is not', 'VALUE': 'null'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c2 is not null',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_not_null_condition_upper_case():
    tree = tokenize('filter ds1 where (c2 IS Not null)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c2', 'OPERATOR': 'IS Not', 'VALUE': 'null'}]
                     },
                'input': 'ds1',
                'query': 'select * from {schema_ds1}.{ds1} where c2 IS Not null',
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_with_hash_column_name():
    tree = tokenize('filter ds3 where (ds2#c5 is not null)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'ds2#C5', 'OPERATOR': 'is not', 'VALUE': 'null'}]
                     },
                'input': 'ds3',
                'query': 'select * from {schema_ds3}.{ds3} where ds2#C5 is not null',
                'parameters': [],
                '_output_metadata': [{'column_name': 'ds1#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds2#C5', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_with_space_values():
    tree = tokenize("filter ds1 where (c1 = 'My Brand')", datasets)
    actual = to_json(tree, datasets)
    expected = {'filter':
                    {'rules':
                         [{'COLUMN': 'c1', 'OPERATOR': '=', 'VALUE': "'My Brand'"}]
                     },
                'input': 'ds1',
                'query': "select * from {schema_ds1}.{ds1} where c1 = 'My Brand'",
                'parameters': [],
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}]
                }
    assert actual == expected


def test_filter_rule_single_condition_with_dashed_dataset():
    tree = tokenize('filter d-s4 where (d-s5#c1 > 0)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'COLUMN': 'd-s5#c1', 'OPERATOR': '>', 'VALUE': '0'}]},
                'input': 'd-s4',
                '_output_metadata': [{'column_name': 'd-s5#c1', 'data_type': 'varchar'},
                                     {'column_name': 'ds#c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_d-s4}.{d-s4} where d-s5#c1 > 0',
                'parameters': []}
    assert actual == expected


def test_filter_rule_with_formula():
    tree = tokenize('filter ds1 where ({day(c1)} > 0)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'FORMULA': 'day(c1)', 'OPERATOR': '>', 'VALUE': '0'}]}, 'input': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_ds1}.{ds1} where day(c1) > 0', 'parameters': []}
    assert actual == expected


def test_filter_rule_with_nested_formula():
    tree = tokenize('filter ds1 where OR(({c3*0.5 + 1 / 23} > 0)( c3 > 0))', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'condition': 'OR',
                           'rules': [{'FORMULA': 'c3*0.5 + 1 / 23', 'OPERATOR': '>', 'VALUE': '0'},
                                     {'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '0'}]},
                'input': 'ds1', '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_ds1}.{ds1} where (c3*0.5 + 1 / 23 > 0) or (c3 > 0)', 'parameters': []}
    assert actual == expected


def test_filter_rule_with_dates_in_value():
    tree = tokenize('filter ds1 where (c3 > 21/01/2019)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '21/01/2019'}]},
                'input': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_ds1}.{ds1} where c3 > 21/01/2019', 'parameters': []}
    assert actual == expected


def test_filter_rule_with_dashes_in_value():
    tree = tokenize('filter ds1 where (c3 > 21-01-2019)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '21-01-2019'}]},
                'input': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_ds1}.{ds1} where c3 > 21-01-2019', 'parameters': []}
    assert actual == expected


def test_filter_rule_with_dots_in_value():
    tree = tokenize('filter ds1 where (c3 > 21.01.2019)', datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'COLUMN': 'c3', 'OPERATOR': '>', 'VALUE': '21.01.2019'}]},
                'input': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': 'select * from {schema_ds1}.{ds1} where c3 > 21.01.2019', 'parameters': []}
    assert actual == expected


def test_filter_rule_with_partial_match():
    tree = tokenize("filter ds1 where (c1 like '%segment%')", datasets)
    actual = to_json(tree, datasets)
    expected = {'filter': {'rules': [{'COLUMN': 'c1', 'OPERATOR': 'like', 'VALUE': "'%segment%'"}]},
                'input': 'ds1',
                '_output_metadata': [{'column_name': 'c1', 'data_type': 'varchar'},
                                     {'column_name': 'c2', 'data_type': 'varchar'}],
                'query': "select * from {schema_ds1}.{ds1} where c1 like '%segment%'",
                'parameters': []}
    assert actual == expected
