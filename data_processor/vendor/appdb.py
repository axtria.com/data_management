#   data_management/vendor/appdb.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import json
import logging
import os

from config import DATABASE_PATH, RECYCLE_BIN
from utils.helpers import decoder, encoder


class AppDb:
    """File system based App Database

    Singleton behavior to avoid multiple instances of engine to form for a given file system.
    """

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.base_path = DATABASE_PATH
        self.recycle_bin = RECYCLE_BIN

    def query(self, obj_name: str, obj_id: str) -> dict:
        """

        Args:
            obj_name:
            obj_id:  is the primary key on which object is filtered.

        Returns:

        """
        with open(self.base_path + obj_name + os.path.sep + obj_id + '.txt') as json_file:
            self.log.debug("reading file -- %r" % self.base_path + obj_name + os.path.sep + obj_id + '.txt')
            return json.loads(decoder(json_file.read()))

    def query_all(self, obj_name: str) -> list:
        """

        Args:
            obj_name:
            obj_id:  is the primary key on which object is filtered.

        Returns:

        """
        items = []
        for obj_file in os.scandir(self.base_path + obj_name):
            with open(self.base_path + obj_name + os.path.sep + obj_file.name) as json_file:
                self.log.debug("reading file -- %r" % self.base_path + obj_name + os.path.sep + obj_file.name)
                items.append(json.loads(decoder(json_file.read())))
        return items

    def save(self, obj_name: str, obj_id: str, obj):
        """

        Args:
            obj_name:
            obj_id:
            obj:

        Returns:

        """
        with open(self.base_path + obj_name + os.path.sep + obj_id + '.txt', 'w') as json_file:
            self.log.debug("saving file -- %r" % self.base_path + obj_name + os.path.sep + obj_id + '.txt')
            return json_file.write(encoder(json.dumps(obj)))

    def remove(self, obj_name: str, obj_id: str):
        self.log.debug('removing file -- %r' % self.base_path + obj_name + os.path.sep + obj_id + '.txt')
        os.replace(self.base_path + obj_name + os.path.sep + obj_id + '.txt', self.recycle_bin + obj_id + '.txt')
