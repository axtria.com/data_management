#   data_management/vendor/postgres.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError

from config import SQLALCHEMY_POSTGRES_URI
from utils.exceptions import PostgresConnectionFailed


class Postgres:
    """Postgres class to connect and execute queries on postgres database.
    """

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        try:
            self.engine = create_engine(SQLALCHEMY_POSTGRES_URI, isolation_level="AUTOCOMMIT")
        except Exception as e:
            self.log.exception(e)
            raise PostgresConnectionFailed

    def execute(self, query):
        self.log.debug(query)
        try:
            return pd.read_sql(query, self.engine)
        except ResourceClosedError:
            return None
