#   data_management/vendor/redshift.py
#   Copyright (C) 2019 Axtria.
#
#   Author: Varun Singhal <varun.singhal@axtria.com>
import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.exc import ResourceClosedError

from config import SQLALCHEMY_REDSHIFT_URI
from utils.exceptions import RedshiftConnectionFailed


class Redshift:
    """Redshift class to connect and execute queries on Redshift cluster.
    Credentials are taken from config file and connected to cluster whenever object is called.
    No explicit operator required for commit DML statements as autocommit is True.

    """
    numeric_dtypes = ['smallint', 'integer', 'bigint', 'numeric', 'real', 'double precision']

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        try:
            self.engine = create_engine(SQLALCHEMY_REDSHIFT_URI, isolation_level="AUTOCOMMIT")
        except Exception as e:
            self.log.exception(e)
            raise RedshiftConnectionFailed

    def execute(self, query):
        self.log.debug(query)
        try:
            return pd.read_sql(query, self.engine)
        except ResourceClosedError:
            return None
