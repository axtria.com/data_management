public class DataManagementTemplatization{
    string tenant;
    public string pSFDCId{get;set;}
    public t_plan__c plan{get;set;}
    string planUuid;
    public string UICurrentFile;
    public Boolean SavedComponent{get;set;}
    public string currentTabName;
    public string planInstanceID{get;set;}
    public string jsonValue{get;set;}
    public list<wrapperInputs> listInputs{get;set;}
     public List<Object> objList = new List<Object>();
    public string folderId;
    public string Value{get;set;}
    public boolean calledForInput{get;set;}
    public string tenantAbbName;
    public string serverName;
    String type='DataMax';
    
    public boolean planstatus{get;set;}
    
    
    public List<SelectOption> options;
    public List<String> SelectedFrequency {get; set;}
    public list<t_plan__c> query{get;set;}
    public string TimePeriod{get;set;}
    string S3Prefix;
    public string columnStatic {get;set;}
    
    Map<string,object> columnMapping;
    public string InputType{get;set;}
    
    
    
    public Map<string,Object> IDMapListing{get;set;} 
    //DataMax
    public string TypeFileCalculation;
    public string ModuleType;
    public string statusJSON{get;set;}
    public string validationJSON{get;set;}
    public string bucket;
    public string mappingColmns{get;set;}
    public string typeColumnsStatic{get;set;}
    public string optionsMapping{get;set;}
    public Map<string,object> statusJSONMap;
    public Map<string,object> ValidationJSONMap;
    public boolean StatusSaved{get;set;}
    public string SelectedFolder{get;set;}
    public list<SelectOption> Foldersavailable{get;set;}
    public string initialfolder{get;set;}
    public map<string,string> TableNameMap;
    public DataManagementTemplatization(){
        ETL_Config__c etl =[Select Org_Type__c,S3_Bucket__c,Esri_Server_IP__c,S3_Filename__c,Server_Type__c from  ETL_Config__c where name='DataManagement'];
        UICurrentFile='UI_File_SC';
        //S3Prefix='Unmanaged';
        S3Prefix = etl.Server_Type__c;
        //tenant = 'ART';
        tenant = etl.Org_Type__c;
        tenantAbbName=etl.Org_Type__c;
        //bucket='sales-iq-ng-782';
        bucket=etl.S3_Bucket__c;
        
        if(S3prefix==null)
            S3Prefix='';
        //initialfolder='DEV/';
        initialfolder=etl.S3_Filename__c+'/';
        //serverName='https://salesiq.axtria.com/';
        servername = etl.Esri_Server_IP__c;
        pSFDCId=System.currentPageReference().getParameters().get('Id');
        ModuleType=System.currentPageReference().getParameters().get('ModuleType');
        plan = new t_plan__c();
        currentTabName='Plan';
        
        TimePeriod='abc';
        
        planstatus=false;
        
        
        
        Map<string,Object> IDMapListing = new Map<string,Object>();
        query=new list<t_plan__c>();
        selectedFrequency = new List<String>();
        
        if(pSFDCId==null|| pSFDCId==''){
            planUuid = newPlan();
            plan.plan_uuid__c =planUuid;
            pSFDCid =plan.id;
            
            SavedComponent=true;
            
            openUIFile('plan','','');
            
        }
        else{ 
            plan =  [Select id,Navigation_from__c,Data_Object__c,Execution_Status__c,Plan_Name__c,Instance__c,Last_Executed_On__c,plan_uuid__c,Time_Period__c,componentstatus__c  from t_plan__c where id=:pSFDCId];
            planUuid  = plan.plan_uuid__c ;
            string planid =pSFDCId;
            
            
            
            saveTheJSONfile('',0,false,'');
            //planWorkflow=true;
            
            gotoplan();
            
            
           
            
        }
        getStatusValidation();
        jsonValue ='';
        
        
        
        
        
       
       Foldersavailable = new list<selectoption>();
       getfolders();
    }
    
    //When tab for a component is selected
    public void selectAComponentTab(){
        currentTabName= ApexPages.CurrentPage().getParameters().get('tabName'); 
        string calledfor= ApexPages.CurrentPage().getParameters().get('calledFor');
        System.debug(calledfor+'tabname==='+currentTabName);
        
            
            
            system.debug('here');
            
            
            
             if(currentTabName!='Plan'){
                 
             }
             else{
                 openUIFile('Plan','','');
                 SavedComponent =true;
                
             }
         //}
    }
    
    
    public void goToPlan(){
        //openUIFile('plan','','');
        CurrentTabName='Plan';
        UICurrentFile='UI_File_SC';
        
        //saveTheJSONfile('',0,false,'');
        
        openUIFile('Plan', '', '');
        SavedComponent=False;
        
        
        
    }
    
    public void goToTemplate(){
        CurrentTabName='Component';
        UICurrentFile='UI_File_Plan';
        
        //saveTheJSONfile('',0,false,'');
        
        openUIFile('Component', '', plan.templatedescription__c);
        SavedComponent=true;
       
       
        
    }
    public void openPlanInputs(){
        if(CurrentTabName=='Plan'){
            openUIFile('plan','','');
            
            
        }
        
       
    }
    public void showTemplateJSON(){
        
        openUIFile('Template' , '', '');
        
    }
    public void closeTemplatePopup(){
        
        System.debug(currenttabname+'===');
        
        
        currenttabname='Plan';
    }
    public void selectATemplate(){
        
        plan.componentstatus__c=true;
       // formrender=true;
        string templateID= ApexPages.CurrentPage().getParameters().get('templateID');
        string TemplateName = ApexPages.CurrentPage().getParameters().get('templateName');
        System.debug('======'+templateID+'======='+TemplateName);
        // If a component is copied from a component, paramter copy will be true and copy tab will be name of the tab of the component it is copied from
        //string copy= ApexPages.CurrentPage().getParameters().get('copy');
        //string copytabName= ApexPages.CurrentPage().getParameters().get('copytabName');
        string uiFileName;
        uiFileName=plan.templatedescription__c;
        
        
        if(uiFileName==null || uiFileName=='')
            uiFileName='None';
        openUIFile('component',templateID,uiFileName); 
        
        SavedComponent =true;
        
        
        currenttabname='Component';
    }
    
    
    public void createListForComponentsTab(){
        
    }
    
    //This call for a webservice that saves the planUI File and component UIFile
    public void saveTheJSON(){
        string jsonBody;
        
        jsonValue =ApexPages.CurrentPage().getParameters().get('jsonValue'); 
        string tabName = (ApexPages.CurrentPage().getParameters().get('tabName'));
        planstatus=true;
        
        
        saveTheJSONfile( jsonValue, 0,true ,tabName );
        if(tabName!='Plan'){
            plan.componentstatus__c=true;
            update plan;
        }
        system.debug('tabname======='+tabname);
        
    }
    
    // Called form SavetheJSON
    public boolean saveTheJSONfile(string jsonBody, integer noOfComponentsValue , boolean Save , string compName){
        HttpRequest req = new HttpRequest(); 
        req.setMethod('POST');
        //Remove later
        integer error=0;
        system.debug('tabname======='+currentTabName);
        System.debug('jsonBody-------'+jsonBody);
        if(currentTabName=='Plan' && jsonBody!='' ){
           Map<string,Object> Parameters= (Map<string,Object>)JSON.deserializeUntyped(jsonBody);
           List<object> ParamList =  (List<object>)Parameters.get('Parameters');
           for(Object o : ParamList){
               Map<string,object> nodeMap= (Map<string,object>)o;
               string Name=(string)nodeMap.get('Name');
               if(Name=='Component Info'){
                 List<object> paramListsecond =(List<object>)nodeMap.get('Params');
                 for(Object o2 :paramListsecond){
                   Map<string,object> o2Map = (Map<string,object>)o2;
                   string SFDCfield = string.valueof(o2Map.get('SFDC'));
                   string Value= string.valueof(o2Map.get('Value'));
                   
                   if(SFDCfield =='Name'){
                       plan.name=Value;
                       plan.Plan_Name__c=Value;
                       for(t_plan__c planExist : [Select id,name from t_plan__C where plan_name__c=:Value and id!=:plan.id  and type__c=:type]){
                          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,('Name already exists' )));
                          Error=1;
                          Break;  
                       }
                    }
                    if(SFDCfield=='Category__c'){
                        plan.category__c=value;
                    }
                    if(SFDCfield=='Data_Object__c'){
                        plan.Data_Object__c=value;
                    }
                    if(SFDCfield=='Module__c'){
                        plan.Module__c=value;
                    }
                    
                  }
                }  
               
               
           }
            
           
        }
        if(error!=1){
        string endpoint = serverName+S3prefix+'DataMaxUIService/Save/';
        req.setEndpoint(endpoint);
        List<String> urlParams;
        if(pSFDCId==null|| pSFDCId==''){
            urlParams = new List<String> {
                'Name=' + UICurrentFile,
                'planID=' + planUuid ,
                'data=' + jsonBody,
                'tenant='+tenantAbbName,
                'type='+type,
                'Saved='+plan.ComponentStatus__c
                
            };
        
        }
        else{
            urlParams = new List<String> {
                //'filename=' + plan.TemplateDescription__c,
                'Name=' +UICurrentFile ,
                'planID=' + planUuid ,
                'data=' + jsonBody,
                'tenant='+tenantAbbName,
                'type='+type,
                'Saved='+plan.ComponentStatus__c
        };
        
        }
        
        String body = String.join(urlParams, '&');
        System.debug(body );
        req.setBody(body);    
        req.setTimeOut(120000);
        Http http = new Http();
        System.debug(endpoint );
        HTTPResponse response= http.send(req);
        system.debug('currentTabName==='+currentTabName);
        if(currentTabName=='Plan' ){
           List<String> urlbody = new List<String> {
               'planID=' + planUuid ,
               'dataValue=' +json.serialize(idMapListing),
               'tenant='+tenantAbbName,
               'type='+type
                
           };
           string url=serverName+S3prefix+'DataMaxUIService/SaveMapping/';
           String bodyurl = String.join(urlbody, '&');
           HttpRequest request1 = new HttpRequest(); 
           request1.setEndpoint(url);
            request1.setMethod('POST');
           request1.setBody(bodyurl );    
           request1.setTimeOut(120000);
           Http httpreq = new Http();
           //Save the JSON Measure
           HTTPResponse response1=httpreq.send(request1); 
           List<String> urlbody2 = new List<String> {
               'planID=' + planUuid ,
               'ValidationStatus=' +json.serialize(ValidationJSONMap),
               'MappingStatus='+json.serialize(statusJSONMap),
               'tenant='+tenantAbbName,
               'type='+type,
               'TableNames=NA'
              
                
           };
           string url2=serverName+S3prefix+'DataMaxUIService/SaveValidation/';
           String bodyurl2 = String.join(urlbody2, '&');
           system.debug(bodyurl2);
           HttpRequest request2 = new HttpRequest(); 
           request2.setEndpoint(url2);
           request2.setMethod('POST');
           request2.setBody(bodyurl2 );    
           request2.setTimeOut(120000);
           Http httpreq2 = new Http();
           HTTPResponse response2=httpreq2.send(request2); 
           
            
            if(Save==True){
                plan.description__c = UICurrentFile;
                if(plan.id==null){
                    system.debug('folderId'+folderId);
                     plan.execution_status__c='Complete';
                     plan.type__c='DataMax';
                     plan.ModuleTypeFile__c=ModuleType;
                     system.debug('====='+ModuleType);
                     if(ModuleType.contains('Inbound')){
                         plan.Navigation_from__c='S3 To '+plan.Module__c;
                     }
                    insert plan;
                }
                else{
                    plan.execution_status__c='Complete';
                    upsert plan;
                    
            
                }
                //createListForComponentsTab();
                
                pSFDCID=plan.id;
                system.debug('response.getBody()========='+response.getBody());
                system.debug('currentTabName===='+currentTabName);
                
                if(currentTabName!='Plan'){
                    
                    Map<string,Object> resBody = (Map<string,Object>)JSON.deserializeUntyped(response.getBody());
                    
                   }
                  else{
                     if(response.getBody()=='Success'){
                         
                     }  
                 }
                
            }
            
            
            
        }
       
         
      }
      
      
      return null;  
    }
    
    //This call for a webservice that creates a new plan instance
    public string newPlan(){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint=serverName+S3prefix+'DataMaxUIService/CreateDataMax/?tenant='+tenantAbbname+'&type='+type+'&ModuleType='+ModuleType;
        system.debug('endpoint'+endpoint);
        
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        string resBody = response.getBody();
        system.debug('resBody '+resBody );    
        return string.valueof(resBody);
        
   }
   public void saveTheDefiniton(){}
   public void getStatusValidation(){
        http h = new http();
        HttpRequest request = new HttpRequest();
        String endPoint=serverName+S3prefix+'DataMaxUIService/getStatusValidation/?planID='+planUuid+'&tenant='+tenantAbbname+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        Map<string,object> resBody= (Map<string,object>)JSON.deserializeUntyped(response.getBody());
        system.debug(resBody);
        statusJSONMap = new map<string,object>();
        ValidationJSONMap = new map<string,object>();
        TableNameMap=new map<string,string>();
        List<Object> Mappingstatus = (List<Object>)resBody.get('MappingStatus');
        List<Object> Validationstatus = (List<Object>)resBody.get('ValidationStatus');
        List<Object> TableNameslist = (List<Object>)resBody.get('TableNames');
        
        for(object o:Mappingstatus){
            Map<string,object> obj =  (Map<string,object>)o;
            system.debug('===obj'+obj);
            for(string key:obj.keyset()){
                system.debug(obj.get(key));
                if(obj.get(key)!=null && obj.get(key)!='')
                    statusJSONMap.put(key,string.valueof(obj.get(key)));
                else
                    statusJSONMap.put(key,'');
            }
        }
        for(object o: Validationstatus ){
            Map<string,object> obj =  (Map<string,object>)o;
            for(string key:obj.keyset()){
                
                if(obj.get(key)!=null && obj.get(key)!='')
                    ValidationJSONMap.put(key,string.valueof(obj.get(key)));
                else
                    ValidationJSONMap.put(key,'');
            }
        }
        for(object o: TableNameslist ){
            Map<string,object> obj =  (Map<string,object>)o;
            for(string key:obj.keyset()){
                
                if(obj.get(key)!=null && obj.get(key)!='')
                    TableNameMap.put(key,string.valueof(obj.get(key)));
                else
                    TableNameMap.put(key,'');
            }
        }
        system.debug(statusJSONMap);
        statusJSON= JSON.serialize(statusJSONMap);
        validationJSON=JSON.serialize(ValidationJSONMap);
        system.debug('---');
        system.debug(statusJSON);
        system.debug(validationJSON);
        for(string str :statusjsonmap.keyset()){
          if(statusjsonmap.get(str)!='Mapping Failed'){
              StatusSaved=true;
          }
          else{
              StatusSaved=false;
              break;
          }
      }
   }
   
   public void getExecutionJSON(){
        
        
   }
   
   public void getMTWorkflow(string filename, string filetype, string fileid, string columns){
        
   }
   
        
        
   
   // Provides the json for the dynamic UI. 
   // UIFileType should be 'Plan' for plan otherwise can be anything
   public void openUIFile(string UIFileType, string typeTemplate, string compUIFileName){
       
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint='';
        
        endPoint=serverName+S3prefix+'DataMaxUIService/Plan/?planID='+planUuid+'&tenant='+tenantAbbname+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response = h.send(request);
        Map<string,Object> resBody = (Map<string,Object>)JSON.deserializeUntyped(response.getBody());
        Object nameOfUIFile = (Object)resBody.get('filename');
        UICurrentFile = string.valueof(nameofUIFile);
        Object templateName= (Object)resBody.get('TemplateName');
        
        
        
        objList = new list<object>();
        objList = (List<object>)resBody.get('Parameters');
        columnMapping = new map<string,object>();
        columnMapping =(Map<string,object>)resBody.get('columnMap');
        Map<string,object> resbody2 = new map<string,object>();
        resbody2.put('Parameters',objList);
        system.debug('value========'+resbody2);
        Value = JSON.serialize(resbody2);
        getMappingfromJSON();
        
        
    }
    public void SaveMapping(){
        string sortedColumns=ApexPages.CurrentPage().getParameters().get('sortedColumns');
        system.debug('columns==='+sortedColumns);
        sortedColumns=sortedColumns.removeend(',');
        Map<string,object> inputColumnSpecific = (Map<string,object>)IdMapListing.get(inputTypeListing); 
        system.debug(inputColumnSpecific );
        inputColumnSpecific.put('MappingCol',sortedColumns);
        system.debug('columnMapping==='+IdMapListing );   
        statusJSONMap.put(inputTypeListing,'Manually Mapped'); 
        statusJSON= JSON.serialize(statusJSONMap);
        for(string str :statusjsonmap.keyset()){
          if(statusjsonmap.get(str)!='Mapping Failed'){
              StatusSaved=true;
          }
          else{
              StatusSaved=false;
              break;
          }
      }
    }
    public void getMapping(){
        string mappingId=ApexPages.CurrentPage().getParameters().get('mappingId');
        system.debug(mappingId+'------');
        system.debug(IdMapListing);
        system.debug(inputTypeListing);
        mappingColmns  = getcolumnsfromS3File(mappingId);
        Map<string,Object> mappingAdapter = (Map<string,Object>)IdMapListing.get(inputTypeListing);
        columnStatic = string.valueof(mappingAdapter.get('TableCol'));
        optionsMapping= string.valueof(mappingAdapter.get('MappingCol'));
        typeColumnsStatic =string.valueof(mappingAdapter.get('TypeCol'));
        List<string> mappingColmnList = new List<string>();
            mappingColmnList  = mappingColmns.split(',');
        for(string col : optionsMapping.split(',')) {
            if(!mappingColmnList.contains(col)){
                statusJSONMap.put(inputTypeListing,'Mapping Failed');
                break;
            }
            else
                statusJSONMap.put(inputTypeListing,'Auto Mapped');
        }
        
        system.debug('getting options'+optionsMapping );
        statusJSON= JSON.serialize(statusJSONMap);   
        system.debug('statusJSON'+statusJSON);
        for(string str :statusjsonmap.keyset()){
          if(statusjsonmap.get(str)!='Mapping Failed'){
              StatusSaved=true;
          }
          else{
              StatusSaved=false;
              break;
          }
      }
    }
    public string getcolumnsfromS3File(string fileName){
    
        system.debug('getting options');
        string optionsMapping='';
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        string folder=initialfolder+selectedfolder+'/';
        String endPoint='http://52.24.8.231/data_management/s3_detect_columns?bucket_name='+bucket+'&object_key='+initialfolder+filename;
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        Map<string,object> resBody= (Map<string,object>)JSON.deserializeUntyped(response.getBody());
        List<object> columnsList= (List<object>)resBody.get('columns');
        for(object o : columnsList){
            Map<string,object> obj = (Map<string,object>)o;
            optionsMapping = optionsMapping+obj.get('column')+',';
        }
        if(optionsMapping.endswith(','))
            optionsMapping =optionsMapping.removeend(',');
        optionsMapping='--None--,'+optionsMapping;
        return optionsMapping ;
    }
   public void getMappingfromJSON(){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint=serverName+S3prefix+'DataMaxUIService/getMappingDataMax/?planID='+planUuid+'&tenant='+tenantAbbname+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        IdMapListing = (Map<string,Object>)JSON.deserializeUntyped(response.getBody());
        system.debug('resBody '+IdMapListing );
   } 
   public string inputTypeListing{get;set;}
   public void inputList(){
       calledforinput=true;
       system.debug(ApexPages.CurrentPage().getParameters().get('SelectedFolder')+'============');
       if(ApexPages.CurrentPage().getParameters().get('SelectedFolder')!='SelectedFolder' &&ApexPages.CurrentPage().getParameters().get('SelectedFolder')!='')
           SelectedFolder=ApexPages.CurrentPage().getParameters().get('SelectedFolder');
       if(ApexPages.CurrentPage().getParameters().get('InputType')!='Adaptor' && ApexPages.CurrentPage().getParameters().get('InputType')!='')
           inputTypeListing=InputType;
       
       callWebServiceListing(); 
   }
   public void callWebServiceListing(){
       listInputs = new list<wrapperinputs>();
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint='http://52.24.8.231/data_management/s3_list_objects?bucket_name='+bucket+'&prefix='+initialfolder+SelectedFolder+'/';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        Map<string,Object> resBody = (Map<string,Object>)JSON.deserializeUntyped(response.getBody());
        List<Object> Listing = (List<Object>)resBody.get('metadata');
        for(Object o :Listing){
            if(listInputs.size()<50){
            Map<string,Object> obj  = (Map<string,Object>)o;
            wrapperinputs w = new wrapperinputs(string.valueof(obj.get('Key')),string.valueof(obj.get('LastModified')),SelectedFolder );
            listInputs.add(w);}
        }
   }
   
    // Fill in the queries that gives result as a Map 
    public Map<string,string> executeQueryLookup(string query, string key,string value){
      return null;  
    }
    
    public PageReference ExitPlan(){
        PageReference pageref;
        pageref= Page.DataManagementListingPage;
        pageref.setRedirect(true);
        return pageref;
  }
    
    
    public string returnUuid(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return 'SC_Instance_ID_'+guid;
    }
    
    public class wrapperInputs{
        public string name{get;set;}
        public boolean checkbox{get;set;}
        public string lastUpdatedOn{get;set;}
         public string id{get;set;}
        public wrapperInputs(string Wname, string wlastUpdatedOn,string SelectedFolder ){
            name=Wname;
            lastUpdatedOn=wlastUpdatedOn;
            checkbox=false;
            id=SelectedFolder+'/'+name;
        }
    }
    
    public static map<string,object> getValidationMapping(string serverName,string S3prefix,string planUuid,string tenant ){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        string endPoint=serverName+S3prefix+'DataMaxUIService/getMappingValidationService/?planID='+planUuid+'&tenant='+tenant+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response = h.send(request);
        Map<string,object> resBody= (Map<string,object>)JSON.deserializeUntyped(response.getBody());
        return resBody;
    }
    
    public PageReference validatePlan(){
        validate(plan.id,servername,S3prefix ,tenant);
        plan.execution_status__c='Validation in progress';
        update plan;
        PageReference pageref;
        pageref= Page.DataManagementListingPage;
        pageref.setRedirect(true);
        return pageref;
    }
    public PageReference executePlan(){
        execute(plan.id,servername,S3prefix ,tenant, TableNameMap);
        plan.Last_Executed_On__c= system.now();
        plan.time_period__c=date.valueof(system.today());
        plan.execution_status__c='Execution in progress';
        update plan;
        PageReference pageref;
        pageref= Page.DataManagementListingPage;
        pageref.setRedirect(true);
        return pageref;
    }
   public void getfolders(){
       Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint='http://52.24.8.231/data_management/s3_list_folders?bucket_name='+bucket+'&prefix='+initialfolder;
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        Map<string,Object> resBody = (Map<string,Object>)JSON.deserializeUntyped(response.getBody());
        List<Object> Listing = (List<Object>)resBody.get('metadata');
        for(Object o :Listing){
            Map<string,Object> obj  = (Map<string,Object>)o;
            Foldersavailable.add(new SelectOption(string.valueof(obj.get('Key')),string.valueof(obj.get('Key'))));
            SelectedFolder =string.valueof(obj.get('Key'));
        }
        callWebServiceListing();
   }
   @future(callout=true)
   public static void validate(string planID ,string servername ,string S3prefix ,string tenant){
   t_plan__c Plan =[Select id,Navigation_from__c,Data_Object__c,Execution_Status__c,Plan_Name__c,Instance__c,
   Last_Executed_On__c,plan_uuid__c,Time_Period__c,componentstatus__c  from t_plan__c where id=:planID];
   Map<string,object> mappingmap = getValidationMapping(serverName,S3prefix,plan.plan_uuid__c,tenant);
        List<String> urlbody = new List<String> {
               'source_module=' +plan.Navigation_from__c.substringbefore(' To'),
               'dest_module=' +plan.Navigation_from__c.substringafter(' To '),
               'entity='+plan.Data_Object__c,
               'filter_clause=',
               'validation='+True,
               'etl='+False,
               'adapter='+json.serialize(mappingmap),
               'move_data=False',
               'validated_table=',
                'planID='+plan.plan_uuid__c
           };
        system.debug(urlbody);
        string url='http://34.211.106.14:80/data_management/transfer?';
        String bodyurl = String.join(urlbody, '&');
        HttpRequest request1 = new HttpRequest(); 
        request1.setEndpoint(url);
        request1.setMethod('POST');
        request1.setBody(bodyurl );    
        request1.setTimeOut(120000);
        Http httpreq = new Http();
        httpreq.send(request1); 
   }
   @future(callout=true)
   public static void execute(string planID ,string servername ,string S3prefix ,string tenant, map<string,string> TableNameMap){
       t_plan__c Plan =[Select id,Navigation_from__c,Data_Object__c,Execution_Status__c,Plan_Name__c,Instance__c,
       Last_Executed_On__c,plan_uuid__c,Time_Period__c,componentstatus__c  from t_plan__c where id=:planID];
       Map<string,object> mappingmap = getValidationMapping(serverName,S3prefix,plan.plan_uuid__c,tenant );
        List<String> urlbody = new List<String> {
               'source_module=' +plan.Navigation_from__c.substringbefore(' To'),
               'dest_module=' +plan.Navigation_from__c.substringafter(' To '),
               'entity='+plan.Data_Object__c,
               'filter_clause=',
               'validation='+False,
               'etl='+True,
               'adapter='+json.serialize(mappingmap),
               'move_data=True',
               'validated_table='+json.serialize(TableNameMap),
               'planID='+plan.plan_uuid__c
                
           };
        system.debug(urlbody);
        string url='http://34.211.106.14:80/data_management/transfer?';
        String bodyurl = String.join(urlbody, '&');
        HttpRequest request1 = new HttpRequest(); 
        request1.setEndpoint(url);
        request1.setMethod('POST');
        request1.setBody(bodyurl );    
        request1.setTimeOut(120000);
        Http httpreq = new Http();
        httpreq.send(request1); 
   }
}