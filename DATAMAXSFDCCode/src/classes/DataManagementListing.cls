public class DataManagementListing{
    public string listingJSON{get;set;}
    public string tenant;
    public string type;
    public string serverName;
    public string s3Prefix;
    public list<t_plan__c> listingItems{get;set;}
    public string log {get;set;}
    public DataManagementListing(){
        ETL_Config__c etl =[Select Org_Type__c,S3_Bucket__c,Esri_Server_IP__c,S3_Filename__c,Server_Type__c from  ETL_Config__c where name='DataManagement'];
        type='DataMax';
        S3Prefix = etl.Server_Type__c;
        if(S3prefix==null)
            S3prefix='';
        
        tenant = etl.Org_Type__c;
        servername = etl.Esri_Server_IP__c;
        system.debug('servername ==='+servername );
        getInboundlIsting();
    }
    public void getInboundlIsting(){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        String endPoint=serverName+S3prefix+'DataMaxUIService/getListingDataMax/?tenant='+tenant+'&type='+type;
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response= h.send(request);
        List<Object> resBody = (List<Object>)JSON.deserializeUntyped(response.getBody());
        listingJSON = json.serialize(resBody);
        system.debug(listingJSON);
    }
    
    public void getListingModulewise(){
        string ModuleName = ApexPages.CurrentPage().getParameters().get('moduleType'); 
        system.debug(ModuleName+'==ModuleName ');
        listingitems= [select id,plan_name__c,componentstatus__c,Time_Period__c,plan_uuid__c,type__c,Navigation_from__c,ModuleTypeFile__c,Execution_Status__c,Last_Executed_On__c
                        ,Data_Object__c from t_plan__c where ModuleTypeFile__c=:ModuleName];
                 
    }
    public PageReference NewInstance(){
        string ModuleName = ApexPages.CurrentPage().getParameters().get('moduleType'); 
        system.debug(ModuleName+'==ModuleName ');
        PageReference pageref;
        pageref= Page.DataManagementTemplatization;
        pageref.getParameters().put('ModuleType',ModuleName);
        pageref.getParameters().put('Id',null);
        pageref.setRedirect(true);
        return pageref;
    }
    
    public PageReference editInstance(){
        string planID= ApexPages.CurrentPage().getParameters().get('PlanId'); 
        t_plan__c DM = [Select id,ModuleTypeFile__c from t_plan__c where id=:planID];
        PageReference pageref;
        pageref= Page.DataManagementTemplatization;
        pageref.getParameters().put('ModuleType',DM.ModuleTypeFile__c );
        pageref.getParameters().put('Id',planID);
        pageref.setRedirect(true);
        return pageref;
    }
    public PageReference executeDM(){
        system.debug('in execute');
        string planID = ApexPages.CurrentPage().getParameters().get('PlanId');
        Date timeperiod= date.valueof(system.now()); 
        system.debug(planID+'================'+timeperiod);
        t_plan__c selectedPlan = [Select id,Data_Object__c,Navigation_from__c,Execution_Status__c,ModuleTypeFile__c,Last_Executed_On__c,Plan_Name__c,plan_uuid__c,type__c,Time_Period__c from t_plan__c where id=:planID];
        t_plan__c tNewInstance= new t_plan__c();
        if(true){//selectedPlan.Time_Period__c!=timeperiod
            
            tNewInstance.plan_name__c = selectedPlan.plan_name__c;
            tNewInstance.Data_Object__c= selectedPlan.Data_Object__c;
            tNewInstance.Last_Executed_On__c= system.now();
            tNewInstance.plan_uuid__c= returnUuid();
            tNewInstance.Execution_Status__c= 'Processing';
            tNewInstance.type__c = selectedPlan.type__c ;
            tNewInstance.ModuleTypeFile__c=selectedPlan.ModuleTypeFile__c;
            tNewInstance.time_period__c=timeperiod;
            tNewInstance.Navigation_from__c =selectedPlan.Navigation_from__c;
            tNewInstance.data_object__C =selectedPlan.data_object__C ;
            callCreateInstance(tNewinstance.plan_uuid__c,selectedPlan.plan_uuid__C);
        }
        else{
            tNewInstance=selectedPlan;
            tNewInstance.Last_Executed_On__c= system.now();
            tNewInstance.Execution_Status__c= 'Processing';
            
        }
        
        
        system.debug('tNewInstance==========='+tNewInstance.id);
        callserviceValidation(tNewInstance);
        upsert tNewInstance;
        PageReference pageref;
        pageref= Page.datamanagementlistingpage;
        pageref.setRedirect(true);
        return pageref;
    }
    public map<string,object> getValidationMapping(string planuuid){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        string endPoint=serverName+S3prefix+'DataMaxUIService/getMappingValidationService/?planID='+planUuid+'&tenant='+tenant+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response = h.send(request);
        Map<string,object> resBody= (Map<string,object>)JSON.deserializeUntyped(response.getBody());
        return resBody;
    }
    
    public void callserviceValidation(t_plan__c plan){
        Map<string,object> mappingmap = getValidationMapping(plan.plan_uuid__c);
        List<String> urlbody = new List<String> {
               'source_module=' +plan.Navigation_from__c.substringbefore(' To'),
               'dest_module=' +plan.Navigation_from__c.substringafter(' To '),
               'entity='+plan.Data_Object__c,
               'filter_clause=',
               'validation='+True,
               'etl='+True,
               'adapter='+json.serialize(mappingmap),
               'move_data=True',
               'validated_table=',
               'planID='+plan.plan_Uuid__c
                
           };
        string url='https://salesiqservices.axtria.com/data_management/transfer?';
        String bodyurl = String.join(urlbody, '&');
        HttpRequest request1 = new HttpRequest(); 
        request1.setEndpoint(url);
        request1.setMethod('POST');
        request1.setBody(bodyurl );    
        request1.setTimeOut(120000);
        Http httpreq = new Http();
        HTTPResponse response1=httpreq.send(request1); 
        plan.Last_Executed_On__c= system.now();
        plan.time_period__c=date.valueof(system.today());
        plan.execution_status__c='Execution in progress';
        plan.componentstatus__c=True;
        
    }
    
    public void callCreateInstance(string newID, string oldID){
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        string endPoint=serverName+S3prefix+'DataMaxUIService/createExecuteInstance/?oldID='+oldID+'&newID='+newID+'&tenant='+tenant+'&type=DataMax';
        request.setEndPoint(endPoint);
        request.setHeader('Content-type', 'application/json');
        request.setMethod('GET');
        request.setTimeout(20000);
        HttpResponse response = h.send(request);
        
    }
    
    public string returnUuid(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return 'SC_ID_'+guid;
    }
    public void nouse(){
    }
    public void getError(){
        string planID = ApexPages.CurrentPage().getParameters().get('PlanId');
        t_plan__c DM = [Select id,log__c from t_plan__c where id=:planID];
        log = DM.Log__c;
        
    }
}