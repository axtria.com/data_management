import os
import json
import logging

from ast import literal_eval
from datetime import datetime
from flask import Flask, request, send_file
from logging.handlers import TimedRotatingFileHandler
from simple_salesforce import exceptions as SFexceptions

from user.controller import Controller
from system.util import fetch_parameter
from system.s3_fs import S3


logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs//scenario_services.log", when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
logging.getLogger('urllib3').setLevel(logging.ERROR)
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('s3transfer').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)


app     = Flask(__name__)
PREFIX  = '/data_management'
METHODS = ["GET","POST"]


@app.route(PREFIX + '/transfer', methods=METHODS)
def data_management():
    def remove_sc_from_queue(scenario_id):
        with open(os.path.join('system', 'scenarios_in_progress.txt') , 'r+') as f:
            a = f.read()
            a = a.replace(scenario_id, '')
            f.truncate(0)
            f.seek(0)
            f.write(a)
            logging.info("Scenario {} has been removed from 'In Progress'".format(scenario_id))

    """
    Params:
        source_module   : from where data is to be fetched
        dest_module     : to where data needs to be inserted
        entity          : files to be moved (e.g. roster)
        filter_clause   : filter clause (e.g. country='US')
        validation      : validation to be run or not. Default True. Will be false when only movement to dest_module is desired after having 
                          run validation previously. 
        etl             : etl to be performed or not
        user_mapping    : for inbound, actual file-to-adapter mapping configured by user at UI.
        move_data       : to move data to destination module or not. In case only validation is desired, this is set to False.
        validated_table : when validation previously called, this indicates the success table that was created on PG per adapter.
                          This will have move_data=True and validation=False. in case validate-only request, this will be blank.
        plan_id         : Key identifier for the record on SalesForce where success or failure needs to be updated. 
    """

    source_module           = fetch_parameter(request, "source_module"            , default='s3',    type=str)
    dest_module             = fetch_parameter(request, "dest_module"              , default='s3',    type=str)
    entity                  = fetch_parameter(request, "entity"                   , default='',      type=str)
    filter_clause           = fetch_parameter(request, "filter_clause"            , default='',      type=str)
    validation 				= fetch_parameter(request, "validation" 			  , default='true',  type=str)
    etl	                    = fetch_parameter(request, "etl"                      , default='',      type=str)
    user_mapping            = fetch_parameter(request, "adapter"				  , default='{}', 	 type=str)
    move_data				= fetch_parameter(request, "move_data"   			  , default='true',  type=str)
    validated_table 		= fetch_parameter(request, "validated_table" 		  , default='{}',    type=str)
    plan_id 				= fetch_parameter(request, "planID"					  , default='', 	 type=str)
    talend_params           = fetch_parameter(request, 'talend_param'             , default='',      type=str)
    error_talend_url        = fetch_parameter(request, 'error_param'              , default='',      type=str)

    if not validated_table:
        validated_table         = json.loads('{}')

    move_data 				= True if move_data.lower() == 'true' else False
    etl						= True if etl.lower() == 'true' else False
    validation 				= True if validation.lower() == 'true' else False
    user_mapping 			= json.loads(user_mapping)
    
    
    try:
        folder              = source_module + '_' + dest_module + '_' + entity + '_' + datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        tmp_dir             = os.path.join('tmp', folder)
        os.mkdir(tmp_dir)
        job                 = Controller(source_module, dest_module, entity, filter_clause, validation, etl, user_mapping, tmp_dir, move_data, 
        								 validated_table, plan_id, request.host, PREFIX, talend_params, error_talend_url)
        response 			= job.invoke()

    except SFexceptions.SalesforceAuthenticationFailed as auth:
        logging.critical(auth)
        # remove_sc_from_queue(scenario_id)
        return str(auth)

    except Exception as e:
        logging.exception(e)
        # remove_sc_from_queue(scenario_id)
        return str(e)
    return response
    # if not response:
    #     # remove_sc_from_queue(scenario_id)
    #     return 'Completed Successfully'
    # if response == 'Validation Failed':
    #     # file_name = timestamp + '.zip'
    #     # abs_file_path = os.path.abspath(os.path.join('tmp'))
    #     # logging.debug(abs_file_path)
    #     # return send_from_directory(abs_file_path, file_name, mimetype='application/octet-stream',
    #     #                            attachment_filename=file_name, as_attachment=True)
    #     # remove_sc_from_queue(scenario_id)
    #     return "Validation Failed"
    # # remove_sc_from_queue(scenario_id)
    # return "Error Occurred \n {}".format(str(response))

@app.route(PREFIX +'/master_sync', methods=METHODS)
def master_sync():
    data_object_ids = fetch_parameter(request, 'dataObjectIds',  default='1',     type=str)
    scenario_id     = fetch_parameter(request, 'scenario_id',    default='',      type=str)
    user_name       = fetch_parameter(request, 'user_name',      default='1',     type=str)
    password        = fetch_parameter(request, 'password',       default='1',     type=str)
    security_token  = fetch_parameter(request, 'security_token', default='',      type=str)
    namespace       = fetch_parameter(request, 'namespace',      default='',      type=str)
    sandbox         = fetch_parameter(request, 'sandbox',        default='False', type=str)

    ms = MasterSync(data_object_ids, scenario_id, user_name, password, security_token, namespace, sandbox)
    ms.invoke()

# @app.route(PREFIX + '/download/<fname>/<attach>', methods=METHODS)
# def download(fname,attach):
#     # return os.path.abspath(os.path.join('tmp',fname))
#     return send_file(os.path.abspath(os.path.join('tmp',fname)), mimetype='application/zip',
#                      as_attachment=True,attachment_filename=attach+"__"+fname)

@app.route(PREFIX + '/s3_list_folders', methods=METHODS)
def s3_list_folders():
    logging.debug("Invoking service to list files on s3 folder")
    bucket_name     = fetch_parameter(request, 'bucket_name',    default='',     type=str)
    prefix          = fetch_parameter(request, 'prefix',         default='',     type=str)
    with open(os.path.join("configurationFiles", "creds.json")) as f:
        creds = json.load(f)
    s3 = S3(creds['s3']['aws_access_key_id'], creds['s3']['aws_secret_access_key'])
    return json.dumps(s3.list_folders(bucket_name=bucket_name, prefix=prefix))

@app.route(PREFIX + '/s3_list_objects', methods=METHODS)
def s3_list_objects():
    logging.debug("Invoking service to list files on s3 folder")
    bucket_name     = fetch_parameter(request, 'bucket_name',    default='',     type=str)
    prefix          = fetch_parameter(request, 'prefix',         default='',     type=str)
    with open(os.path.join("configurationFiles", "creds.json")) as f:
        creds = json.load(f)
    s3 = S3(creds['s3']['aws_access_key_id'], creds['s3']['aws_secret_access_key'])
    return json.dumps(s3.detect_columns(bucket_name, prefix))


@app.route(PREFIX + '/download', methods=METHODS)
def download():
    file_path       = fetch_parameter(request, 'file_path',       default='', type=str)
    attachment_name = fetch_parameter(request, 'attachment_name', default='', type=str)
    logging.debug("Sending Error File for {} as attachment".format(attachment_name))
    return send_file(file_path, mimetype='application/text', as_attachment=True, attachment_filename=attachment_name)

@app.route(PREFIX + "/adapter/get_list", methods=METHODS)
def get_list():
    # adapter_name   = fetch_parameter(request, 'adapter_name', default='', type=str)
    # logging.info("Retrieving adapter definintions for {}".format(adapter_name))
    definition = {}
    definition['Content'] = []
    try:
        for file in os.listdir(os.path.join("configurationFiles", "adapters")):
            logging.debug("Scanning : {}".format(file))
            with open(os.path.join('configurationFiles', "adapters", file)) as f:
                config = json.load(f)
                for entity in config:
                        if entity.lower() == file.split(".")[0].lower():
                            file = entity
                config = config[file]["displayname_to_adapter"]
                for adapter in config:
                    d = {}
                    d['name'] = adapter.lower()
                    d['adaptor_meta'] = []
                    for val in config[adapter].values():
                        d['adaptor_meta'].append({"column_name" : list(val.keys())[0], "data_type":list(val.values())[0]})
                    definition['Content'].append(d)
        logging.debug(definition)
        return json.dumps(definition)
    except Exception as e:
        logging.exception("ERROR")
        return str(e)

@app.route(PREFIX + "/adapter/fetch_definition", methods=METHODS)
def fetch_definition():
    adapter_name   = fetch_parameter(request, 'adapter_name', default='', type=str)
    logging.info("Retrieving adapter definintions for {}".format(adapter_name))
    definition = {}
    try:
        for adapter in adapter_name.split(","):
            definition[adapter] = {}
            break_outer = False
            for file in os.listdir(os.path.join("configurationFiles", "adapters")):
                if break_outer:
                    break
                logging.debug("Scanning : {}".format(file))
                with open(os.path.join('configurationFiles', "adapters", file)) as f:
                    config = json.load(f)
                    for entity in config:
                        if entity.lower() == file.split(".")[0].lower():
                            file = entity
                    config = config[file]["displayname_to_adapter"]
                    for adap in config:
                        if adap.lower() == adapter.lower():
                            logging.debug("Adapter found.")
                            for val in config[adap].values():
                                definition[adapter].update(val)
                            break_outer = True
                            break
        logging.debug(definition)
        return json.dumps(definition)
    except Exception as e:
        logging.exception("ERROR")
        return str(e)

@app.route(PREFIX + "/adapter/fetch_filter", methods=METHODS)
def fetch_filter():
    entity   = fetch_parameter(request, 'entity', default='', type=str)
    source   = fetch_parameter(request, 'source', default='', type=str)
    dest     = fetch_parameter(request, 'dest',   default='', type=str)

    logging.info("Retrieving filters for {}".format(entity))
    filter_set = set()
    try:
        with open(os.path.join('configurationFiles', "adapters", entity.lower() + '.json')) as f:
            CONFIG = literal_eval(str(json.load(f)).lower())
        for adapter in CONFIG[entity.lower()][source.lower()+'_to_'+dest.lower()]['source_to_adapter']:
            # logging.debug(adapter)
            filter_set.update(set(CONFIG[entity.lower()][source.lower()+'_to_'+dest.lower()]['source_to_adapter'][adapter]['filters'].keys()))
        return json.dumps(list(filter_set))
    except Exception as e:
        logging.exception("ERROR")
        return str(e)



@app.before_request
def log_request_info():
    logging.info('Request ::: %r' % request.url)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


@app.errorhandler(500)
def internal_server_error_500(error):
    logging.error(f'Generic Server Exception (500): {error}')
    return f"Server Exception: {error}"

@app.errorhandler(503)
def internal_server_error_503(error):
    logging.error(f'Server Exception: {error}')
    return f"Server Exception (503): {error}"

if __name__ == '__main__':
    app.run(debug=False)



